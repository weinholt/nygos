#!/bin/sh
# -*- coding: utf-8 -*-
# The Unbelievable Operating System From Hell
# Copyright © 2006-2007 Göran Weinholt <goran@weinholt.se>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

# Build a bootable image with GRUB and src/nygos. Works on Debian.

export PATH=$PATH:/sbin:/usr/sbin

set -e

dd if=/dev/zero of=nygos.img bs=512 count=$((4 * 18 * 80))

/sbin/mke2fs -N 32 -F nygos.img

if [ -d /usr/lib/grub/i386-pc/ ]; then
	e2cp /usr/lib/grub/i386-pc/* nygos.img:boot/grub/
elif [ -d /usr/share/grub/i386-redhat ]; then
	e2cp /usr/share/grub/i386-redhat/* nygos.img:boot/grub/
elif [ -d /usr/lib/grub/x86_64-pc ]; then
	e2cp /usr/lib/grub/x86_64-pc/* nygos.img:boot/grub/
else
	e2cp /lib/grub/i386-pc/* nygos.img:boot/grub/
fi

if ! [ -f menu.lst ] ; then
cat <<EOF > menu.lst
timeout 3
color cyan/blue white/blue

title   The Unbelievable Operating System from Hell
root    (fd0)
kernel  /nygos
EOF
fi

e2cp menu.lst nygos.img:boot/grub/

e2cp src/nygos nygos.img:

cat <<EOF > device.map
(fd0) $PWD/nygos.img
EOF

grub --device-map=device.map --batch <<EOF
root (fd0)
setup (fd0)
EOF

echo
echo
echo "Your bootable image is waiting for you: nygos.img"
