;; -*- coding: utf-8 -*-
;; The Unbelievable Operating System from Hell
;; Copyright © 2007 Göran Weinholt <goran@weinholt.se>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; USB Hub class driver

;; Request codes
(define HUB-GET-STATUS 0)
(define HUB-CLEAR-FEATURE 1)
(define HUB-GET-STATE 2)
(define HUB-SET-FEATURE 3)
(define HUB-GET-DESCRIPTOR 6)
(define HUB-SET-DESCRIPTOR 7)

(define (usb-hub-class-driver usbdev descriptors)
  (print "USB Hub class driver started")

;;   (print "Selecting configuration value " (cd-configuration-value-ref confdesc))
;;   (!? USBD (list 'set-configuration usbdev (cd-configuration-value-ref (car descriptors))))
;;   (print "Selected configuration 0")
  ;;     (usb-register-hub usbdev)


  
  (do-setup-request usbdev
                    0
                    #b10100000
                    HUB-GET-DESCRIPTOR
                    (ash DESCRIPTOR-CONFIGURATION 8)
                    0
                    7)
    
  (forever
    (match (?)
      
      (_ #f)))
  )
