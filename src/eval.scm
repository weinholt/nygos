;; -*- coding: utf-8 -*-
;; The Unbelievable Operating System from Hell
;; Copyright © 2005-2007 Göran Weinholt <goran@weinholt.se>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; Eval

(define (eval-seq statements env procs)
  (let lp ((s statements))
    (cond ((null? s)
           (unspecified))
          ((null? (cdr s))
           (eval-code (car s) env procs))
          (else
           (eval-code (car s) env procs)
           (lp (cdr s))))))

(define (lookup-global name)
  (let* ((env (sys!global-environment))
         (hash (symbol-hash name))
         (index (modulo hash (vector-length env))))
    (assq name (vector-ref env index))))

(define (define-global name value)
  (let* ((env (sys!global-environment))
         (hash (symbol-hash name))
         (index (modulo hash (vector-length env))))
    (vector-set! env index
                 (cons (cons name value)
                       (vector-ref env index)))))

(define (bind-formals env formals* values*)
  (let lp ((new env)
           (formals formals*)
           (values values*))
    (cond ((null? formals)
           (unless (null? values)
             (error 'eval "too many arguments to procedure"
                    formals values))
           new)
          ((symbol? formals)
           (lp (cons (cons formals values) new)
               '() '()))
          ((null? values)
           (error 'eval "too few arguments to procedure"))
          (else
           (lp (cons (cons (car formals) (car values)) new)
               (cdr formals) (cdr values))))))

(define (eval-code code env procs)
  (match code
    (('quote x) x)
    ((? symbol? x)                      ;variables
     (cond ((assq x env) => cdr)
           ((lookup-global x) => cdr)
           (else
            (error 'eval "unbound variable" x))))
    (('begin statements ___)
     (eval-seq statements env procs))
    (('if test success)
     (if (eval-code test env procs)
         (eval-code success env procs)
         (unspecified)))
    (('if test success fail)
     (if (eval-code test env procs)
         (eval-code success env procs)
         (eval-code fail env procs)))
    (('let (bindings ___) body ___)
     (let ((new-env (map (lambda (x)
                           (cons (car x) (eval-code (cadr x) env procs)))
                         bindings)))
       (eval-seq body (append new-env env) procs)))
    (('closure name values ___)
     (match (assq name procs)
       ((name ('code formals closure-linked body))
        (let ((code (cadr (assq name procs)))
              (env (map (lambda (name)
                          (cons name (eval-code name env procs)))
                        closure-linked)))
          (lambda x
            (eval-code body (bind-formals env formals x) procs))))))
    (('define name value)
     (cond ((lookup-global name) =>
            (lambda (x) (set-cdr! x (eval-code value env procs))))
           (else
            (define-global name (eval-code value env procs)))))
    (('set! name value)
     (cond ((or (assq name env) (lookup-global name)) =>
            (lambda (x)
              (set-cdr! x (eval-code value env procs))))
           (else
            (error 'eval "set! on unbound variable" name))))
    (('unsafe-set-car! name value)
     (set-car! (cdr (or (assq name env) (lookup-global name)))
               (eval-code value env procs)))
    (('unsafe-car name)
     (cadr (or (assq name env) (lookup-global name))))
    (('unsafe-make-value size type)
     (case type
       ((vector) (unsafe-make-value (eval-code size env procs) vector))
       ((string) (unsafe-make-value (eval-code size env procs) string))
       ((record) (unsafe-make-value (eval-code size env procs) record))
       ((symbol) (unsafe-make-value (eval-code size env procs) symbol))
       (else (error 'eval "Unsupported type for unsafe-make-value" type))))
    ((fun ___)                          ;function application
     (apply (eval-code (car fun) env procs)
            (map (lambda (code)
                   (eval-code code env procs))
                 (cdr fun))))
    (x x)))                             ;literals

(define (eval-compiled code)
  (match code
    (('labels (procedures ___) body)
     (eval-code body '() procedures))))

(define (eval code)
  (eval-compiled
   (compiler-passes
    (expand-top-level-forms! (list code)
                             mstore))))
