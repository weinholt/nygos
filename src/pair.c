/* -*- coding: utf-8 -*-
 * The Unbelievable Operating System from Hell
 * Copyright © 2005-2007 Göran Weinholt <goran@weinholt.se>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <nygos.h>
#include <malloc.h>
#include <runtime.h>

#include <pair.h>

/* C version */
SCM cons(SCM car, SCM cdr)
{
	SCM s_pair = new(sizeof(struct pair), PAIR);
 	scmset(s_pair, PAIR, 0, car);
 	scmset(s_pair, PAIR, 1, cdr);

	return s_pair;
}

defun (set_car_ex, "set-car!", 2, 0, (SCM s_pair, SCM value),
	   "Set the first cell in s_pair to value.",
{
	TYPE_ASSERT(IS_PAIR(s_pair), s_pair, 1);
	scmset(s_pair, PAIR, 0, value);

	return UNSPEC;
})

defun (_set_cdr_ex, "set-cdr!", 2, 0, (SCM s_pair, SCM value),
	   "Set the second cell in s_pair to value.",
{
	TYPE_ASSERT(IS_PAIR(s_pair), s_pair, 1);
	scmset(s_pair, PAIR, 1, value);

	return UNSPEC;
})

SCM set_cdr_ex(SCM s_pair, SCM cdr)
{
	TYPE_ASSERT(IS_PAIR(s_pair), s_pair, 1);
	scmset(s_pair, PAIR, 1, cdr);

	return UNSPEC;
}

defun (car, "car", 1, 0, (SCM pair),
	   "Return the first value in the pair.",
{
	TYPE_ASSERT(IS_PAIR(pair), pair, 1);
	return scmref(pair, PAIR, 0);
})

defun (cdr, "cdr", 1, 0, (SCM pair),
	   "Return the second value in the pair.",
{
	TYPE_ASSERT(IS_PAIR(pair), pair, 1);
	return scmref(pair, PAIR, 1);
})

#define MCAR(n)  (((struct pair *) get_addrt(n, PAIR))->car)
#define MCDR(n)  (((struct pair *) get_addrt(n, PAIR))->cdr)

defun (cas_car_ex, "cas-car!", 3, 0, (SCM pair, SCM cmp, SCM src),
	   "This is an atomic version of the following:\n\
(cond ((eq? (car pair) cmp)\n\
       (set-car! pair src)\n\
       #t)\n\
      (else #f))\n\
This operation is atomic, which means that it is impossible for (car pair)\n\
to change before (set-car! pair src) has executed.", {
	TYPE_ASSERT(IS_PAIR(pair), pair, 0);
	uint8 zf = 0;
	/* compare dest with eax, if they are equal load src into dest */
	asm volatile("lock; cmpxchg %[src], %[dest]; setz %[zf]" :
				 [cmp] "+a" (cmp), /* %eax */
				 [dest] "+m" (MCAR(pair)),
				 [zf] "=q" (zf) :
				 [src] "r" (src) :
				 "cc");
	/* cmp is also updated, but are any algorithms interested in it? */
	return TO_BOOL(zf);
})

defun (cas_cdr_ex, "cas-cdr!", 3, 0, (SCM pair, SCM cmp, SCM src),
	   "This is an atomic version of the following:\n\
(cond ((eq? (cdr pair) cmp)\n\
       (set-cdr! pair src)\n\
       #t)\n\
      (else #f))\n\
This operation is atomic, which means that it is impossible for (cdr pair)\n\
to change before (set-cdr! pair src) has executed.", {
	TYPE_ASSERT(IS_PAIR(pair), pair, 0);
	uint8 zf = 0;
	/* compare dest with eax, if they are equal load src into dest */
	asm volatile("lock; cmpxchg %[src], %[dest]; setz %[zf]" :
				 [cmp] "+a" (cmp), /* %eax */
				 [dest] "+m" (MCDR(pair)),
				 [zf] "=q" (zf) :
				 [src] "r" (src) :
				 "cc");
	/* cmp is also updated, but are any algorithms interested in it? */
	return TO_BOOL(zf);
})
