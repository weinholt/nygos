/* -*- coding: utf-8 -*-
 * The Unbelievable Operating System from Hell
 * Copyright © 2005, 2007 Göran Weinholt <goran@weinholt.se>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _NYGOS_H
#define _NYGOS_H

typedef unsigned char uint8;
typedef unsigned short uint16;
typedef unsigned int uint32;
typedef unsigned long uint64;
typedef signed char int8;
typedef signed short int16;
typedef signed int int32;
typedef signed long long int64;
typedef unsigned long size_t;
typedef unsigned long wchar_t;

#define buflen(buf) (sizeof buf / sizeof buf[0])
#define NULL (char*)0

/* Aligns to powers of two. */
#define ALIGN(x, n) (((x) + ((n) - 1)) & ~((n) - 1))

/* Stolen from glib2 headers. */
#define _BOOLEAN_EXPR(expr)		\
 __extension__ ({				\
   int _boolean_var_;			\
   if (expr)					\
      _boolean_var_ = 1;		\
   else							\
      _boolean_var_ = 0;		\
   _boolean_var_;				\
})
#define LIKELY(expr)	(__builtin_expect (_BOOLEAN_EXPR(expr), 1))
#define UNLIKELY(expr)	(__builtin_expect (_BOOLEAN_EXPR(expr), 0))

void panic(char *msg) __attribute__ ((noreturn));

#endif
