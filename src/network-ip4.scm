;; -*- coding: utf-8 -*-
;; The Unbelievable Operating System from Hell
;; Copyright © 2007 Göran Weinholt <goran@weinholt.se>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; IPv4, used by network.scm

;;; RFC 826 - Ethernet Address Resolution Protocol: Or converting
;;; network protocol addresses to 48.bit Ethernet address for
;;; transmission on Ethernet hardware.
;;; This is what it looks like for IPv4 over Ethernet.
(define-bytevector-struct arp-header (endianness big)
  (u16 arp-hardware-type-ref arp-hardware-type-set!)
  (u16 arp-protocol-type-ref arp-protocol-type-set!)
  (u8 arp-hardware-address-length-ref arp-hardware-address-length-set!)
  (u8 arp-protocol-address-length-ref arp-protocol-address-length-set!)
  (u16 arp-opcode-ref arp-opcode-set!)
  (u48 arp-source-ha-ref arp-source-ha-set!)
  (u32 arp-source-ip-ref arp-source-ip-set!)
  (u48 arp-target-ha-ref arp-target-ha-set!)
  (u32 arp-target-ip-ref arp-target-ip-set!))

(define (process-arp-packet interface pkt)
  (cond ((and (= (arp-hardware-type-ref pkt) 1) ;ethernet
              (= (arp-protocol-type-ref pkt) #x0800) ;IP
              (= (arp-hardware-address-length-ref pkt) 6)
              (= (arp-protocol-address-length-ref pkt) 4))
         (case (arp-opcode-ref pkt)
           ((1)
            (when (interface-has-ip4? interface (arp-target-ip-ref pkt))
              (let ((reply (make-bytevector 28)))
                (interface-add-neighbor interface
                                        (ip4-integer->inetaddr (arp-source-ip-ref pkt)
                                                               interface)
                                        (arp-source-ha-ref pkt) 'stale)
                (arp-hardware-type-set! reply 1)
                (arp-protocol-type-set! reply #x0800)
                (arp-hardware-address-length-set! reply 6)
                (arp-protocol-address-length-set! reply 4)
                (arp-opcode-set! reply 2) ; arp reply
                (arp-source-ha-set! reply (interface-lladdr interface))
                (arp-source-ip-set! reply (arp-target-ip-ref pkt))
                (arp-target-ha-set! reply (arp-source-ha-ref pkt))
                (arp-target-ip-set! reply (arp-source-ip-ref pkt))

                (ethernet-send interface (arp-source-ha-ref pkt)
                               #x0806 (list reply)))))
           ((2)
            (interface-add-neighbor interface
                                    (ip4-integer->inetaddr (arp-source-ip-ref pkt)
                                                           interface)
                                    (arp-source-ha-ref pkt) 'stale))))))

(define (send-arp-request interface target-ip source-ip)
  (let ((req (make-bytevector 28)))
;;     (interface-add-neighbor interface target-ip 0 'incomplete)
    (arp-hardware-type-set! req 1)
    (arp-protocol-type-set! req #x0800)
    (arp-hardware-address-length-set! req 6)
    (arp-protocol-address-length-set! req 4)
    (arp-opcode-set! req 1) ; arp request
    (arp-source-ha-set! req (interface-lladdr interface))
    (arp-source-ip-set! req source-ip)
    (arp-target-ip-set! req target-ip)

    (ethernet-send interface #xffffffffffff
                   #x0806 (list req))))

;;; RFC 791: Internet Protocol

(define DEFAULT-TTL 64)

(define PROTOCOL-ICMPv4 1)
(define PROTOCOL-TCP 6)
(define PROTOCOL-UDP 17)

(define-bytevector-struct ip4-header (endianness big)
  ;; FIXME: later when syntax-case is used, redo this as a bitfield
  (u8 ip4-version/ihl-ref ip4-version/ihl-set!)
  (u8 ip4-tos-ref ip4-tos-set!)
  (u16 ip4-length-ref ip4-length-set!)
  (u16 ip4-identification-ref ip4-identification-set!)
  (u16 ip4-flags/fragment-offset-ref ip4-flags/fragment-offset-set!)
  (u8 ip4-ttl-ref ip4-ttl-set!)
  (u8 ip4-protocol-ref ip4-protocol-set!)
  (u16 ip4-header-checksum-ref ip4-header-checksum-set!)
  (u32 ip4-source-ref ip4-source-set!)
  (u32 ip4-target-ref ip4-target-set!))

(define (ip4-version-ref pkt)
  (ash (ip4-version/ihl-ref pkt) -4))
(define (ip4-ihl-ref pkt)
  (logand (ip4-version/ihl-ref pkt) #xf))
(define (ip4-flags-ref pkt)
  (ash (ip4-flags/fragment-offset-ref pkt) -13))
(define (ip4-fragment-offset-ref pkt)
  (logand (ip4-flags/fragment-offset-ref pkt) #b1111111111111))

(define (process-ip4-packet interface pkt)
  ;; FIXME: verify that the route for the source is the right one
  ;; FIXME: verify length and checksum
  (when (and (zero? (ip4-fragment-offset-ref pkt)) ;not fragmented
             (zero? (logand #b1 (ip4-flags-ref pkt))))
    (let-syntax ((process
                  (syntax-rules ()
                    ((_ call)
                     (call (bytevector-section/shared pkt (* 4 (ip4-ihl-ref pkt))
                                                      (ip4-length-ref pkt))
                           (ip4-integer->inetaddr (ip4-source-ref pkt) interface)
                           (ip4-integer->inetaddr (ip4-target-ref pkt) interface))))))
      (case (ip4-protocol-ref pkt)
        ((1) (process process-icmp4-packet))
        ((6) (process process-tcp-packet))
        ((17) (process process-udp-packet))))))

(define (ip4-send payload source target type-of-service
                  protocol time-to-live)
  (cond ((route-lookup-by-target target) =>
         (lambda (route)
           (cond ((interface-lookup-neighbor (route-interface route)
                                             (or (route-router route) target))
                  =>
                  (lambda (ncache-entry)
                    (let ((header (make-bytevector 20))
                          (flags 2)) ;DF is always set, to be more like IPv6
                      (ip4-version/ihl-set! header (logior (ash 4 4)
                                                           5))
                      (ip4-tos-set! header type-of-service)
                      (ip4-length-set! header (+ (* 4 5)
                                                 (bytevectors-length payload)))
                      ;; (ip4-identification-set! header id)
                      (ip4-flags/fragment-offset-set! header (logior (ash flags 13)
                                                                     0)) ;FIXME: fragmentation
                      (ip4-ttl-set! header time-to-live)
                      (ip4-protocol-set! header protocol)
                      (ip4-source-set! header (inetaddr->ip4-integer (route-source route)))
                      (ip4-target-set! header (inetaddr->ip4-integer target))
                      (bytevector-u16-native-set! header 10 (ip-native-checksum-finish
                                                             (ip-native-checksum-part header)))
                      ;; (ip4-header-checksum-set! header (calculate-checksum header))
                      (ethernet-send (route-interface route)
                                     (ncache-entry-lladdr ncache-entry)
                                     #x0800 (cons header payload)))))
                 (else
                  (print "not in neighbor table: " target)))))
        (else
         (print "No route to host: " target))))

;;; RFC 792: Internet Control Message Protocol

(define-bytevector-struct icmp4-header (endianness big)
  (u8 icmp4-type-ref icmp4-type-set!)
  (u8 icmp4-code-ref icmp4-code-set!)
  (u16 icmp4-checksum-ref icmp4-checksum-set!))

(define (process-icmp4-packet pkt source target)
  (case (icmp4-type-ref pkt)
    ((8)                                ;echo request
     (icmp4-type-set! pkt 0)            ;turn it into an echo reply
     (icmp4-checksum-set! pkt 0)
     (bytevector-u16-native-set! pkt 2 (ip-native-checksum-finish
                                        (ip-native-checksum-part pkt)))
     (ip4-send (list pkt) target source 0 PROTOCOL-ICMPv4 DEFAULT-TTL))
    (else
     (print "ICMP: type=" (icmp4-type-ref pkt)
            " code=" (icmp4-code-ref pkt)))))
