;; -*- coding: utf-8 -*-
;; The Unbelievable Operating System from Hell
;; Copyright © 2007 Göran Weinholt <goran@weinholt.se>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; Advanced Configuration and Power Interface (ACPI) support.

;; This code should actually run very early during boot, since the
;; data found using ACPI will indicate what hardware the system has.

;; The FADT will indicate if the system has VGA compatible registers
;; and an i8042 keyboard controller. The rest of the hardware is
;; discovered through the DSDT and SSDT's.

;; There needs to be a fail-safe though. If the ACPI loader fails it
;; shouldn't stop the system from booting.

(define-bytevector-struct rsdp (endianness little)
  ((ascii-string 8) rsdp-signature-ref)
  (u8 rsdp-checksum-ref)
  ((ascii-string 6) rsdp-oemid-ref)
  (u8 rsdp-revision-ref)
  (u32 rsdp-rsdt-address-ref)
  (u32 rsdp-length-ref)
  (u64 rsdp-xsdt-address-ref)
  (u8 rsdp-extended-checksum-ref))

(define-bytevector-struct acpi-dh (endianness little)
  ((ascii-string 4) acpi-dh-signature-ref)
  (u32 acpi-dh-length-ref)
  (u8 acpi-dh-revision-ref)
  (u8 acpi-dh-checksum-ref)
  ((ascii-string 6) acpi-dh-oem-id-ref)
  ((ascii-string 8) acpi-dh-oem-table-id-ref)
  (u32 acpi-dh-oem-revision-ref)
  ((ascii-string 4) acpi-dh-creator-id-ref)
  (u32 acpi-dh-creator-revision-ref))

(define acpi-dh-size 36)

(define-bytevector-struct fadt (endianness little)
  (u32 fadt-facs-address-ref)
  (u32 fadt-dsdt-address-ref)
  (u8)
  (u8 fadt-preferred-pm-profile-ref)
  (u16 fadt-sci-interrupt-ref)           ;SCI interrupt number
  (u32 fadt-smi-command-ref)             ;I/O port for SMI command
  (u8 fadt-acpi-enable-ref)
  (u8 fadt-acpi-disable-ref)
  (u8 fadt-s4bios-req-ref))

(define (eisa-id->string n)
  "Takes an integer representing an EISA ID and turns it into a
string. Such an ID might look like this: PNP0B00"
  (let ((bv (make-bytevector 4)))
    (bytevector-u32-set! bv 0 n (endianness little))
    (let ((id (bytevector-u32-ref bv 0 (endianness big))))
      (string (integer->char (+ #x40 (bit-field id 26 32)))
              (integer->char (+ #x40 (bit-field id 21 26)))
              (integer->char (+ #x40 (bit-field id 16 21)))
              (string-ref (number->string (bit-field id 12 16) 16) 0)
              (string-ref (number->string (bit-field id 8 12) 16) 0)
              (string-ref (number->string (bit-field id 4 8) 16) 0)
              (string-ref (number->string (bit-field id 0 4) 16) 0)))))

;;; Namespace manglement

(define-record-type acpi-node (really-make-acpi-node name node children)
  acpi-tree?
  (name acpi-node-name)
  (node acpi-node-node acpi-node-node-set!)
  (children acpi-node-children acpi-node-children-set!))

(define (make-acpi-node name node . children)
  (really-make-acpi-node name node children))

(define (make-acpi-tree-with-predefines)
  (make-acpi-node "\\" #f
                  (make-acpi-node "_GPE" #f)
                  (make-acpi-node "_PR" #f)
                  (make-acpi-node "_SB" #f)
                  (make-acpi-node "_SI" #f)
                  (make-acpi-node "_TZ" #f)
                  (make-acpi-node "_GL" 'fixme-mutex) ;FIXME
                  (make-acpi-node "_OS" "The Unbelievable Operating System From Hell (unreleased)")
                  (make-acpi-node "_OSI" (lambda x -1)) ;FIXME
                  (make-acpi-node "_REV" 3)))

(define (print-acpi-tree node)
  (define (print-subtree node first middle indent last lastindent)
    (print first (acpi-node-name node))
    (unless (null? (acpi-node-children node))
      (let lp ((node (acpi-node-children node)))
        (let ((indent (if (null? (cdr node)) lastindent indent)))
          (print-subtree (car node)
                         (if (null? (cdr node)) last middle)
                         (string-append indent "|-- ")
                         (string-append indent "|   ")
                         (string-append indent "`-- ")
                         (string-append indent "    "))
          (unless (null? (cdr node))
            (lp (cdr node)))))))
  (print-subtree node
                 ""
                 "|-- "
                 "|   "
                 "`-- "
                 "    "))

(define (acpi-tree-append! root path node)
  #f)


;;; Makeshift ports

(define (make-bytestream bv)
  (let ((length (bytevector-length bv))
        (index 0))
    (lambda (cmd)
      (match cmd
        ('get
         (if (= index length)
             (eof-object)
             (let ((v (bytevector-u8-ref bv index)))
               (set! index (+ index 1))
               v)))
        ('lookahead
         (if (= index length)
             (eof-object)
             (bytevector-u8-ref bv index)))
        (('get-bytevector-n n)
         (if (= index length)
             (eof-object)
             (let* ((end (min (+ index n) length))
                    (v (bytevector-section bv index end)))
               (set! index end)
               v)))
        ('position
         index)
        (('set-port-position! n)
         (if (<= 0 n length)
             (set! index n)
             (error 'set-port-position! "out of range" n)))))))

(define (get-u8 port)
  (let ((v (port 'get)))
    ;;    (print "READ: #x" (number->string v 16))
    v))

(define (lookahead-u8 port)
  (let ((v (port 'lookahead)))
    ;;    (print "LOOKAHEAD: #x" (number->string v 16))
    v))

(define (get-bytevector-n port n)
  (let ((v (port (list 'get-bytevector-n n))))
    ;;    (print "GET-BYTEVECTOR-N: " v)
    v))

(define (port-position port)
  (let ((v (port 'position)))
    ;;    (print "POSITION: " v)
    v))

(define (set-port-position! port n)
;;   (print "SET-PORT-POSITION!: " n)
  (port (list 'set-port-position! n)))

;;; AML reader

(define (read-package-length port)
  (let ((lead-byte (get-u8 port)))
    (case (bit-field lead-byte 6 8)
      ((0) (logand lead-byte #b111111))
      ((1) (logior (ash (get-u8 port) 4)
                   (logand #b1111 lead-byte)))
      ((2) (let* ((x (get-u8 port))
                  (y (get-u8 port)))
             (logior (ash y 12)
                     (ash x 4)
                     (logand #b1111 lead-byte))))
      ((3) (let* ((x (get-u8 port))
                  (y (get-u8 port))
                  (z (get-u8 port)))
             (logior (ash z 20)
                     (ash y 12)
                     (ash x 4)
                     (logand #b1111 lead-byte)))))))

(define (string-trim-right str char)
  (do ((i (- (string-length str) 1) (- i 1)))
      ((or (= i -1)
           (not (char=? char (string-ref str i))))
       (substring str 0 (+ i 1)))))

(define (read-name-string port)
  (define (read-one-nameseg start)
    (let* ((byte1 (or start (get-u8 port)))
           (byte2 (get-u8 port))
           (byte3 (get-u8 port))
           (byte4 (get-u8 port))
           (name (list byte1 byte2 byte3 byte4)))
      (unless (every (lambda (b)
                       (or (<= #x41 b #x5a)
                           (<= #x30 b #x39)
                           (= b #x5f)))
                     name)
        (error 'read-name-string
               "Unexpected byte in namestring"
               name (port-position port)))
      (string-trim-right (list->string (map integer->char name))
                         #\_)))
  (let lp ((ret '()) (byte (get-u8 port)))
    (cond
     ((or (<= #x41 byte #x5A)
          (= byte #x5F))
      (reverse (cons (read-one-nameseg byte) ret)))
     ((= byte #x5c) (lp (cons 'root ret) (get-u8 port))) ;absolute path
     ((= byte #x5e) (lp (cons 'up ret) (get-u8 port)))
     ((= byte #x2e)                     ;dual name
      (let* ((name1 (read-one-nameseg #f))
             (name2 (read-one-nameseg #f)))
        (reverse `(,name2 ,name1 ,@ret))))
     ((= byte #x2f)                     ;multi name
      (let ((length (get-u8 port)))
        (do ((i 0 (+ i 1))
             (ret ret (cons (read-one-nameseg #f) ret)))
            ((= i length)
             (reverse ret)))))
     ((= byte #x00) (reverse ret))      ;null name
     (else
      (error 'read-name-string
             "Unexpected byte in namestring"
             byte (port-position port))))))


;; (let ((test (make-bytestream
;;              (u8-list->bytevector (map char->integer
;;                                        '(#\\ #\^ #\A #\B #\C #\D #\e))))))
;;   (read-name-string test))

;; (let ((test (make-bytestream
;;              (u8-list->bytevector (map char->integer
;;                                        '(#\A #\B #\C #\D #\e))))))
;;   (read-name-string test))

;; (let ((test (make-bytestream
;;              (u8-list->bytevector (map char->integer
;;                                        '(#\. #\A #\B #\C #\D
;;                                          #\X #\_ #\_ #\_ #\e))))))
;;   (read-name-string test))

;; (let ((test (make-bytestream
;;              (u8-list->bytevector (map char->integer
;;                                        '(#\\ #\/ #\x03 #\A #\B #\C #\D
;;                                          #\X #\Y #\Z #\W
;;                                          #\C #\M #\S #\_ #\e))))))
;;   (read-name-string test))

(define (read-field port)
  (let ((byte (get-u8 port)))
    (case byte
      ((#x00)                           ; reserved field
       (list 'reserved (read-package-length port)))
      ((#x01)                           ; access field???
       (list 'access byte (get-u8 port)))
      (else
       (let* ((byte2 (get-u8 port))
              (byte3 (get-u8 port))
              (byte4 (get-u8 port))
              (length (read-package-length port)))
         (list 'named
               (list->string (map integer->char
                                  (list byte byte2 byte3 byte4)))
               length))))))

;; TODO: better table format
(define acpi-opcodes
  '((#x00 . #(0 constant () #f))
    (#x01 . #(1 constant () #f))
    (#x06 . #(define-alias term-object (name-string name-string) #f))
    (#x08 . #(define-name term-object (name-string term-arg) #f))
    (#x0a . #(byte-prefix data-object (byte-data) #f))
    (#x0b . #(word-prefix data-object (word-data) #f))
    (#x0c . #(dword-prefix data-object (dword-data) #f))
    (#x0d . #(string-prefix data-object (asciiz-data) #f))
    (#x0e . #(qword-prefix data-object (qword-data) #f))
    (#x10 . #(let-scope term-object (name-string) term-list))
    (#x11 . #(buffer term-object (term-arg) byte-list))
    (#x12 . #(make-package term-object () package))
    (#x13 . #(make-var-package term-object (term-arg) var-package))
    (#x14 . #(define-method term-object (name-string byte-data) term-list))
    (#x2e . #(resolve-name name-object (name-string) #f))
    (#x2f . #(resolve-name name-object (name-string) #f))
    (#x5b01 . #(define-mutex term-object (name-string byte-data) #f))
    (#x5b02 . #(define-event term-object (name-string) #f))
    (#x5b12 . #(cond-ref-of-op term-object (super-name super-name) #f))
    (#x5b13 . #(create-field-op term-object (term-arg term-arg term-arg name-string) #f))
    (#x5b1f . #(load-table-op term-object (term-arg term-arg term-arg term-arg term-arg term-arg) #f))
    (#x5b20 . #(load-op term-object (name-string super-name) #f))
    (#x5b21 . #(stall-op term-object (term-arg) #f))
    (#x5b22 . #(sleep-op term-object (term-arg) #f))
    (#x5b23 . #(acquire-op term-object (super-name word-data) #f))
    (#x5b24 . #(signal-op term-object (super-name) #f))
    (#x5b25 . #(wait-op term-object (super-name term-arg) #f))
    (#x5b26 . #(reset-op term-object (super-name) #f))
    (#x5b27 . #(release-op term-object (super-name) #f))
    (#x5b28 . #(from-bcd-op term-object (term-arg target) #f))
    (#x5b29 . #(to-bcd-op term-object (term-arg target) #f))
    (#x5b2a . #(unload-op term-object (super-name) #f))
    (#x5b30 . #(revision-op data-object () #f))
    (#x5b31 . #(debug-op debug-object () #f))
    (#x5b32 . #(fatal-op term-object (byte-data dword-data term-arg) #f))
    (#x5b33 . #(timer-op term-object () #f))
    (#x5b80 . #(define-op-region term-object (name-string byte-data term-arg term-arg) #f))
    (#x5b81 . #(define-field term-object (name-string byte-data) field-list))
    (#x5b82 . #(define-device term-object (name-string) object-list))
    (#x5b83 . #(define-processor term-object (name-string byte-data dword-data byte-data) object-list))
    (#x5b84 . #(define-power-resp term-object (name-string byte-data word-data) object-list))
    (#x5b85 . #(define-thermal-zone term-object (name-string) object-list))
    (#x5b86 . #(define-index-field term-object (name-string name-string byte-data) field-list))
    (#x5b87 . #(define-bank-field term-object (name-string name-string term-arg byte-data) field-list))
    (#x5b88 . #(define-data-region term-object (name-string term-arg term-arg term-arg) #f))
    (#x5c . #(resolve-name name-object (name-string) #f))
    (#x5e . #(resolve-name name-object (name-string) #f))
    (#x5f . #(resolve-name name-object (name-string) #f))
    (#x60 . #(local0-op local-object () #f))
    (#x61 . #(local1-op local-object () #f))
    (#x62 . #(local2-op local-object () #f))
    (#x63 . #(local3-op local-object () #f))
    (#x64 . #(local4-op local-object () #f))
    (#x65 . #(local5-op local-object () #f))
    (#x66 . #(local6-op local-object () #f))
    (#x67 . #(local7-op local-object () #f))
    (#x68 . #(arg0-op arg-object () #f))
    (#x69 . #(arg1-op arg-object () #f))
    (#x6a . #(arg2-op arg-object () #f))
    (#x6b . #(arg3-op arg-object () #f))
    (#x6c . #(arg4-op arg-object () #f))
    (#x6d . #(arg5-op arg-object () #f))
    (#x6e . #(arg6-op arg-object () #f))
    (#x70 . #(store-op term-object (term-arg super-name) #f))
    (#x71 . #(ref-of-op term-object (super-name) #f))
    (#x72 . #(add term-object (term-arg term-arg target) #f))
    (#x73 . #(concat-op term-object (term-arg term-arg target) #f))
    (#x74 . #(subtract term-object (term-arg term-arg target) #f))
    (#x75 . #(increment term-object (super-name) #f))
    (#x76 . #(decrement term-object (super-name) #f))
    (#x77 . #(multiply-op term-object (term-arg term-arg target) #f))
    (#x78 . #(divide-op term-object (term-arg term-arg target target) #f))
    (#x79 . #(shift-left-op term-object (term-arg term-arg target) #f))
    (#x7a . #(shift-right-op term-object (term-arg term-arg target) #f))
    (#x7b . #(and-op term-object (term-arg term-arg target) #f))
    (#x7c . #(nand-op term-object (term-arg term-arg target) #f))
    (#x7d . #(or-op term-object (term-arg term-arg target) #f))
    (#x7e . #(nor-op term-object (term-arg term-arg target) #f))
    (#x7f . #(xor-op term-object (term-arg term-arg target) #f))
    (#x80 . #(not-op term-object (term-arg target) #f))
    (#x81 . #(find-set-left-bit-op term-object (term-arg target) #f))
    (#x82 . #(find-set-right-bit-op term-object (term-arg target) #f))
    (#x83 . #(deref-op term-object (term-arg) #f))
    (#x84 . #(concatres-op term-object (term-arg term-arg target) #f))
    (#x85 . #(mod-op term-object (term-arg term-arg target) #f))
    (#x86 . #(notify-op term-object (super-name term-arg) #f))
    (#x87 . #(sizeof-op term-object (super-name) #f))
    (#x88 . #(index-op term-object (term-arg term-arg target) #f))
    (#x89 . #(match-op term-object (term-arg byte-data term-arg) #f))
    (#x8a . #(create-dword-field-op term-object (term-arg term-arg name-string) #f))
    (#x8b . #(create-word-field-op term-object (term-arg term-arg name-string) #f))
    (#x8c . #(create-byte-field-op term-object (term-arg term-arg name-string) #f))
    (#x8d . #(create-bit-field-op term-object (term-arg term-arg name-string) #f))
    (#x8e . #(object-type-op term-object (super-name) #f))
    (#x8f . #(create-qword-field-op term-object (term-arg term-arg name-string) #f))
    (#x90 . #(land-op term-object (term-arg term-arg) #f))
    (#x91 . #(lor-op term-object (term-arg term-arg) #f))
    (#x92 . #(lnot-op term-object (term-arg) #f))
    (#x93 . #(lequal-op term-object (term-arg term-arg) #f))
    (#x94 . #(lgreater-op term-object (term-arg term-arg) #f))
    (#x95 . #(lless-op term-object (term-arg term-arg) #f))
    (#x96 . #(to-buffer-op term-object (term-arg target) #f))
    (#x97 . #(to-decimal-string-op term-object (term-arg target) #f))
    (#x98 . #(to-hex-string-op term-object (term-arg target) #f))
    (#x99 . #(to-integer-op term-object (term-arg target) #f))
    (#x9c . #(to-string-op term-object (term-arg term-arg target) #f))
    (#x9d . #(copy-object-op term-object (term-arg term-arg) #f))
    (#x9e . #(mid-op term-object (term-arg term-arg term-arg target) #f))
    (#x9f . #(continue-op term-object () #f))
    (#xa0 . #(if-op term-object (term-arg) term-list))
    (#xa1 . #(else-op term-object () term-list))
    (#xa2 . #(while-op term-object (term-arg) term-list))
    (#xa3 . #(noop-op term-object () #f))
    (#xa4 . #(return-op term-object (term-arg) #f))
    (#xa5 . #(break-op term-object () #f))
    (#xcc . #(breakpoint-op term-object () #f))
    (#xff . #(-1 constant () #f))))

(define (get-bytecode port)
  (let ((byte (get-u8 port)))
    (cond ((eof-object? byte)
           byte)
          ((= byte #x5b)
           (logior (ash byte 8) (get-u8 port)))
          (else byte))))

(define (lookup-opcode opcode)
  (cond ((assq opcode acpi-opcodes) => cdr)
        ((<= #x41 opcode #x5a)
         '#(resolve-name name-object (name-string) #f))
        (else #f)))

(define (read-fixed-argument type port)
  (case type
    ((name-string)
     (read-name-string port))
    ((term-arg)
     (let ((term (read-term-arg port)))
       (cond ((and (pair? term) (eq? (car term) 'resolve-name))
              ;; Check for function calls here
              (if (and (string? (caadr term)) (string=? (caadr term) "CMRD"))
                  (list 'FUNCALL term (read-term-arg port))
                  term))
             (else term))))
    ((byte-data)  (get-u8 port))
    ((word-data)  (bytevector-u16-ref (get-bytevector-n port 2) 0 (endianness little)))
    ((dword-data) (bytevector-u32-ref (get-bytevector-n port 4) 0 (endianness little)))
    ((qword-data) (bytevector-u64-ref (get-bytevector-n port 8) 0 (endianness little)))
    ((super-name)
     (read-term-arg port))
    ((target)
     (read-term-arg port))
    ((asciiz-data)
     (let lp ((bytes '()))
       (let ((byte (get-u8 port)))
         (if (zero? byte)
             (list->string (map integer->char (reverse bytes)))
             (lp (cons (logand #x7f byte) bytes))))))
    (else
     (error 'read-fixed-argument
            "unknown fixed argument type" type))))

(define (read-variable-argument type end-pos port)
  (case type
    ((term-list object-list)
     (let lp ((terms '()))
       (if (< (port-position port) end-pos)
           (lp (cons (read-term-arg port) terms))
           (reverse terms))))
    ((field-list)
     (let lp ((fields '()))
       (if (< (port-position port) end-pos)
           (lp (cons (read-field port) fields))
           (reverse fields))))
    ((package)
     (let ((elements (get-u8 port)))
       (let lp ((ret '()))
         (if (< (port-position port) end-pos)
             (lp (cons (read-term-arg port) ret))
             (list 'package elements (reverse ret))))))
    ((byte-list)
     (get-bytevector-n port (- end-pos (port-position port))))
    (else
     (error 'read-term-arg
            "unknown variable argument type" type))))

(define (read-term-arg port)
  (let* ((start-position (port-position port))
         (bytecode (get-bytecode port)))
    (cond ((eof-object? bytecode)
           (error 'read-term-arg "premature end of AML code"))
          ((lookup-opcode bytecode) =>
           (lambda (op)
             (when (eq? (vector-ref op 1) 'name-object)
               (set-port-position! port start-position))
             (case (vector-ref op 1)
               ((term-object arg-object local-object name-object)
                (let* ((position (port-position port))
                       (end-pos (and (vector-ref op 3)
                                     (+ position (read-package-length port)))))
                  
                  ;; (print op " " start-position "-" end-pos)
                  (let* ((operand (vector-ref op 0))
                         (fixed-args (map-in-order (cut read-fixed-argument <> port)
                                                   (vector-ref op 2)))
                         (varargs (if (vector-ref op 3)
                                      (list (read-variable-argument (vector-ref op 3)
                                                                    end-pos port))
                                      '())))
                    ;;  (print (list start-position end-pos operand fixed-args '...))
                    ;;  (thread-yield!)
                    (cons operand (append fixed-args varargs)))))
               ((constant)
                (vector-ref op 0))
               ((data-object)
                (read-fixed-argument (car (vector-ref op 2)) port))
               (else
                (error 'read-term-arg "Unknown opcode type" op)))))
          (else
           (error 'read-term-arg "Unknown bytecode" bytecode)))))

;;; Get all tables

(define (locate-rsdp bv)
  (do ((i 0 (+ i 16)))
      ((or (>= i (bytevector-length bv))
           ;; Check for "RSD PTR "
           (and (= #x5352 (bytevector-u16-ref bv (+ i 0) (endianness little)))
                (= #x2044 (bytevector-u16-ref bv (+ i 2) (endianness little)))
                (= #x5450 (bytevector-u16-ref bv (+ i 4) (endianness little)))
                (= #x2052 (bytevector-u16-ref bv (+ i 6) (endianness little)))
                ;; TODO: check the revision. The length seems to have
                ;; changed between some revisions...
                ;; Checksum:
                (zero?
                 (logand #xff (apply + (bytevector->u8-list
                                        (bytevector-section bv i (+ i 20))))))))
       (and (< i (bytevector-length bv))
            (bytevector-section bv i (+ i 36))))))

;; The RSDP points at the RSDT or the XSDT
(define rsdp-table                      ;Root System Description Pointer Structure
  (or (locate-rsdp (make-bytevector/direct 1024 #x9fc00)) ;Extended BIOS Data Area
      (locate-rsdp (make-bytevector/direct #x18000 #xe8000)))) ;BIOS read-only memory

;; The RSDT contains a list of pointers to tables with the generic
;; header. It points to the FADT.
(define rsdt-table #f)                  ;Root System Description Table
;; The FADT points to the DSDT and the FACS tables.
(define fadt-table #f)                  ;Fixed ACPI Description Table
(define dsdt-table #f)                  ;Differentiated System Description Table
(define facs-table #f)                  ;Firmware ACPI Control Structure

(define (acpi-get-table address)
  "Get the ACPI table at the given address. Returns the header
and the table as two values."
  ;; TODO: make sure the address is addressable
  (let ((header (make-bytevector/direct acpi-dh-size address)))
    ;; TODO: verify the checksum (the sum of the bytes is zero)
    (make-bytevector/direct (acpi-dh-length-ref header) address)))

(define (acpi-get-initial-tables)
  (cond ((and rsdp-table (not (zero? (rsdp-rsdt-address-ref rsdp-table))))
         (set! rsdt-table (acpi-get-table (rsdp-rsdt-address-ref rsdp-table)))
         (print "Found ACPI, loading tables...")
         (do ((i acpi-dh-size (+ i 4)))
             ((= i (bytevector-length rsdt-table)))
           (let* ((table (acpi-get-table (bytevector-u32-ref rsdt-table i (endianness little))))
                  (data (bytevector-section/shared table acpi-dh-size (bytevector-length table)))
                  (signature (acpi-dh-signature-ref table)))
             (case (string->symbol signature)
               ;; TODO: treat SSDT tables
               ((FACP)
                (set! fadt-table table)
                (set! dsdt-table (acpi-get-table (fadt-dsdt-address-ref data)))
                (set! facs-table (acpi-get-table (fadt-facs-address-ref data))))
               (else
                (print "Unhandled table: " signature))))))
        ((and rsdp-table (not (zero? (rsdp-xsdt-address-ref rsdp-table))))
         ;; TODO: XSDT is preferred over RSDT
         (print "Fetch the XSDT!!"))
        (else
         (print "No ACPI support detected."))))

(acpi-get-initial-tables)

(define (acpi-test)
  (define tmp (make-bytestream
               (bytevector-section dsdt-table acpi-dh-size
                                   (bytevector-length dsdt-table))))
  (define namespace (make-acpi-tree-with-predefines))
  (let lp ()
    
    (if (eof-object? (lookahead-u8 tmp))
        '()
        (begin
          (write (read-term-arg tmp namespace '()))
          (newline)
          (lp)))))
