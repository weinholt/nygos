;; -*- coding: utf-8 -*-
;; The Unbelievable Operating System from Hell
;; Copyright © 2007 Göran Weinholt <goran@weinholt.se>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; RTL8139 network interface card driver

(define-bytevector-struct rtl8139-regs (endianness little)
  (u8 IDR0-ref) (u8 IDR1-ref) (u8 IDR2-ref) ; NIC hardware address
  (u8 IDR3-ref) (u8 IDR4-ref) (u8 IDR5-ref)
  (u16 res0-ref)
  (u32 MAR0..3-ref MAR0..3-set!)        ; Multicast address filter
  (u32 MAR4..7-ref MAR4..7-set!)
  (u32 TSD0-ref TSD0-set!)
  (u32 TSD1-ref TSD1-set!)
  (u32 TSD2-ref TSD2-set!)
  (u32 TSD3-ref TSD3-set!)
  (u32 TSAD0-ref TSAD0-set!)
  (u32 TSAD1-ref TSAD1-set!)
  (u32 TSAD2-ref TSAD2-set!)
  (u32 TSAD3-ref TSAD3-set!)
  (u32 RBSTART-ref RBSTART-set!)        ; Receive Buffer Start Address
  (u16 ERBCR-ref)
  (u8 ERSR-ref)
  (u8 CR-ref CR-set!)                   ; Command Register
  (u16 CAPR-ref CAPR-set!)              ; Current Adress of Packet Read
  (u16 CBR-ref)
  (u16 IMR-ref IMR-set!)                ; Interrupt Mask Register
  (u16 ISR-ref ISR-set!)                ; Interrupt Status Register
  (u32 TCR-ref)                         ; Transmit Config Register
  (u32 RCR-ref RCR-set!)                ; Receive Config Register
  (u32 TCTR-ref)
  (u32 MPC-ref)
  (u8 9346CR-ref)
  (u8 CONFIG0-ref CONFIG0-set!)
  (u8 CONFIG1-ref CONFIG1-set!)
  (u8 res1-ref)
  (u32 TimerInt-ref)
  (u8 MSR-ref)                          ; Media status
  (u8 CONFIG3-ref)
  (u8 CONFIG4-ref)
  (u8 res2-ref)
  (u16 MULINT-ref)
  (u8 RERID-ref)
  (u8 res3-ref)
  (u16 TSAD-ref)                        ; Transmit Status of All Descriptors
  (u16 BMCR-ref BMCR-set!)              ; Basic Mode Control Register
  (u16 BMSR-ref)                        ; Basic Mode Status Register
  (u16 ANAR-ref)
  (u16 ANLPAR-ref)
  (u16 ANER-ref)
  ;; ...
  )

(define-bytevector-struct rtl8139-regs-mac (endianness big)
  (u48 IDR-ref))

(define (TSDn-ref regs n)
  (bytevector-u32-ref regs (+ #x10 (* n 4)) (endianness little)))
(define (TSADn-ref regs n)
  (bytevector-u32-ref regs (+ #x20 (* n 4)) (endianness little)))
(define (TSDn-set! regs n v)
  (bytevector-u32-set! regs (+ #x10 (* n 4)) v (endianness little)))
(define (TSADn-set! regs n v)
  (bytevector-u32-set! regs (+ #x20 (* n 4)) v (endianness little)))

;; Status register in the Rx packet header
(define rx-mcast    #b100000000000000)
(define rx-phymatch  #b10000000000000)
(define rx-bcast      #b1000000000000)
(define rx-invsym-error      #b100000)
(define rx-runt-error         #b10000)
(define rx-long-error          #b1000)
(define rx-crc-error            #b100)
(define rx-frame-alignment-error #b10)
(define rx-ok                     #b1)
(define rx-errors            #b111110)

;; (define tx-carrier-sense-lost #b)
;; (define tx-transmit-abort #b)
;; (define tx-out-of-window-collision)
;; (define tx-cd-heart-beat)

(define (rtl8139-reset regs rxbuf)
  ;; Reset
  (CR-set! regs #b10000)
  (do ((try 0 (+ try 1)))
      ((or (= try 10) (not (logtest (CR-ref regs) #b10000))))
    (thread-msleep! 10))
  (when (logtest (CR-ref regs) #b10000)
    (error 'rtl8139-init "timeout when resetting"))

  ;; Disable interrupts for now
  (IMR-set! regs #b1)

  ;; Set receive buffer
  (RBSTART-set! regs (bytevector-dev-addr rxbuf))

  ;; Enable receiver and transmitter
  (CR-set! regs #b1100)

  ;; Accept broadcast, multicast and our hardware address, WRAP=1
  (RCR-set! regs #b10001110))

(define (rtl8139-init driver dev)
  (let* ((regs (pci-device-get-resource dev 0))
         (irq (pci-device-irq dev))
         (rxbuf (dma-alloc 65535 dev))
         (txbuf (dma-alloc 8192 dev))
         (interface (register-ethernet-interface (IDR-ref regs))))
    (print (interface-name interface) ": RTL8139 NIC found with MAC "
           (number->string (IDR0-ref regs) 16) #\:
           (number->string (IDR1-ref regs) 16) #\:
           (number->string (IDR2-ref regs) 16) #\:
           (number->string (IDR3-ref regs) 16) #\:
           (number->string (IDR4-ref regs) 16) #\:
           (number->string (IDR5-ref regs) 16) " at IRQ " irq)

    (rtl8139-reset regs rxbuf)
    (MAR0..3-set! regs #xFFFFFFFF)      ;accept all multicast packets
    (MAR4..7-set! regs #xFFFFFFFF)

    (cond ((logtest (MSR-ref regs) #b100)
           (print (interface-name interface) ": link down"))
          (else
           (print (interface-name interface) ": "
                  (if (logtest (MSR-ref regs) #b1000)
                      10 100)
                  " Mbps, "
                  (if (logtest (BMCR-ref regs) #b100000000)
                      "full" "half")
                  "-duplex.")))

    (set-irq-handler! irq (self))
    (irq-acknowledge! irq)
    (let ((buflen 8192)
          (offset 0)
          (tx-desc 0)
          (irq? (lambda (x) (eq? x irq))))
      (forever
        (match (?)
          ((? irq? _)
           (while (= (logand (CR-ref regs) #b1) 0)
             (let ((status (bytevector-u16-ref rxbuf offset (endianness little)))
                   (size (bytevector-u16-ref rxbuf (+ offset 2) (endianness little))))
               (cond ((logtest status rx-errors)
                      (print (interface-name interface)
                             ": Error in packet... status=" status))
                     (else
                      (let ((packet (make-bytevector (- size 4))))
                        (bytevector-copy! rxbuf (+ offset 4) ; skip the receive header
                                          packet 0
                                          (- size 4)) ; skip FCS
                        (process-ethernet-frame interface packet))))
               (set! offset (modulo (align (+ offset size 4) 4)
                                    buflen))
               (CAPR-set! regs (logand #xffff (- offset 16)))))
           (ISR-set! regs #b1110000001111111) ; clear the interrupt causes
           (irq-acknowledge! irq))

          (('send (? list? parts))
           ;; `parts' is a list of bytevectors that together
           ;; constitute an ethernet frame.
           (let lp ((size 0)
                    (parts parts))
             (cond ((and (null? parts) (< size 60))
                    ;; FIXME: fill out the buffer with NULs. 64 bytes
                    ;; is the minimum for Ethernet frames and the card
                    ;; fills out the FCS itself.
                    (lp 60 '()))
                   ((null? parts)
                    ;; Send the frame
                    (TSADn-set! regs tx-desc (bytevector-dev-addr txbuf))
                    ;; FIXME: check MTU size, the maximum for rtl8139 is 1792 bytes
                    (TSDn-set! regs tx-desc size)

                    ;; FIXME: switch tx descriptor based on tx irq's
                    (set! tx-desc (modulo (+ tx-desc 1) 4)))
                   (else
                    (let ((len (bytevector-length (car parts))))
                      (bytevector-copy! (car parts) 0
                                        txbuf size
                                        len)
                      (lp (+ size len) (cdr parts)))))))

          ((from tag 'get-hwaddr)
           (! from (list tag (IDR-ref regs))))
          (_ #f))))))

(register-driver
 (make-pci-driver "RTL8139 NIC driver"
                  '((#x10ec #x8139 #f #f #f #f #f))
                  rtl8139-init))
