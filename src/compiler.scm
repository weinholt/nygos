;; -*- coding: utf-8 -*-
;; The Unbelievable Operating System from Hell
;; Copyright © 2006-2007 Göran Weinholt <goran@weinholt.se>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; A compiler that builds a heap and lots of code.
;; Not implemented yet: call/cc, dynamic-wind

;; The value tagging and most real improvements to the compiler were
;; inspired by Abdulaziz Ghuloum's "An Incremental Approach to
;; Compiler Construction" from the 2006 Scheme and Functional
;; Programming Papers.

(define (unspecified)
  (if #f #f))

(define eof-object
  (let ((x (read (open-input-string ""))))
    (lambda ()
      x)))

(use-modules (srfi srfi-1) (srfi srfi-9) (srfi srfi-13)
             (srfi srfi-14) (srfi srfi-39)
             (ice-9 pretty-print) (ice-9 optargs)
             (ice-9 format))
(debug-set! stack 200000)              ; lest we get a stack overflow
(load "srfi-60.scm")
(load "srfi-69.scm")

;; Guile is an interpreter, so these guys are like LOAD:
(define (include file)
  ;; Load with our own reader, since Guile's reader lacks some stuff
  (with-input-from-file file
    (lambda ()
      (do ((expr (read) (read)))
          ((eof-object? expr))
        (eval expr (interaction-environment))))))
(define (conditional-include file pred?)
  (with-input-from-file file
    (lambda ()
      (do ((expr (read) (read)))
          ((eof-object? expr))
        (if (and (pair? expr) (pred? expr))
            (eval expr (interaction-environment)))))))

;;; Load our custom READ function
(conditional-include "port.scm"
                     (lambda (expr)
                       (and (eq? (car expr) 'define)
                            (not (memq (caadr expr)
                                       '(write write-char newline
                                               display eof-object?))))))

(define (print-error . x)
  (with-output-to-port (current-error-port)
    (lambda ()
      (apply print x))))

;;; Load our compiler passes
(include "compiler-passes.scm")

;;; Load Al Petrofsky's much finer macros
(include "alexpander.scm")

(define mstore (null-mstore))

(define (macro-expansion-pass exprs)
  (expand-top-level-forms! exprs mstore))

(use-modules (ice-9 syncase))
(load "syntax.scm")
(load "srfi-26.scm")
(load "match.scm")

;;; Bytevectors

(define-record-type bytevector (really-make-bytevector vec) bytevector?
  (vec bytevector-vec))

(define (u8-list->bytevector l)
  (really-make-bytevector
   (list->vector
    (map (lambda (x)
           (if (not (and (number? x)
                         (<= -128 x 255)))
               (error "This bytevector contains illegal numbers"
                      x l))
           (if (negative? x)
               (+ 256 x)
               x))
         l))))

(define (bytevector-length bv)
  (vector-length (bytevector-vec bv)))

(define (bytevector->u8-list bv)
  (vector->list (bytevector-vec bv)))

;;; Encoding of the expressions:

(define-record-type prim (make-primitive name label reqargs) prim?
  (name prim-name)
  (label prim-label)
  (reqargs prim-reqargs))

(define closure-offset 3)

;; There is a copy of these constants in malloc.h!

(define FIXNUM-MASK        #b11)
(define FIXNUM-TAG         #b00)

(define CHAR-MASK    #b11111111)
(define CHAR-TAG     #b00001111)

(define BOOL-MASK    #b11111111)
(define BOOL-TAG     #b00011111)

(define FALSE-VALUE #b000011111)
(define TRUE-VALUE  #b100011111)

(define EOL-VALUE    #b00101111)
(define UNSPEC-VALUE #b01001111)
(define EOF-VALUE    #b01101111)

;; Heap-allocated values are allocated on 16-byte boundaries

(define HEAP-MASK    #b1111)
;; FIXNUM            #b0000
(define PAIR-TAG     #b0001)            ; mutable
(define VECTOR-TAG   #b0010)            ; mutable
(define BYTES-TAG    #b0011)            ; mutable
;; FIXNUM            #b0100
(define SYMBOL-TAG   #b0101)            ; immutable
(define PROC-TAG     #b0110)            ; immutable
(define NUMBER-TAG   #b0111)            ; immutable
;; FIXNUM            #b1000
;; available         #b1001
(define RECORD-TAG   #b1010)            ; mutable if there are mutators
;; available         #b1011
;; FIXNUM            #b1100
(define THREAD-TAG   #b1101)            ; mutable
(define STRING-TAG   #b1110)            ; mutable
;; CHAR/BOOL/etc.    #b1111


(define alignment 16)
(define word-size 8)
(define INT-MAX (- (arithmetic-shift 1 61) 1))
(define INT-MIN (- 0 INT-MAX 1))
;;(define INT-MAX #b+011111111111111111111111111111)
;;(define INT-MIN #b-100000000000000000000000000000)
(define BIGINT-MAX 9223372036854775807)
(define BIGINT-MIN (- 0 BIGINT-MAX 1))

(define (align n by)
  (+ n (- by (modulo n by))))

(define (caching-symbol-hash x . maybe-bound)
  ;; Uses guile's symbol property lists to cache the hash.
  (let ((bound (if (null? maybe-bound) *default-bound* (car maybe-bound))))
    (cond ((symbol-property x 'cached-hash) =>
           (lambda (hash)
             (modulo hash bound)))
          (else
           (let ((hash (symbol-hash x bound)))
             (set-symbol-property! x 'cached-hash hash)
             (modulo hash bound))))))

(define symbols (make-hash-table eq? caching-symbol-hash))
(define symbols-obarray (make-vector 512 '()))

(define environment (make-vector 512 '()))

(define (top-level-define name . rest)
  (let-optional rest ((value (unspecified)))
    (let* ((hash (caching-symbol-hash name))
           (index (modulo hash (vector-length environment))))
      (vector-set! environment index
                   (cons (cons name value)
                         (vector-ref environment index))))))

(define (top-level-defined? name)
  (let* ((hash (caching-symbol-hash name))
         (index (modulo hash (vector-length environment)))
         (bucket (vector-ref environment index)))
    (assq name bucket)))

;; Encoding of different types:

(define (string-encode str)
  (values STRING-TAG
          (u8-list->bytevector
           (append-map!
            (lambda (c)
              (list (ash (char->integer c) -16)
                    (logand #xff (ash (char->integer c) -8))
                    (logand #xff (char->integer c))))
            (string->list str)))))

(define (symbol-encode sym)
  (let* ((str (symbol->string sym))
         (hash (caching-symbol-hash sym))
         (index (modulo hash (vector-length symbols-obarray))))
    (cond ((hash-table-ref/default symbols sym #f)
           (values SYMBOL-TAG 'already-encoded))
          (else
           (vector-set! symbols-obarray index
                        (cons (cons sym '())
                              (vector-ref symbols-obarray index)))
           (values SYMBOL-TAG
                   (list (encode str)
                         (encode hash)))))))

(define (pair-encode pair)
  (values PAIR-TAG
          (list (encode (car pair))
                (encode (cdr pair)))))

(define (vector-encode vector)
  (values VECTOR-TAG
          (cons (encode (vector-length vector))
                (map! encode (vector->list vector)))))

(define (number-encode number)
  (if (or (< number BIGINT-MIN)
          (> number BIGINT-MAX))
      (error "can't encode number" number)
      ;; FIXME: this is only little-endian
      (values NUMBER-TAG
              (list (logand #xFFFFffff number)
                    (ash number -32)))))

(define (prim-encode proc)
  (values PROC-TAG
          (list (encode (prim-name proc))
                (string-append "(" (prim-label proc) ")")
                (encode (prim-reqargs proc))
                (encode '()))))

(define (bytevector-encode bv)
  ;; A bit complicated since this, unlike everything else, requires
  ;; that bytes instead of longs are encoded. Also, the object
  ;; references itself.
  (values BYTES-TAG bv))

(define (encode v)
  (cond
   ;; Immediates:
   ((null? v) EOL-VALUE)
   ((eq? v #f) FALSE-VALUE)
   ((eq? v #t) TRUE-VALUE)
   ((eq? v (unspecified)) UNSPEC-VALUE)
   ((eof-object? v) EOF-VALUE)
   ((char? v)
    (let ((i (char->integer v)))
      (if (or (<= 0 i #xD7FF) (<= #xE000 i #x10FFFF))
          (bitwise-ior (ash i 8) CHAR-TAG)
          (error "can't encode illegal character" i))))
   ((number? v)
    (if (or (< v INT-MIN)
            (> v INT-MAX))
        (allocate v)
        (bitwise-ior (ash v 2) FIXNUM-TAG)))
   ;; Non-immediates
   ((pair? v)       (allocate v))
   ((vector? v)     (allocate v))
   ((string? v)     (allocate v))
   ((symbol? v)     (allocate v))
   ((prim? v)       (allocate v))
   ((bytevector? v) (allocate v))
   (else
    (error "can't encode" v))))

;; Special case for references to the pairs in the environment
(define doing-environment #f)

(define (environment-pair-name name)
  ;; Very ugly code, replaces non-alphabetic characters with hex
  ;; Makes it suitable for use as an assembler label.
  (string->symbol
   (string-append/shared
    "env_"
    (string-concatenate-reverse/shared
     (string-fold (lambda (c tail)
                    (cons (if (char-alphabetic? c)
                              (make-string 1 c)
                              (number->string (char->integer c) 16))
                          tail))
                  '()
                  (symbol->string name))))))

(define (allocate value)
  (define (encode-nonimm v)
    (cond ((pair? v)       (pair-encode v))
          ((string? v)     (string-encode v))
          ((symbol? v)     (symbol-encode v))
          ((vector? v)     (vector-encode v))
          ((number? v)     (number-encode v))
          ((prim? v)       (prim-encode v))
          ((bytevector? v) (bytevector-encode v))
          (else            (error "can't allocate" v))))
  (receive (type-tag encoded) (encode-nonimm value)
    (define (label+type label)
      (string-append "(" (symbol->string label)
                     " + " (number->string type-tag) ")"))
    (cond ((and (symbol? value) (hash-table-ref/default symbols value #f)) =>
           label+type)
          (else
           (let ((label (gensym "scm")))
             ;; References to pairs in the environment
             (if (and doing-environment
                      (pair? value)
                      (or (eq? (cdr value) (unspecified))
                          (prim? (cdr value))
                          (vector? (cdr value))))
                 (set! label (environment-pair-name (car value))))
             ;; Interning
             (if (symbol? value) (hash-table-set! symbols value label))
             (print " .data")
             (print " .align " alignment)
             (print label ":")
             (cond ((bytevector? encoded)
                    ;; Special treatment for bytevectors, since they
                    ;; do not consist of word-sized parts and
                    ;; reference themselves.
                    (let ((dataloc (gensym "bv")))
                      (print " .quad " dataloc)
                      (print " .quad " (bytevector-length encoded))
                      (print dataloc ":")
                      (print " .byte " (string-join (map number->string
                                                         (bytevector->u8-list encoded))
                                                    ","))))
                   (else
                    (print " .quad "
                           (string-join (map (lambda (x)
                                               (if (number? x)
                                                   (number->string x)
                                                   x))
                                             encoded)
                                        ","))))
             (print " .text")
             (label+type label))))))

;;; The code generator lives below here

(define %reg-numargs "%rbp")            ; number of arguments passed
(define %reg-closure "%rbx")            ; current closure
(define %reg-value "%rax")              ; return value register
(define %reg-stack "%rsp")              ; stack register
(define %reg-thread "%r15")             ; current thread

(define (emit fmt . args)
  (display " ")
  (display (apply format fmt args))
  (newline))

(define (emit-label label)
  (display label)
  (display ":")
  (newline))

(define (emit-push what)
  (display " pushq ")
  (display what)
  (newline))

(define (emit-jump where)
  (display " jmp ")
  (display where)
  (newline))

(define (emit-move src dest)
  (display " movq ")
  (display src)
  (display ",")
  (display dest)
  (newline))

;;(define (emit-body body lex lambda-level tail-pos))

(define (constant-encode x)
  (string-append "$" (number->string x)))

(define (label-encode x)
  (string-append "$" (symbol->string x)))

(define (type-ref offset type)
  (- (* word-size offset) type))

(define (compile-lookup var lex si)
  "Returns the address of the given variable, possibly relative
to some register."
  (cond ((assq var lex) =>
         (lambda (l)
           (cond ((pair? (cdr l))
                  ;; Closure argument
                  (format "~a(~a)" (type-ref (+ (cddr l) 1) PROC-TAG) %reg-closure))
                 (else
                  ;; Local variable or procedure argument
                  (format "~a(%rsp)" (- si (cdr l) word-size))))))
        (else
         (unless (top-level-defined? var)
           (print-error "possible reference to undefined variable: " var)
           (top-level-define var))
         ;; Free variable
         (string-append (symbol->string (environment-pair-name var))
                        "+" (number->string word-size)))))

(define (extend-env* var where old)
  (cons (cons var where) old))

(define (emit-body body env si tail-pos)
  (let lp ((exprs body))
    (receive (first rest) (car+cdr exprs)
      (cond ((and (pair? rest) (or (literal? first)
                                   (variable? first)))
             ;; ignore literals and variables in bodies
             (lp rest))
            (else
             (compile first env si (and tail-pos (null? rest)))
             (if (pair? rest)
                 (lp rest)))))))

(define gen-label
  (let ((x 0))
    (lambda ()
      (set! x (+ x 1))
      (string-append ".L" (number->string x)))))

(define (code-formals x) (cadr x))
(define (code-closed x) (caddr x))
(define (code-body x) (cdddr x))

(define-syntax define-primitive
  (syntax-rules ()
    ((define-primitive (name . formals) lex si
       body ...)
     (begin
       (set! *primitive-defines*
             (cons `(define (name . formals) (name . formals))
                   *primitive-defines*))
       (set! *primitives*
             (cons (cons 'name (lambda (lex si . formals)
                                 body ...))
                   *primitives*))))))

(define *primitives* '())
(define *primitive-defines* '())

(define (primitive-operation? expr)
  (and (symbol? (car expr))
       (assq (car expr) *primitives*)))

(define (compile-primitive-operation op expr lex si)
  (apply (cdr op) lex si (cdr expr)))

(define (emit-equal-flag->boolean)
  (emit-move (constant-encode BOOL-TAG)
             %reg-value)
  (emit "sete %ah"))

(define (emit-type-test mask tag)
  (emit "andl ~a, %eax" (constant-encode mask))
  (emit "cmpl ~a, %eax" (constant-encode tag))
  (emit-equal-flag->boolean))

(define-primitive (unspecified) lex si
  (emit-move (constant-encode (encode (unspecified)))
             %reg-value))

(define-primitive (eof-object) lex si
  (emit-move (constant-encode (encode (eof-object)))
             %reg-value))

(define-primitive (fixnum? x) lex si
  (compile x lex si #f)
  (emit-type-test FIXNUM-MASK FIXNUM-TAG))

(define-primitive (pair? x) lex si
  (compile x lex si #f)
  (emit-type-test HEAP-MASK PAIR-TAG))

(define-primitive (char? x) lex si
  (compile x lex si #f)
  (emit-type-test CHAR-MASK CHAR-TAG))

(define-primitive (string? x) lex si
  (compile x lex si #f)
  (emit-type-test HEAP-MASK STRING-TAG))

(define-primitive (vector? x) lex si
  (compile x lex si #f)
  (emit-type-test HEAP-MASK VECTOR-TAG))

(define-primitive (record? x) lex si
  (compile x lex si #f)
  (emit-type-test HEAP-MASK RECORD-TAG))

(define-primitive (symbol? x) lex si
  (compile x lex si #f)
  (emit-type-test HEAP-MASK SYMBOL-TAG))

(define-primitive (bytevector? x) lex si
  (compile x lex si #f)
  (emit-type-test HEAP-MASK BYTES-TAG))

(define-primitive (procedure? x) lex si
  (compile x lex si #f)
  (emit-type-test HEAP-MASK PROC-TAG))

(define-primitive (thread? x) lex si
  (compile x lex si #f)
  (emit-type-test HEAP-MASK THREAD-TAG))

(define-primitive (not x) lex si
  (compile x lex si #f)
  (emit "cmp $~a, %rax" (encode #f))
  (emit-equal-flag->boolean))

(define-primitive (cons x y) lex si
  (emit "// cons ~a ~a" x y)
  (compile y lex si #f)
  (emit "push %rax")
  (compile x lex (+ si word-size) #f)
  (emit "push %rax")

  (emit-push (constant-encode PAIR-TAG))
  (emit-push (constant-encode (align (* word-size 2)
                                     alignment)))
  (emit "call _new")
  (emit "addq $~a, %rsp" (* word-size 2))

  (emit "pop ~a(%rax)" (type-ref 0 PAIR-TAG))
  (emit "pop ~a(%rax)" (type-ref 1 PAIR-TAG)))

(define-primitive (rdtsc) lex si
  ;; XXX: Not a very portable function...
  (emit-push (constant-encode NUMBER-TAG))
  (emit-push (constant-encode (align (* word-size 2)
                                     alignment)))
  (emit "call _new")
  (emit "addq $~a, %rsp" (* word-size 2))
  (emit "movq %rax, %rcx")

  (emit "rdtsc")
  (emit "movl %eax, ~a(%rcx)" (type-ref 0 NUMBER-TAG))
  (emit "movl %edx, ~a(%rcx)" (type-ref 1 NUMBER-TAG))
  (emit "movq %rcx, %rax"))

(define-primitive (eq? x y) lex si
  (compile y lex si #f)
  (emit "push %rax")
  (compile x lex (+ si word-size) #f)
  (emit "pop %rcx")
  (emit "cmp %rax, %rcx")
  (emit-equal-flag->boolean))

(define-primitive (zero? x) lex si
  (compile x lex si #f)
  (emit "cmp $~a, %rax" (encode 0))
  (emit-equal-flag->boolean))

(define-primitive (null? x) lex si
  (compile x lex si #f)
  (emit "cmp $~a, %rax" (encode '()))
  (emit-equal-flag->boolean))

(define-primitive (unsafe-integer->char sv) lex si
  (compile sv lex si #f)
  (emit "shlq $6, ~a" %reg-value)
  (emit "orq ~a, ~a" (constant-encode CHAR-TAG) %reg-value))

(define-primitive (unsafe-char->integer char) lex si
  (compile char lex si #f)
  (emit "shrq $6, ~a" %reg-value)
  (emit "andq ~a, ~a" (constant-encode (- (ash 1 23) 4)) %reg-value))

(define-primitive (unsafe-string->bytevector str) lex si
  (compile str lex si #f)
  (emit "subq ~a, ~a" (constant-encode (- STRING-TAG BYTES-TAG)) %reg-value))

(define-primitive (unsafe-bytevector->string bv) lex si
  (compile bv lex si #f)
  (emit "addq ~a, ~a" (constant-encode (- STRING-TAG BYTES-TAG)) %reg-value))

(define *label-stack-indexes* (make-parameter '()))

;; compile-lookup uses `si' to see how many words have been pushed on
;; the stack since the variable being looked up was bound.
(define (compile expr lex si tail-pos)
  (cond ((literal? expr)
         (emit "movq $~a, %rax" (encode (literal-value expr))))

        ((variable? expr)
         (emit "movq ~a, %rax" (compile-lookup (variable-var expr) lex si)))

        ;; SET! and top-level DEFINE
        ((or (mutation? expr) (definition? expr))
         ;; Let the value from the expression end up in the val register
         (compile (mutation-expression expr)
                  lex si #f)
         (emit "movq %rax, ~a" (compile-lookup (mutation-variable expr) lex si))
         (emit "movq ~a, %rax" (constant-encode UNSPEC-VALUE)))

        ((eq? (car expr) 'code)
         (let ((badarglist-label (gen-label))
               ;; number of arguments required for procedures with rest arguments:
               (numargs-required (- (length (formals-proper (code-formals expr)))
                                    1)))
           ;; Function prelude
           (emit-push %reg-numargs) ; save number of args passed as local

           ;; Check that the right number of arguments was passed
           (cond ((proper-list? (code-formals expr))
                  (emit "cmp $~a, ~a"
                        (length (code-formals expr))
                        %reg-numargs)
                  (emit "jne ~a" badarglist-label))
                 ((not (zero? numargs-required))
                  ;; a procedure with rest arguments with some
                  ;; non-optional arguments:
                  (emit "cmp $~a, ~a" numargs-required %reg-numargs)
                  (emit "jl ~a" badarglist-label)))

           (unless (proper-list? (code-formals expr))
             ;; Make room for the rest list
             (emit-push (constant-encode EOL-VALUE))
             (set! si (+ si word-size))
             ;; void cons_stack(non_rest, stack)
             (emit-push %reg-stack)
             (emit-push (constant-encode numargs-required))
             (emit "call cons_stack")
             (emit "addq $~a, %rsp" (* word-size 2)))

           ;; Function body

           ;; WARNING: very ugly code!
           ;; Extend the environment with the arguments passed.
           (let lp ((new-env lex)
                    (formals (code-formals expr))
                    (offset (- (* word-size 3))))
             (if (or (symbol? formals) (null? formals))
                 ;; Extend the environment with the closure variables.
                 (let lp ((new-env (if (symbol? formals)
                                       (extend-env* formals 0 new-env)
                                       new-env))
                          (closed (code-closed expr))
                          (offset 1))
                   (if (null? closed)

                       (emit-body (code-body expr) new-env si #t)

                       (lp (extend-env* (car closed) (cons 'closure offset) new-env)
                           (cdr closed)
                           (+ offset 1))))

                 (lp (extend-env* (car formals) offset new-env)
                     (cdr formals)
                     (- offset word-size))))

           ;; Function epilogue

           (emit "movq ~a(%rsp), ~a" si %reg-numargs)
           (emit "addq ~a, %rsp" (constant-encode
                                  (+ si (* 1 word-size))))
           (emit "ret")

           (emit-label badarglist-label)
           (emit-push %reg-closure)
           (emit-push %reg-numargs)
           (emit "call badargs")
           ;; XXX: if this contract violation should be restartable,
           ;; we need to have the label of this code.
           (emit-jump badarglist-label)))

        ((eq? (car expr) 'labels)
         ;; LABELS can only be present at the root of the
         ;; `expressions' variable...
         (let ((skip (gensym "skip_labels")))
           (emit-jump skip)
           (for-each (lambda (x)
                       (let ((label (car x))
                             (code (cadr x)))
                         (emit-label label)
                         (compile code '() 0 #f)))
                     (cadr expr))
           (emit-label skip)
           (compile (caddr expr) lex si tail-pos)))

        ;; Emits an assembler label that is valid inside the body
        ((eq? (car expr) 'label)
         (emit-label (cadr expr))
         (parameterize ((*label-stack-indexes* (cons (cons (cadr expr) si)
                                                     (*label-stack-indexes*))))
           (emit-body (cddr expr) lex si tail-pos)))

        ((eq? (car expr) 'goto)
         (emit "addq ~a, %rsp" (constant-encode (- si
                                                   (cdr (assq (cadr expr)
                                                              (*label-stack-indexes*))))))
         (emit-jump (cadr expr)))

        ((eq? (car expr) 'let)
         (let lp ((bindings (let-bindings expr))
                  (new-env lex)
                  (si si))
           (cond ((null? bindings)
                  (emit-body (let-body expr) new-env si tail-pos)
                  (emit "addq ~a, %rsp" (constant-encode
                                         (* word-size
                                            (length (let-bindings expr))))))
                 (else
                  (compile (cadar bindings) lex si #f)
                  (emit-push %reg-value)

                  (lp (cdr bindings)
                      (extend-env* (caar bindings) si new-env)
                      (+ si word-size))))))

        ((eq? (car expr) 'closure)
         (cond ((= (length expr) 2)
                ;; no closure variables, inline as a constant
                (emit "movq $~a, %rax"
                      (encode (make-primitive #f
                                              (symbol->string
                                               (cadr expr))
                                              0))))
               (else
                (emit-push (constant-encode PROC-TAG))
                (emit-push (constant-encode (align (* word-size
                                                      (+ 1 (length
                                                            (cddr expr))))
                                                   alignment)))
                (emit "call _new")
                (emit "addq ~a, %rsp" (constant-encode (* word-size 2)))

                (emit "movq ~a, ~a(%rax)"
                      (label-encode (cadr expr))
                      (type-ref 1 PROC-TAG))
                (do ((i 2 (+ i 1))
                     (v (cddr expr) (cdr v)))
                    ((null? v))
                  (emit "movq ~a, %rdx"
                        (compile-lookup (car v) lex si))
                  (emit "movq %rdx, ~a(%rax)"
                        (type-ref i PROC-TAG))))))

        ;; IF
        ((conditional? expr)
         (compile (conditional-test expr) lex si #f)
         (let ((fail (gen-label))
               (exit (gen-label)))
           (emit "cmpq ~a, %rax" (constant-encode FALSE-VALUE))
           (emit "je ~a" fail)
           (compile (conditional-consequent expr) lex
                    si tail-pos)
           (emit-jump exit)
           (emit-label fail)
           (compile (conditional-alternative expr) lex
                    si tail-pos)
           (emit-label exit)))

        ;; BEGIN
        ((sequence? expr)
         (emit-body (sequence-expressions expr) lex si tail-pos))

        ;; Primitives
        ((primitive-operation? expr) =>
         (lambda (op)
           (compile-primitive-operation op expr lex si)))

        ((eq? (car expr) 'unsafe-set-car!)
         ;; only used for heap-allocated closure variables
         (compile (caddr expr) lex si #f)
         (emit "pushq %rax")
         (compile (cadr expr) lex (+ si word-size) #f)
         (emit "popq ~a(%rax)" (type-ref 0 PAIR-TAG))
         (emit "movq ~a, %rax" (constant-encode UNSPEC-VALUE)))
        ((eq? (car expr) 'unsafe-car)
         ;; only used for heap-allocated closure variables
         (compile (cadr expr) lex si #f)
         (emit "movq ~a(%rax), %rax" (type-ref 0 PAIR-TAG)))

        ;; Primitive used to construct strings, records, vectors and
        ;; symbols.
        ((eq? (car expr) 'unsafe-make-value)
         ;; (unsafe-make-value words-to-allocate::fixnum type)
         (emit-push (constant-encode
                     (case (caddr expr)
                       ((vector) VECTOR-TAG)
                       ((string) STRING-TAG)
                       ((record) RECORD-TAG)
                       ((symbol) SYMBOL-TAG)
                       (else (error 'compile "Unsupported type for unsafe-make-value"
                                    (caddr expr))))))
         (compile (cadr expr) lex (+ si word-size) #f)
         ;; %reg-value holds (the number of words to allocate << 2).
         ;; Shift to the left to get the number of bytes to allocate,
         ;; then align it.
         (emit "shlq $1, ~a" %reg-value)
         (emit "addq ~a, ~a" (constant-encode (- alignment 1)) %reg-value)
         (emit "andq ~a, ~a" (constant-encode (logxor #xFFFFffffFFFFffff (- alignment 1))) %reg-value)
         (emit-push %reg-value)
         (emit "callq _new")
         (emit "addq ~a, ~a" (constant-encode (* word-size 2)) %reg-stack))

        (else
         ;; Function application
         (let ((badtype-label (gen-label))
               (aftercall-label (gen-label)))
           (if (memq (car expr) '(+ - * / logand logxor logior > >= < <= number?
                                    bit-field car cdr
                                    value-ref value-set!
                                    bytevector-u8-ref
                                    bytevector-u8-set!
                                    bytevector-u16-native-ref
                                    bytevector-u16-native-set!
                                    bytevector-u32-native-ref
                                    bytevector-u32-native-set!))
               (set! tail-pos #f))

           (unless tail-pos
             (set! si (+ si word-size))
             (emit-push %reg-closure))

           (for-each (lambda (operand)
                       (compile operand lex si #f)
                       (emit-push %reg-value)
                       (set! si (+ si word-size)))
                     ;; reversed order for the SYSV calling convention
                     (reverse (application-operands expr)))
           ;; Leave the function in %reg-value
           (compile (application-operator expr) lex si #f)

           (emit "movq %rax, %rcx")
           (emit "andq ~a, %rcx" (constant-encode HEAP-MASK))
           (emit "cmpq ~a, %rcx" (constant-encode PROC-TAG))
           (emit "jne ~a" badtype-label)

           (cond (tail-pos
                  ;; N.B.! This is terrible! Tail-calls are
                  ;; implemented by first setting up a regular call
                  ;; and then overwriting the previous call with this
                  ;; new one. It would be better to simply reuse the
                  ;; current frame.

                  ;; On the stack right now: local variables (the
                  ;; saved number of arguments and LETs). Above that
                  ;; is a return address, and still above that are all
                  ;; the arguments that were passed to this function.

                  ;; Save the return address
                  (emit "push ~a(%rsp)" (+ si (* word-size 1)))
                  (set! si (+ si word-size))

                  ;; Make %rcx point past the current stack frame,
                  ;; past the saved frame pointer and return address.
                  ;; Go past the arguments and set up space for the
                  ;; new arguments.
                  (emit "movq ~a(%rsp), %rcx" si)
                  (emit "leaq ~a(%rsp,%rcx,~a), %rcx"
                        (+ si word-size
                           (* word-size
                              (- (length
                                  (application-operands expr)))))
                        word-size)

                  ;; Move the new frame on top of the old one
                  (let ((count (+ 1 (length (application-operands expr)))))
                    (emit "std")
                    (emit "leaq ~a(%rcx), %rdi" (* word-size (- count 1)))
                    (emit "leaq ~a(%rsp), %rsi" (* word-size (- count 1)))
                    (do ((i 0 (+ i 1)))
                        ((= i count))
                      (emit "movsq"))
                    (emit "cld"))

                  (emit "movq %rcx, %rsp") ;use the new stack

                  (emit "movq %rax, ~a" %reg-closure)
                  (emit "movq ~a(~a), %rcx" (type-ref 1 PROC-TAG)
                        %reg-closure)
                  (emit "movq ~a, ~a" (constant-encode
                                       (length (application-operands expr)))
                        %reg-numargs)
                  (emit "jmp *%rcx"))
                 (else                  ; non-tail calls
                  ;; Get the function to apply from the value register,
                  ;; extract the function pointer, pass the number of
                  ;; arguments and call the function.
                  (emit "movq %rax, ~a" %reg-closure)
                  (emit "movq ~a(~a), %rcx" (type-ref 1 PROC-TAG)
                        %reg-closure)
                  (emit "movq ~a, ~a" (constant-encode
                                       (length (application-operands expr)))
                        %reg-numargs)
                  (emit "callq *%rcx")

                  (emit "leaq (%rsp,~a,~a), %rsp" %reg-numargs word-size)
                  (emit "popq ~a" %reg-closure)
                  (emit-jump aftercall-label)))

           ;; Tried to apply a bad type
           (emit-label badtype-label)
           (emit-push %reg-closure)
           (emit-push %reg-value)
           (emit "call badcall")
           ;; XXX: go back to the branch if this is gonna be restartable
           (emit-jump badtype-label)
           (emit-label aftercall-label)))))



;;; Output the generated code and data

(set-current-output-port (open-output-file (cadr (command-line))))

(display "// Automatically generated by compiler.scm
 .globl run_top_level_forms, _symbols, _environment, env_25multiboot2dinfo, startup_thread
 .text
startup_thread:
")

;;; Expand and compile the source code files

(define c-primitives (with-input-from-file ".primitives.scm" read))

(for-each (lambda (prim)
            (let-optional prim (cname sname reqargs restargs arglist help)
              (top-level-define sname (make-primitive sname cname reqargs))))
          c-primitives)

(print-error "Macroexpanding expressions...")

(define expressions
  (append! (macro-expansion-pass
            *primitive-defines*)
;;            (macro-expansion-pass
;;             '((define %vga-text-memory (sys!make-bytevector (* 80 (+ 25 1) 2)
;;                                                             #f
;;                                                             #xb8000))
;;               (bytevector-u32-native-set! %vga-text-memory 0 #x02590458)))

           (macro-expansion-pass
            (append-map file->list
                        '("syntax.scm" "srfi-26.scm" "match.scm"
                          "srfi-1.scm" "r5rs.scm")))
           null-output
           (macro-expansion-pass
            (append-map file->list
                        '("srfi-9.scm"
                          "concurrency.scm"
                          "symbols.scm"
                          "srfi-69.scm"
                          "bytevectors.scm"

                          "port.scm"
                          "io-ports.scm"
                          "fonts.scm"
                          "vga-text.scm"
                          "errors.scm"
                          "keyboard-keymap.scm"
                          "keyboard.scm"

                          "serial.scm"
                          "multiboot.scm"
                          "pci.scm"
                          "usb.scm"
                          "usb-hub-class.scm"
                          "usb-uhci-hub.scm"
                          "usb-uhci.scm"
                          "network-ip6.scm"
                          "network-ip4.scm"
                          "network-udp.scm"
                          "network-tcp.scm"
                          "network.scm"
                          "e100.scm"
                          "rtl8139.scm"
                          "rtc.scm"

                          "alexpander.scm"
                          "compiler-passes.scm"
                          "eval.scm"
                          "acpi.scm"    ;actually needs to be very early, even before vga-text
                          "init.scm")))

           (macro-expansion-pass
            `((define mstore ',mstore)))))

(print-error "Optimizing expressions...")

;; (with-output-to-file "/tmp/dump.unopt.scm"
;;   (lambda ()
;;     (pretty-print expressions)))

(set! expressions
      (compiler-passes expressions))

;; (with-output-to-file "/tmp/dump.scm"
;;   (lambda ()
;;     (pretty-print expressions)))

(for-each (lambda (expr)
            (when (definition? expr)
              (unless (top-level-defined? (mutation-variable expr))
                (top-level-define (mutation-variable expr)))))
          (cdaddr expressions))

(print-error "Generating code...")
(compile expressions '() 0 #f)


(display "
 movq %rax, %rbx
 pushq $exit_msg
/* callq puts
 movq %rbx, (%rsp)
 callq write
 movq $13, (%rsp)
 callq putchar*/
 movq %r15, (%rsp)
 call thread_terminate_ex
 .data
exit_msg:
 .asciz \"Top level return value: \"
")

(set! doing-environment #t)
(let ((env (allocate environment)))
  (print ".text")
  (print "sys__global_environment:")
  (print " movq $" env ", %rax")
  (print " ret"))

(set! doing-environment #f)
(let ((sym (allocate symbols-obarray)))
  (print ".text")
  (print "sys__symbol_table:")
  (print " movq $" sym ", %rax")
  (print " ret"))


(flush-all-ports)
