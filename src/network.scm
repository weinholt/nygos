;; -*- coding: utf-8 -*-
;; The Unbelievable Operating System from Hell
;; Copyright © 2007 Göran Weinholt <goran@weinholt.se>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; This is a little bit inspired by Luke Gorrie's Slitch, a TCP/IP
;; stack in Common Lisp. The notion of consing together parts of the
;; packet as it travels down the protocol stack is elegant enough and
;; might even be efficient with a network card that does scatter
;; gather.

;; Right now the code is a bit ad hoc!

#|

Frames go up, frames go down.

Input

Frames come in from the NIC driver. They are classified by
protocol and the MAC header is stripped. If it's an IPv4 packet,
the packet is processed by the IPv4 code, and so on.

IPv4 and IPv6 packets are further processed, translating the
addresses to inetaddr records. This can be done with a table of
recently used inetaddr records, because they are immutable. The
scope of the address is determined by matching it against some
static rules (FE80::/10 and 169.254/16 are link-local unicast,
for example). This is then matched against addresses on the
interface to determine is the packet is for this machine or not.

At this point it doesn't matter which IP version the packet uses.

If the packet is for us it is further processed, to determine
which protocol it is for. UDP packets have their target port
looked up in some table and if there is someone interested in the
packet, it is simply handed to them and it shows up on some I/O
port somewhere. If there is nothing listening on the port, an
ICMP error message is generated.

TCP packets have their source address, source port, target
address, target port looked up in a table. If a match is found,
the packet is sent to the matching thread. This thread implements
the TCP state machine and sends any data to and from the I/O port
it is connected to.

Output

Outgoing packets come from networked I/O ports or layers below
that. The IP layer must pick an appropriate interface and
destination MAC address to send the frame to. Packets are
constructed by consing together a header and a payload and
sending this to the layer below, together with some information
about source and destination.

|#

(define-record-type inetaddr (really-make-inetaddr bytevector scope) inetaddr?
  ;; If scope is #f, then this address is of global scope. Otherwise
  ;; scope is an interface.
  (bytevector inetaddr-bytevector)
  (scope inetaddr-scope))               ; RFC 4007 

(define-record-printer (inetaddr x port)
  (display "#<inetaddr ")
  (display (inetaddr->string x))
  (display ">"))

(define (inetaddr=? x y)
  (and (equal? (inetaddr-bytevector x)
               (inetaddr-bytevector y))
       (equal? (inetaddr-scope x)
               (inetaddr-scope y))))

(define (inetaddr-ipv4-mapped? inet)
  ;; RFC4291  IPv4-Mapped IPv6 Address (::FFFF:x.y.z.w)
  (let ((i (inetaddr-bytevector inet)))
    ;; Match against ::ffff:0/96
    (and (= #x00000000 (bytevector-u32-ref i 0 (native-endianness)))
         (= #x00000000 (bytevector-u32-ref i 4 (native-endianness)))
         (= #x0000ffff (bytevector-u32-ref i 8 (endianness big))))))

(define (inetaddr->string inet)
  (let* ((i (inetaddr-bytevector inet))
         (s (cond ((inetaddr-ipv4-mapped? inet)
                   (string-append (number->string (bytevector-u8-ref i 12)) "."
                                  (number->string (bytevector-u8-ref i 13)) "."
                                  (number->string (bytevector-u8-ref i 14)) "."
                                  (number->string (bytevector-u8-ref i 15))))
                  (else
                   ;; FIXME: compress the address
                   (string-append
                    (number->string (bytevector-u16-ref i 0 (endianness big)) 16) ":"
                    (number->string (bytevector-u16-ref i 2 (endianness big)) 16) ":"
                    (number->string (bytevector-u16-ref i 4 (endianness big)) 16) ":"
                    (number->string (bytevector-u16-ref i 6 (endianness big)) 16) ":"
                    (number->string (bytevector-u16-ref i 8 (endianness big)) 16) ":"
                    (number->string (bytevector-u16-ref i 10 (endianness big)) 16) ":"
                    (number->string (bytevector-u16-ref i 12 (endianness big)) 16) ":"
                    (number->string (bytevector-u16-ref i 14 (endianness big)) 16))))))
    (cond ((inetaddr-scope inet) =>
           (lambda (iface)
             (string-append s "%" (interface-name iface)))) ; RFC 4007
          (else s))))

(define (ip4-integer->inetaddr i interface)
  ;; RFC4291 IPv4-Mapped IPv6 Address
  (let ((bv (make-bytevector 16))
        (link-local (= #xa9fe0000 (logand i #xffff0000)))) ;169.254/16
    (bytevector-u16-set! bv 10 #xffff (endianness big))
    (bytevector-u32-set! bv 12 i (endianness big))
    (really-make-inetaddr bv (and link-local interface))))

(define (ip6-bytevector->inetaddr a interface)
  (let ((bv (make-bytevector 16))
        (link-local (= #b1111111010000000
                       (logand (bytevector-u16-ref a 0 (endianness big))
                               #b1111111111000000)))) ;FE80::/10
    (bytevector-copy! a 0 bv 0 16)
    (really-make-inetaddr bv (and link-local interface))))

(define (inetaddr->ip4-integer inet)
  (unless (inetaddr-ipv4-mapped? inet)
    (error 'inetaddr->ip4-integer "Expected an IPv4-Mapped IPv6 address" inet))
  (bytevector-u32-ref (inetaddr-bytevector inet)
                      12 (endianness big)))

(define (bytevector->inetaddr bv interface)
  (let ((ret (make-bytevector 16)))
    (case (bytevector-length bv)
      ((4)
       (bytevector-u16-set! ret 10 #xffff (endianness big))
       (bytevector-copy! bv 0 ret 12 4)
       (really-make-inetaddr ret interface))
      ((16)
       (bytevector-copy! bv 0 ret 0 16)
       (really-make-inetaddr ret interface))
      (else
       (error 'bytevector->inetaddr
              "The bytevector must be of length 4 or 16" bv)))))

(define-record-type inetprefix (really-make-inetprefix inetaddr length mask) inetprefix?
  (inetaddr inetprefix-inetaddr)
  (length inetprefix-length)
  (mask inetprefix-mask))

(define (inetprefix->string prefix)
  (cond ((inetaddr-ipv4-mapped? (inetprefix-inetaddr prefix))
         (string-append (inetaddr->string (inetprefix-inetaddr prefix))
                        "/" (number->string (- (inetprefix-length prefix) 96))))
        (else
         (string-append (inetaddr->string (inetprefix-inetaddr prefix))
                        "/" (number->string (inetprefix-length prefix))))))

(define (inetprefix-match prefix match)
  ;; FIXME: bigint!
  (let ((length (inetprefix-length prefix))
        (subnet-prefix (inetaddr-bytevector (inetprefix-inetaddr prefix)))
        (subnet-mask (inetprefix-mask prefix))
        (address (inetaddr-bytevector match)))
    (let-syntax ((match-part
                  (syntax-rules ()
                    ((_ offset)
                     (= (logand (bytevector-u32-native-ref subnet-mask offset)
                                (bytevector-u32-native-ref subnet-prefix offset))
                        (logand (bytevector-u32-native-ref subnet-mask offset)
                                (bytevector-u32-native-ref address offset)))))))
      (and (eq? (inetaddr-scope (inetprefix-inetaddr prefix))
                (inetaddr-scope match))
           (match-part 12)
           (match-part 8)
           (match-part 4)
           (match-part 0)))))

(define (make-inetprefix inetaddr length)
  (define (length->mask length)
    ;; FIXME: bigint!!
    (let ((bv (make-bytevector 16)))
      (bytevector-u32-set! bv 0 (logxor #xffffffff (ash #xffffffff (- 0 length))) (endianness big))
      (when (> length 32)
        (bytevector-u32-set! bv 4 (logxor #xffffffff (ash #xffffffff (- 32 length))) (endianness big)))
      (when (> length 64)
        (bytevector-u32-set! bv 8 (logxor #xffffffff (ash #xffffffff (- 64 length))) (endianness big)))
      (when (> length 96)
        (bytevector-u32-set! bv 12 (logxor #xffffffff (ash #xffffffff (- 96 length))) (endianness big)))
      bv))
  (if (inetaddr-ipv4-mapped? inetaddr)
      (really-make-inetprefix inetaddr (+ 96 length) (length->mask (+ 96 length)))
      (really-make-inetprefix inetaddr length (length->mask length))))

;;; old stuff:

(define (lladdr->string i)              ;TODO:  rename to mac-addr->string or something
  (string-append
   (number->string (bit-field i 40 48) 16) ":"
   (number->string (bit-field i 32 40) 16) ":"
   (number->string (bit-field i 24 32) 16) ":"
   (number->string (bit-field i 16 24) 16) ":"
   (number->string (bit-field i 8 16) 16) ":"
   (number->string (bit-field i 0 8) 16)))

;;; Misc

(define (mac->ifname mac)
  ;; FIXME: use all bits in the mac and handle conflicts
  (list->string
   `(#\e #\t #\h #\-
     ,(integer->char (+ (char->integer #\A)
                        (modulo mac 25))))))

(define (ip-native-checksum-part bv)
  "Implements the checksumming described in RFC 1071, as used by
the IPv4 and IPv6 protocols. Call this function on the parts of
the packet and give the sum to ip-native-checksum-finish. Only
the last bytevector can have an odd number of bytes.

The checksum is calculated using native endianness and must not
be byteshifted when sent over the wire."
  (do ((len (if (odd? (bytevector-length bv))
                (- (bytevector-length bv) 1)
                (bytevector-length bv)))
       (i 0 (+ i 2))
       (sum (if (odd? (bytevector-length bv))
                (bytevector-u8-ref bv (- (bytevector-length bv) 1))
                0)
            (+ sum (bytevector-u16-native-ref bv i))))
      ((>= i len) sum)))

(define (ip-native-checksum-finish sum)
  (logxor #xffff (logand #xffff (+ (ash sum -16) sum))))

;;; Routing

(define-record-type route (make-route prefix via interface source)
  route?
  (prefix route-prefix)
  (via route-router)
  (interface route-interface)
  (source route-source))

(define routing-table
  (spawn
   (lambda ()
     (let ((routes '()))
       (forever
         (match (?)
           ((from tag ('get-route (? inetaddr? target)))
            (! from (list tag (find (lambda (route)
                                      (inetprefix-match (route-prefix route) target))
                                    routes))))
           ;; TODO: sort routes by prefix length or whatever the RFCs say
           ((from tag ('add-route (? route? route)))
            (print "New route " (inetprefix->string (route-prefix route))
                   " router "
                   (if (route-router route)
                       (inetaddr->string (route-router route))
                       "-")
                   " dev " (interface-name (route-interface route))
                   " source " (inetaddr->string (route-source route)))
            (set! routes (cons route routes))
            (! from (list tag #t)))
           (_ #f)))))))

(define (route-lookup-by-target target)
  "Lookup the route to `target' in the routing table. Returns
either #f or a route object."
  (!? routing-table (list 'get-route target)))

(define (route-add route)
  (!? routing-table (list 'add-route route)))

;;; Interfaces

(define-record-type interface (make-interface name thread lladdr mtu up inetprefixes
                                              neighbor-cache)
  interface?
  (name interface-name)
  (thread interface-driver)
  (lladdr interface-lladdr)
  (mtu interface-mtu)
  (up interface-up)
  (inetprefixes interface-inetprefixes interface-inetprefixes-set!)
  (neighbor-cache interface-neighbor-cache)) ;ncache-thread

(define-record-type ncache-entry (make-ncache-entry address lladdr state isrouter)
  ncache-entry?
  (address ncache-entry-address)
  (lladdr ncache-entry-lladdr)
  (state ncache-entry-state)
  (isrouter ncache-entry-isrouter))

(define (make-neighbor-cache-thread)
  ;; Maintains the neighbor cache (and does a very poor job of it too,
  ;; right now).
  (spawn
   (lambda ()
     (let ((entries '()))               ;a simple alist right now
       (forever
         (match (?)
           (('add (? inetaddr? address) lladdr state)
            (unless (assoc address entries inetaddr=?)
              (print "new neighbor " address " = " (lladdr->string lladdr))
              (set! entries (cons (cons address (make-ncache-entry address lladdr state #f))
                                  entries))))
           ((from tag ('lookup (? inetaddr? address)))
            (cond ((assoc address entries inetaddr=?) =>
                   (lambda (x)
                     (! from (list tag (cdr x)))))
                  (else
                   (! from (list tag #f)))))
           (_ #f)))))))

(define (interface-has-ip4? interface ip4)
  (any (lambda (prefix)
         (let ((inet (inetprefix-inetaddr prefix)))
           (and (inetaddr-ipv4-mapped? inet)
                (= ip4 (inetaddr->ip4-integer inet)))))
       (interface-inetprefixes interface)))

(define (interface-has-ip6? interface ip6)
  (any (lambda (prefix)
         (equal? (inetaddr-bytevector
                  (inetprefix-inetaddr prefix))
                 ip6))
       (interface-inetprefixes interface)))

(define (interface-lookup-neighbor interface address)
  (!? (interface-neighbor-cache interface) (list 'lookup address)))

(define (interface-add-neighbor interface address lladdr state)
  (unless (member state '(stale))
    (error 'interface-add-neighbor "state should be one of: stale" state))
  (unless (integer? lladdr)
    (error 'interface-add-neighbor "lladdr should currently be an integer" lladdr))
  (! (interface-neighbor-cache interface) (list 'add address lladdr state)))

(define *interfaces* '())

(define (interface-add-prefix! interface prefix)
  ;; FIXME: clear the prefix of any address bits it might have
  (route-add (make-route prefix #f interface (inetprefix-inetaddr prefix)))
  (interface-inetprefixes-set! interface (cons prefix (interface-inetprefixes interface))))

(let ((lo (make-interface "loopback" #f 0 65535 #t '() (make-neighbor-cache-thread))))
  (interface-add-prefix! lo (make-inetprefix
                             (bytevector->inetaddr
                              (u8-list->bytevector '(127 0 0 1)) lo)
                             8))
  (interface-add-prefix! lo (make-inetprefix
                             (bytevector->inetaddr
                              (u16-list->bytevector '(0 0 0 0 0 0 0 1)
                                                    (endianness big)) lo)
                             128))
  (set! *interfaces* (cons lo *interfaces*)))

(define network-layer
  (spawn
   (lambda ()
     (forever
       (match (?)
         ((from tag ('register-ethernet-interface lladdr))
          (let* ((name (mac->ifname lladdr))
                 (interface (make-interface name from lladdr 1500 #t
                                            '() (make-neighbor-cache-thread))))
            (route-add (make-route (make-inetprefix
                                    (bytevector->inetaddr '#vu8(0 0 0 0) #f)
                                    0)
                                   (bytevector->inetaddr '#vu8(193 11 254 1) #f)
                                   interface
                                   (bytevector->inetaddr '#vu8(193 11 254 199) #f)))
            
            (interface-add-prefix! interface
                                   (make-inetprefix
                                    (bytevector->inetaddr
                                     (u8-list->bytevector '(192 168 0 2))
                                     #f)
                                    16))
            
            (interface-add-prefix! interface
                                   (make-inetprefix
                                    (bytevector->inetaddr
                                     (u8-list->bytevector '(193 11 254 199))
                                     #f)
                                    24))

            ;; The address is from RFC 3513: Internet Protocol Version
            ;; 6 (IPv6) Addressing Architecture. It's a link-local
            ;; address with an IEEE EUI-64 id created from the
            ;; interface hardware address.
            (interface-add-prefix! interface
                                   (make-inetprefix
                                    (bytevector->inetaddr
                                     (u16-list->bytevector
                                      (list #xfe80
                                            0
                                            0
                                            0
                                            (logxor #b1000000000 (bit-field lladdr 32 48))
                                            (logior #xff (ash (bit-field lladdr 24 32) 8))
                                            (logior #xfe00 (bit-field lladdr 16 24)) 
                                            (bit-field lladdr 0 16))
                                      (endianness big))
                                     interface)
                                    64))

            (set! *interfaces* (cons interface *interfaces*))
            (! from (list tag interface)))))))))

(define (register-ethernet-interface lladdr)
  (!? network-layer (list 'register-ethernet-interface lladdr)))

(define (interfaces)
  (for-each (lambda (i)
              (print (interface-name i)
                     (if (interface-up i) ":" ": DOWN")
                     " lladdr " (lladdr->string (interface-lladdr i))
                     " mtu " (interface-mtu i))
              (for-each (lambda (inetprefix)
                          (print " prefix " (inetprefix->string inetprefix)))
                        (interface-inetprefixes i)))
            *interfaces*))

(define (get-interface name)
  (cond ((interface? name)
         name)
        (else
         (find (lambda (iface)
                 (string-ci=? (interface-name iface) name))
               *interfaces*))))

;;; Ethernet

(define-bytevector-struct ethernet-header (endianness big)
  (u48 ethernet-target-ref ethernet-target-set!)
  (u48 ethernet-source-ref ethernet-source-set!)
  (u16 ethernet-protocol-ref ethernet-protocol-set!))

(define ethernet-layer
  (spawn-supervised
   (lambda ()
     (forever
       (match (?)
         ((from interface (? bytevector? frame))
          (process-packet interface (ethernet-protocol-ref frame)
                          (bytevector-section/shared
                           frame 14 (bytevector-length frame))))
         ((from tag 'ping)
          (! from (list tag 'pong)))
         (_ #f))))))

(define (process-packet interface protocol packet)
  (case protocol
    ((#x0806) (process-arp-packet interface packet))
    ((#x0800) (process-ip4-packet interface packet))
    ((#x86dd) (process-ip6-packet interface packet))
    ((#x8100) (process-vlan-packet interface packet))))

(define (process-ethernet-frame interface frame)
  (! ethernet-layer (list (self) interface frame)))

(define (ethernet-send interface target protocol packet)
  (let ((hdr (make-bytevector 14)))
    (ethernet-target-set! hdr target)
    (ethernet-source-set! hdr (interface-lladdr interface))
    (ethernet-protocol-set! hdr protocol)
    (! (interface-driver interface) (list 'send (cons hdr packet)))))

;;; IEEE Std. 802.1Q-2003, Virtual Bridged Local Area Networks

(define (vlan-vid-ref packet)
  (logand #xfff (bytevector-u16-ref packet 0 (endianness big))))
(define (vlan-original-protocol-ref packet)
  (bytevector-u16-ref packet 2 (endianness big)))

(define (process-vlan-packet interface packet)
  ;; TODO: lookup VLAN interfaces
  (cond ((zero? (vlan-vid-ref packet))
         (process-packet interface (vlan-original-protocol-ref packet) 
                         (bytevector-section/shared packet 4 (bytevector-length packet))))))

;;; Routing

;; (define-record-type ip4route (make-ip4route destination gateway device source)
;;   )


;;; Networked I/O ports

;; (define-record-type internet-address (make-internet-address network-protocol address interface)
;;   internet-address?
;;   (protocol internet-address-protocol)
;;   (address internet-address-address)
;;   (interface internet-address-interface))



;; (define (make-network-port network transport)
;;   (case transport
;;     ((udp)
;;      (make-thread-port
;;       (really-make-socket (make-udp-port-thread network)
;;                           'udp)))
;;     (else
;;      (error 'make-socket "transport protocol not implemented" transport))))

(define (make-internet-port transport . rest)
  "Create an Internet I/O port. The argument `transport'
determines the transport protocol, which currently means UDP or
TCP. A specific Internet Protocol version can optionally be
specified.

The port needs to be connected to a remote address with
connect-internet-port and optionally bound to a local address
with bind-internet-port. "
  (let ((internet-version (:optional rest #f)))
    (case transport
      ((udp)
       (make-port (spawn (make-udp-port-thread internet-version))
                  'network (binary-transcoder) (buffer-mode none) #t #t
                  #t #f))
      ((tcp)
       (make-port (spawn (make-tcp-port-thread internet-version))
                  'network (binary-transcoder) (buffer-mode block) #t #t
                  #t #f))
      (else
       (error 'make-network-port
              "This transport protocol is not implemented" transport)))))



;; (define (make-ipv4-address port address)
;;   (unless (network-port? port)
;;     #f)
;;   (make-internet-address ))

;; (define (make-link-wildcard-address port iface) #f)

;; (define (make-link-broadcast-address port iface) #f)
