/* -*- coding: utf-8 -*-
 * The Unbelievable Operating System from Hell
 * Copyright © 2005-2007 Göran Weinholt <goran@weinholt.se>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/* Code for i8253/i8254 PIT. */
#include <nygos.h>
#include <timer.h>
#include <interrupt.h>

volatile uint64 jiffies = 0;

/* Our interrupt handler wrapper for IRQ0 */
void wrap_int_irq0(void);

#define PIT_FREQ 1193182		/* 1193181.666... Hz rounded up */
#define PIT_COUNT (PIT_FREQ / HZ)

/* I/O ports */
#define COUNTER_0 0x40			/* IRQ0 */
#define COUNTER_1 0x41
#define COUNTER_2 0x42			/* Speaker */
#define CONTROL   0x43

/* Control register bit definitions */
#define COUNT_BINARY 0
#define COUNT_BCD    1

#define MODE_INTERRUPT_ON_TERMINAL_COUNT  (0 << 1)
#define MODE_PROGRAMMABLE_ONESHOT         (1 << 1)
#define MODE_RATE_GENERATOR               (2 << 1)
#define MODE_SQUARE_WAVE_GENERATOR        (3 << 1)
#define MODE_SOFTWARE_TRIGGERED_STROBE    (4 << 1)
#define MODE_HARDWARE_TRIGGERED_STROBE    (5 << 1)

#define LATCH_COUNTER       0000
#define COUNTER_LOW         0020
#define COUNTER_HIGH        0040
#define COUNTER_WORD        0060

#define SELECT_0            0000
#define SELECT_1            0100
#define SELECT_2            0200

void timer_init(void)
{
	/* Set the number of interrupts per second... */
	outb(COUNT_BINARY | MODE_RATE_GENERATOR | COUNTER_WORD | SELECT_0, CONTROL);
	outb(PIT_COUNT & 0xFF, COUNTER_0);
	outb(PIT_COUNT >> 8, COUNTER_0);

	set_intvec(IRQ(0), wrap_int_irq0, IST_IRQ);
	irq_enable(0);
}
