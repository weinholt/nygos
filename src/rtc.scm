;; -*- coding: utf-8 -*-
;; The Unbelievable Operating System from Hell
;; Copyright © 2006-2007 Göran Weinholt <goran@weinholt.se>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; Routines for accessing the Real-Time Clock (Motorola MC146818A).

(define rtc (make-bytevector/io 2 #x70))

(define (rtc-cmos-read i)
  (bytevector-u8-set! rtc 0 i)
  (bytevector-u8-ref rtc 1))

(define (rtc-cmos-write! i v)
  (bytevector-u8-set! rtc 0 i)
  (bytevector-u8-set! rtc 1 v))

#;
(do ((i 0 (+ i 1)))
    ((= i 63))
  (display (number->string (rtc-cmos-read i) 16))
  (display #\space))
(newline)

(define (bcd->number n)
  (string->number (number->string n 16)))

;; From scsh's SRFI-19 code:
(define-record-type date
  (make-date nanosecond second minute hour day month year zone-offset)
  date?
  (nanosecond date-nanosecond set-date-nanosecond!)
  (second date-second set-date-second!)
  (minute date-minute set-date-minute!) 
  (hour date-hour set-date-hour!)
  (day date-day set-date-day!)
  (month date-month set-date-month!)
  (year date-year set-date-year!)
  (zone-offset date-zone-offset set-date-zone-offset!))

(define (current-date)
  (make-date
   0
   (bcd->number (rtc-cmos-read 0))
   (bcd->number (rtc-cmos-read 2))
   (bcd->number (rtc-cmos-read 4))
   (bcd->number (rtc-cmos-read 7))
   (bcd->number (rtc-cmos-read 8))
   (+ 2000 (bcd->number (rtc-cmos-read 9)))
   0))

(define (tm:leap-year? year)
  (or (and (zero? (modulo year 4))
           (positive? (modulo year 100)))
      (zero? (modulo year 400))))

(define (leap-year? date)
  (tm:leap-year? (date-year date)))


(define (date->unix-time date)
  (define acc-days-per-month
    (if (leap-year? date)
        '#(0 31 60 91 121 152 182 213 244 274 305 335 366)
        '#(0 31 59 90 120 151 181 212 243 273 304 334 365)))
  (+ (date-second date)
     (- (date-zone-offset date))
     (* 60 (date-minute date))
     (* 3600 (date-hour date))
     (* 86400 (+ (- (date-day date) 1)
                 (vector-ref acc-days-per-month (- (date-month date) 1))
                 (do ((y (date-year date) (- y 1))
                      (days 0 (+ days (if (tm:leap-year? y) 366 365))))
                     ((= y 1970) days))))))

(let ((now (current-date)))
  (print "Time: "
         (date-year now) "-"
         (date-month now) "-"
         (date-day now) " "
         (date-hour now) ":"
         (date-minute now) ":"
         (date-second now) " UTC")

  (print "Unix time: " (date->unix-time now)))
