/* -*- coding: utf-8 -*-
 * The Unbelievable Operating System from Hell
 * Copyright © 2005-2007 Göran Weinholt <goran@weinholt.se>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RUNTIME_H
#define _RUNTIME_H

struct closure {
	SCM name;
	SCM (*code) ();
	unsigned int args;
	SCM *bindings;
};

SCM define_procedure(char *name, int req, int rest,
					 SCM (*function)());

SCM assertion_violation(const char *msg);
SCM signal_error(const char *func_name, const char *error_msg, SCM vals);
#define ERROR(err, vals)  do { return signal_error(__func__, (err), cons((vals), EOL)); } while (0);

#define TYPE_ASSERT(assertion, value, arg) \
   ({ while (UNLIKELY(!(assertion))) { \
         value = signal_error(__FUNCTION__, "Type error: " #assertion, \
                              cons(value, EOL)); \
	  }})

#include <number.h>

/* Defines a function that can only be called from Scheme code. This
 * macro is also parsed by defprimitives.scm, which is not very clever
 * about any changes in whitespace. `arglen' can be used to find out
 * how many arguments a function received. getarg() can be used by
 * functions with rest arguments, if the last argument is named
 * `rest'. */
/* _rbp must be a global variable, or else gcc 4.2 will believe it
 * statically knows the value when it optimizes the code. */
volatile register int64 _rbp asm("rbp");
#define defun(cname, name, numargs, restargs, arglist, hilfe, code...)	\
  SCM prim__ ## cname arglist { \
      int64 arglen = _rbp;								\
	  /* puts(__FUNCTION__); putchar(' '); puti(arglen,10); putchar('\n');*/ \
      if ((restargs == 0 && UNLIKELY(arglen != numargs)) || \
          (restargs != 0 && UNLIKELY(arglen < numargs))) \
          assertion_violation("Wrong number of arguments"); \
      code \
  };

#define getarg(i) ((&rest)[(i)])

#endif
