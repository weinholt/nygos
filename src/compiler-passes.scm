;; -*- coding: utf-8 -*-
;; The Unbelievable Operating System from Hell
;; Copyright © 2007 Göran Weinholt <goran@weinholt.se>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; A couple of compiler passes for compiler.scm.

(define (compiler-passes exprs)
  (closure-analysis-pass
   (free-variable-analysis-pass
    (assignment-analysis-pass
     (loop-analysis-pass
      `(begin ,@exprs))))))

;;; Predicates and accessors for code

(define (literal? expr)
  (or (number? expr) (char? expr) (boolean? expr) (string? expr)
      (eq? expr (unspecified))
      (and (pair? expr)
           (eq? (car expr) 'quote))))

(define (literal-value expr)
  (if (and (pair? expr)
           (eq? (car expr) 'quote))
      (cadr expr)
      expr))

(define (variable? expr)
  (symbol? expr))

(define (variable-var expr)
  expr)

(define (let? expr)
  (and (pair? expr)
       (eq? (car expr) 'let)))

(define (let-bindings expr)
  (cadr expr))

(define (let-body expr)
  (cddr expr))

(define (lambda? expr)
  (and (pair? expr)
       (eq? (car expr) 'lambda)))

(define (lambda-formals expr)
  (cadr expr))

(define (lambda-formals-proper expr)
  (formals-proper (lambda-formals expr)))

(define (formals-proper expr)
  (let lp ((x expr))
    (cond ((null? x)
           '())
          ((symbol? x)
           (list x))
          (else
           (cons (car x)
                 (lp (cdr x)))))))

(define (lambda-body expr)
  (cddr expr))

(define (mutation? expr)
  (and (pair? expr)
       (eq? (car expr) 'set!)))

(define (definition? expr)
  (and (pair? expr)
       (eq? (car expr) 'define)))

(define (mutation-variable expr)
  (cadr expr))

(define (mutation-expression expr)
  (caddr expr))

(define (sequence? expr)
  (and (pair? expr)
       (eq? (car expr) 'begin)))

(define (sequence-expressions expr)
  (cdr expr))

(define (conditional? expr)
  (and (pair? expr)
       (eq? (car expr) 'if)))

(define (conditional-test expr)
  (cadr expr))

(define (conditional-consequent expr)
  (caddr expr))

(define (conditional-alternative expr)
  (if (pair? (cdddr expr))
      (cadddr expr)
      (unspecified)))

(define (application-operator expr)
  (car expr))

(define (application-operands expr)
  (cdr expr))

(define (letrec? expr)
  (and (pair? expr)
       (eq? (car expr) 'letrec)))

(define letrec-bindings let-bindings)
(define letrec-body let-body)


;;; Trivial loop analysis

(define (make-letrec bindings body)
  (let ((variables (map car bindings))
        (values (map cadr bindings))
        (fresh (map (lambda (x) (gensym "letrec")) bindings)))
    `((lambda ,variables
        ((lambda ,fresh
           ,@(map (lambda (fresh variable)
                    `(set! ,variable ,fresh))
                  fresh variables)
           ,@body)
         ,@values))
      ,@(make-list (length bindings) (unspecified)))))


(define (is-loop-label-misused-in-body? proc inside-closure args tail-pos body)
  (let lp ((exprs body))
    (or (is-loop-label-misused? proc #f args (and tail-pos (null? (cdr exprs))) (car exprs))
        (if (null? (cdr exprs)) #f (lp (cdr exprs))))))

(define (is-loop-label-misused? proc inside-closure args tail-pos expr)
  "Find occurences of `proc' in the expression where `proc' used
in some way other than for iteration. Calling `proc' inside a
lambda that can escape also counts."
  (cond ((eq? proc expr) #t)

        ((literal? expr) #f)
        ((variable? expr) #f)

        ((mutation? expr)
         (or (eq? proc (mutation-variable expr))
             (is-loop-label-misused? proc inside-closure args #f (mutation-expression expr))))

        ((lambda? expr)
         (is-loop-label-misused-in-body? proc #t args #f
                                         (lambda-body expr)))

        ((sequence? expr)
         (is-loop-label-misused-in-body? proc inside-closure args tail-pos
                                         (sequence-expressions expr)))

        ((conditional? expr)
         (or (is-loop-label-misused? proc inside-closure args #f (cadr expr))
             (any (cut is-loop-label-misused? proc inside-closure args tail-pos <>)
                  (cddr expr))))

        ((let? expr)
         (or (any (cut is-loop-label-misused? proc inside-closure args #f <>)
                  (map cadr (let-bindings expr)))
             (is-loop-label-misused-in-body? proc inside-closure args tail-pos
                                             (let-body expr))))

        ((letrec? expr)
         (or (any (cut is-loop-label-misused? proc inside-closure args #f <>)
                  (map cadr (letrec-bindings expr)))
             (is-loop-label-misused-in-body? proc #t args tail-pos
                                             (letrec-body expr))))

        ((and (lambda? (application-operator expr))
              (proper-list? (lambda-formals (application-operator expr)))
              (= (length (application-operands expr))
                 (length (lambda-formals (application-operator expr)))))
         (or (any (cut is-loop-label-misused? proc inside-closure args #f <>)
                  (application-operands expr))
             (is-loop-label-misused-in-body? proc inside-closure args tail-pos
                                             (lambda-body (application-operator expr)))))

        ((and inside-closure (eq? (car expr) proc))
         #t)

        ((and (eq? (car expr) proc)
              (or (not (= (length (cdr expr)) args))
                  (not tail-pos)))
         #t)

        ((and (eq? (car expr) proc)     ;a call to the next iteration of the loop
              (and (= (length (cdr expr)) args)
                   tail-pos))
         ;; Only valid if one of the arguments doesn't use `proc'
         (any (cut is-loop-label-misused? proc inside-closure args #f <>)
              (cdr expr)))

        (else
         (any (cut is-loop-label-misused? proc inside-closure args #f <>)
              expr))))

(cond-expand
 (nygos
  ;; Skip this analysis for now, so that EVAL doesn't need LABEL and GOTO.
  (define (optimizable-loop? expr) #f))
 (else
  (define (optimizable-loop? expr)
    (match expr
      ((('letrec ((label ('lambda (loopvars ___)
                           body ___)))
          label)
        init ___)
       (cond ((or (not (proper-list? loopvars))
                  (not (proper-list? init))
                  (not (= (length loopvars) (length init))))
              ;; (print-error "Warning: loop variables and initial values are wrong, can't optimize this loop:"
              ;;              expr)
              #f)
             ((is-loop-label-misused-in-body? label #f (length loopvars) #t body)
              ;; (print-error "Warning: analysis shows that " label " is misused, so I can't optimize this loop: "
              ;;              expr)
              #f)
             (else
              #t)))
      (_ #f)))))

(define (optimize-loop expr)
  (define (rewrite-expr label asm-label loopvars expr)
    (cond ((literal? expr) expr)
          ((variable? expr) expr)
          ((mutation? expr)
           `(set! ,(mutation-variable expr)
                  ,(rewrite-expr label asm-label loopvars (mutation-expression expr))))

          ((lambda? expr)
           `(lambda ,(lambda-formals expr)
              ,@(map (cut rewrite-expr label asm-label loopvars <>)
                     (lambda-body expr))))

          ((sequence? expr)
           `(begin ,@(map (cut rewrite-expr label asm-label loopvars <>)
                          (sequence-expressions expr))))

          ((conditional? expr)
           `(if ,@(map (cut rewrite-expr label asm-label loopvars <>)
                       (cdr expr))))

          ((letrec? expr)
           `(letrec ,(zip (map car (let-bindings expr))
                          (map (cut rewrite-expr label asm-label loopvars <>)
                               (map cadr (let-bindings expr))))
              ,@(map (cut rewrite-expr label asm-label loopvars <>)
                     (let-body expr))))

          ((eq? (car expr) label)
           (cond ((null? loopvars)
                  `(goto ,asm-label))
                 (else
                  (let* ((tempvars (map (lambda (x) (gensym "tmpvar")) loopvars)))
                    `(begin ((lambda ,tempvars
                               ,@(map (lambda (var tmp)
                                        `(set! ,var ,tmp))
                                      loopvars
                                      tempvars)
                               (goto ,asm-label))
                             ,@(application-operands expr)))))))

          (else
           (map (cut rewrite-expr label asm-label loopvars <>) expr))))
  (match expr
    ((('letrec ((label ('lambda (loopvars ___)
                         body ___)))
        label)
      init ___)
     (let ((asm-label (gensym ".lp")))
       `((lambda ,loopvars
           (label ,asm-label
                  ,@(map (lambda (x)
                           (loop-analysis-pass*
                            (rewrite-expr label asm-label loopvars x))) body)))
         ,@init)))))

(define (loop-analysis-pass* expr)
  "This phase takes code with the forms SET!, BEGIN, IF, LETREC,
LET, application, literals and variables. It tries to find the
loops resulting from expansion of named LET and DO."
  (cond ((literal? expr) expr)
        ((variable? expr) expr)
        ((mutation? expr)
         `(set! ,(mutation-variable expr)
                ,(loop-analysis-pass* (mutation-expression expr))))

        ((lambda? expr)
         `(lambda ,(lambda-formals expr)
            ,@(map (lambda (e)
                     (loop-analysis-pass* e))
                   (lambda-body expr))))

        ((sequence? expr)
         `(begin ,@(map (lambda (x)
                          (loop-analysis-pass* x))
                        (sequence-expressions expr))))

        ((conditional? expr)
         ;; Optimize (if #f ...) while we're at it
         (if (not (conditional-test expr))
             (loop-analysis-pass* (conditional-alternative expr))
             `(if ,@(map (lambda (x)
                           (loop-analysis-pass* x))
                         (cdr expr)))))

        ((optimizable-loop? expr)
         (optimize-loop expr))

        ((letrec? expr)
         (make-letrec (zip (map car (letrec-bindings expr))
                           (map (lambda (e)
                                  (loop-analysis-pass* e))
                                (map cadr (letrec-bindings expr))))
                      (map (lambda (e)
                             (loop-analysis-pass* e))
                           (letrec-body expr))))

        ;; Some other misguided but trivial optimizations: poor man's
        ;; partial evaluation.
        ((and (eq? (car expr) '+) (every number? (cdr expr)))
         (apply + (cdr expr)))
        ((and (eq? (car expr) '-) (every number? (cdr expr)))
         (apply - (cdr expr)))
        ((and (eq? (car expr) '*) (every number? (cdr expr)))
         (apply * (cdr expr)))
        ((and (eq? (car expr) 'char->integer)
              (char? (cadr expr))
              (null? (cddr expr)))
         (char->integer (cadr expr)))

        (else
         (map loop-analysis-pass* expr))))

(define (loop-analysis-pass expr)
  (loop-analysis-pass* expr))

;;; Assignment analysis

(define (find-mutable expr in-closure find)
  "Find any mutations of and references to the variable names in
`find' where the mutation/reference happens inside a closure."
  (cond ((literal? expr) '())
        ((and in-closure (memq expr find))
         (list expr))
        ((variable? expr) '())
        ((mutation? expr)
         (if (and in-closure (memq (mutation-variable expr) find))
             (cons (mutation-variable expr)
                   (find-mutable (mutation-expression expr) in-closure find))
             (find-mutable (mutation-expression expr) in-closure find)))
        ((lambda? expr)
         (append-map (lambda (x)
                       (find-mutable x #t find))
                     (lambda-body expr)))
        ((or (conditional? expr) (sequence? expr))
         (append-map (lambda (x)
                       (find-mutable x in-closure find))
                     (cdr expr)))
        ((let? expr)
         (append-map (lambda (x)
                       (find-mutable x in-closure find))
                     (append (map cadr (let-bindings expr))
                             (let-body expr))))

        ((and (lambda? (application-operator expr)) ; LET
              (proper-list? (lambda-formals (application-operator expr)))
              (= (length (application-operands expr))
                 (length (lambda-formals (application-operator expr)))))
         (append-map (lambda (x)
                       (find-mutable x in-closure find))
                     (append (application-operands expr)
                             (lambda-body (application-operator expr)))))

        (else
         (append-map (lambda (x)
                       (find-mutable x in-closure find))
                     expr))))

(define (assignment-analysis-pass* expr mutables)
  "This phase takes code with the forms SET!, BEGIN, IF, LAMBDA,
LETREC, LABEL, GOTO, application, literals and variables. This
pass transforms mutable variables into heap-allocated objects."
  (cond ((literal? expr) expr)
        ((variable? expr)
         (if (memq expr mutables)
             `(unsafe-car ,expr)
             expr))

        ((mutation? expr)
         (if (memq (mutation-variable expr) mutables)
             `(unsafe-set-car! ,(mutation-variable expr)
                               ,(assignment-analysis-pass* (mutation-expression expr) mutables))
             `(set! ,(mutation-variable expr)
                    ,(assignment-analysis-pass* (mutation-expression expr) mutables))))

        ;; Collect and allocate mutable objects around the body
        ((letrec? expr)
         (let* ((mutable (delete-duplicates
                          (append-map (lambda (x)
                                        (find-mutable x #f (map car (letrec-bindings expr))))
                                      (letrec-body expr))))
                (body (map (lambda (e)
                             (assignment-analysis-pass* e (append mutable mutables)))
                           (letrec-body expr))))
           (if (null? mutable)
               `(letrec ,(letrec-bindings expr)
                  ,@body)
               `(letrec ,(letrec-bindings expr)
                  ((lambda ,mutable
                     ,@body)
                   ,@(map (lambda (x)
                            `(cons ,x 'mutable))
                          mutable))))))

        ;; Collect and allocate mutable objects around the body
        ((lambda? expr)
         (let* ((mutable (delete-duplicates
                          (append-map (lambda (x)
                                        (find-mutable x #f (lambda-formals-proper expr)))
                                      (lambda-body expr))))
                (body (map (lambda (e)
                             (assignment-analysis-pass* e (append mutable mutables)))
                           (lambda-body expr))))

           (if (null? mutable)
               `(lambda ,(lambda-formals expr)
                  ,@body)
               `(lambda ,(lambda-formals expr)
                  ((lambda ,mutable
                     ,@body)
                   ,@(map (lambda (x)
                            `(cons ,x 'mutable))
                          mutable))))))

        ((sequence? expr)
         `(begin ,@(map (lambda (x)
                          (assignment-analysis-pass* x mutables))
                        (sequence-expressions expr))))

        ((conditional? expr)
         `(if ,@(map (lambda (x)
                       (assignment-analysis-pass* x mutables))
                     (cdr expr))))

        (else
         (map (lambda (e)
                (assignment-analysis-pass* e mutables))
              expr))))

(define (assignment-analysis-pass expr)
  (assignment-analysis-pass* expr '()))

; (begin (newline)
;        (pretty-print
;         (assignment-analysis-pass
;          '(let ((f (lambda (c)
;                      (cons (lambda (v) (set! c v))
;                            (lambda () c)))))
;             (let ((p (f 0)))
;               ((car p) 12)
;               ((cdr p)))))))
; =>
; (let ((f (lambda (c) ()
;            (let ((c (mutable c)))
;              (cons (lambda (v) (c) (mutable-set! c v))
;                    (lambda () (c) (mutable-ref c)))))))
;   (let ((p (f 0)))
;     ((car p) 12)
;     ((cdr p))))


;;; Free variable analysis

(define (find-free-variables expr find)
  (cond ((literal? expr)
         '())
        ((variable? expr)
         (if (memq expr find) (list expr) '()))
        ((mutation? expr)
         (if (memq (mutation-variable expr) find)
             (cons (mutation-variable expr)
                   (find-free-variables (mutation-expression expr) find))
             (find-free-variables (mutation-expression expr) find)))
        ((lambda? expr)
         (append-map (lambda (x)
                       (find-free-variables x find))
                     (lambda-body expr)))
        ((or (conditional? expr) (sequence? expr))
         (append-map (lambda (x)
                       (find-free-variables x find))
                     (cdr expr)))
        ((let? expr)
         (append-map (lambda (x)
                       (find-free-variables x find))
                     (append (map cadr (let-bindings expr))
                             (let-body expr))))
        (else
         (append-map (lambda (x)
                       (find-free-variables x find))
                     expr))))

(define (free-variable-analysis-pass* expr lex)
  "This phase takes code with the forms SET!, BEGIN, IF, LAMBDA,
LETREC, LABEL, GOTO, application, literals and variables.
Alpha-conversion must have been done (alexpander does this). It
transforms applications with a LAMBDA in the operator position
into LETs. It annotates LAMBDAs with a list of free variables."
  (cond ((literal? expr) expr)
        ((variable? expr) expr)
        ((mutation? expr)
         `(set! ,(mutation-variable expr)
                ,(free-variable-analysis-pass* (mutation-expression expr) lex)))

        ((letrec? expr)
         (let* ((newlex (append (map car (letrec-bindings expr)) lex))
                (body (map (lambda (e)
                             (free-variable-analysis-pass* e newlex))
                           (letrec-body expr))))
           `(letrec ,(zip (map car (letrec-bindings expr))
                          (map (lambda (e)
                                 (free-variable-analysis-pass* e newlex))
                               (map cadr (letrec-bindings expr))))
              ,@body)))

        ;; Annotate lambdas with a list of which of its variables are used
        ;; by nested lambdas.
        ((lambda? expr)
         (let* ((newlex (append (lambda-formals-proper expr) lex))
                (body (map (lambda (e)
                             (free-variable-analysis-pass* e newlex))
                           (lambda-body expr)))
                (free-vars (delete-duplicates
                            (append-map (lambda (x)
                                          (find-free-variables x lex))
                                        (lambda-body expr)))))
           `(lambda ,(lambda-formals expr) ,free-vars
              ,@body)))

        ((sequence? expr)
         `(begin ,@(map (lambda (x)
                          (free-variable-analysis-pass* x lex))
                        (sequence-expressions expr))))

        ((conditional? expr)
         `(if ,@(map (lambda (x)
                       (free-variable-analysis-pass* x lex))
                     (cdr expr))))

        ((and (lambda? (application-operator expr))
              (proper-list? (lambda-formals (application-operator expr)))
              (= (length (application-operands expr))
                 (length (lambda-formals (application-operator expr)))))
         ;; Turns ((lambda (...) ...) ...) back to a LET
         (let* ((op (application-operator expr))
                (body (map (lambda (e)
                             (free-variable-analysis-pass* e (append (lambda-formals op) lex)))
                           (lambda-body op))))
           `(let ,(zip (lambda-formals (application-operator expr))
                       (map (lambda (e)
                              (free-variable-analysis-pass* e lex))
                            (application-operands expr)))
              ,@body)))

        ;; Anything else is an application:
        (else
         (map (lambda (e)
                (free-variable-analysis-pass* e lex))
              expr))))

(define (free-variable-analysis-pass expr)
  (free-variable-analysis-pass* expr '()))

; (begin (newline)
;        (pretty-print
;         (free-variable-analysis-pass '((lambda (x) (lambda (y)
;                                                       (lambda ()
;                                                         (+ x y))))
;                                        5))))
; =>
; (let ((x 5))
;   (lambda (y) (x)
;           (lambda () (x y) (+ x y))))



;;; Closure analysis
(define (closure-analysis-pass* expr collect-label)
  "This phase takes code with the forms SET!, BEGIN, IF,
annotated LAMBDA, LETREC, LET, LABEL, GOTO, application, literals
and variables. It transforms LAMBDA into CLOSURE and CODE
labels."
  (cond ((literal? expr) expr)
        ((variable? expr) expr)
        ((mutation? expr)
         `(set! ,(mutation-variable expr)
                ,(closure-analysis-pass* (mutation-expression expr) collect-label)))
        ;; Replace lambda with CLOSURE
        ((lambda? expr)
         (let ((label (gensym "procedure")))
           (collect-label `(,label
                            (code ,(lambda-formals expr)
                                  ,(car (lambda-body expr))
                                  ,@(map (lambda (e)
                                           (closure-analysis-pass* e collect-label))
                                         (cdr (lambda-body expr))))))
           `(closure ,label ,@(car (lambda-body expr)))))

        ((sequence? expr)
         `(begin ,@(map (lambda (x)
                          (closure-analysis-pass* x collect-label))
                        (sequence-expressions expr))))

        ((conditional? expr)
         `(if ,@(map (lambda (x)
                       (closure-analysis-pass* x collect-label))
                     (cdr expr))))

        ((let? expr)
         `(let ,(zip (map car (let-bindings expr))
                     (map (lambda (e)
                            (closure-analysis-pass* e collect-label))
                          (map cadr (let-bindings expr))))
            ,@(map (lambda (e)
                     (closure-analysis-pass* e collect-label))
                   (let-body expr))))

        ((letrec? expr)
         `(letrec ,(zip (map car (letrec-bindings expr))
                        (map (lambda (e)
                               (closure-analysis-pass* e collect-label))
                             (map cadr (letrec-bindings expr))))
            ,@(map (lambda (e)
                     (closure-analysis-pass* e collect-label))
                   (letrec-body expr))))

        (else
         (map (lambda (e)
                (closure-analysis-pass* e collect-label))
              expr))))

(define (closure-analysis-pass expr)
  (let* ((labels '())
         (collect-label (lambda (x)
                          (set! labels (cons x labels)))))
    `(labels ,(reverse! labels)
             ,(closure-analysis-pass* expr collect-label))))

; (begin (newline)
;        (pretty-print
;         (closure-analysis-pass '(let ((x 5))
;                                   (lambda (y) (x)
;                                           (lambda () (x y) (+ x y)))))))
; =>
; '(labels ((f0 (code () (x y) (+ x y)))
;           (f1 (code (y) (x) (closure f0 x y))))
;          (let ((x 5)) (closure f1 x)))
