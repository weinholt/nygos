;; -*- coding: utf-8 -*-
;; The Unbelievable Operating System from Hell
;; Copyright © 2005-2007 Göran Weinholt <goran@weinholt.se>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define (error who message . irritants)
  (print "Error signaled by " who ": " message)
  (display "Irritants: ")
  (for-each (lambda (x) (display x) (display " ")) irritants)
  (newline)
  (print "Terminating thread " (self))
  (halt!))

(define (contract-violation who message . irritants)
  (apply error who message irritants))
(define assertion-violation contract-violation)

