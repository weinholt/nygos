;; -*- coding: utf-8 -*-
;; The Unbelievable Operating System from Hell
;; Copyright © 2006-2007 Göran Weinholt <goran@weinholt.se>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; The Universal Serial Bus Driver

;; XXX: these fields are not to be exported.
(define-record-type :usb-device (make-usb-device hcd hub hub-port
                                                 address low-speed
                                                 max-packet0)
  usb-device?
  (thread usb-device-thread usb-device-thread-set!)
  (hcd usb-device-hcd)
  (hub usb-device-hub)
  (hub-port usb-device-hub-port)
  (address usb-device-address usb-device-address-set!)
  (low-speed usb-device-low-speed?)
  (max-packet0 usb-device-max-packet0 usb-device-max-packet0-set!))

(define-bytevector-struct setup-data (endianness little)
  ;; Bit 7: 1=device to host, 0=host to device
  ;; Bit 6-5: 0=standard, 1=class, 2=vendor
  ;; Bit 4-0: 0=device, 1=interface, 2=endpoint, 3=other
  (u8 setup-request-type-ref setup-request-type-set!)
  (u8 setup-request-ref setup-request-set!)
  (u16 setup-value-ref setup-value-set!)
  (u16 setup-index-ref setup-index-set!)
  (u16 setup-length-ref setup-length-set!))

(define GET-STATUS 0)
(define CLEAR-FEATURE 1)
(define SET-FEATURE 3)
(define SET-ADDRESS 5)
(define GET-DESCRIPTOR 6)
(define SET-DESCRIPTOR 7)
(define GET-CONFIGURATION 8)
(define SET-CONFIGURATION 9)
(define GET-INTERFACE 10)
(define SET-INTERFACE 11)
(define SYNCH-FRAME 12)

;;; USB descriptors

(define-bytevector-struct usb-descriptor (endianness little)
  (u8 descriptor-length-ref)
  (u8 descriptor-type-ref))             ; one of the constants below

(define DESCRIPTOR-DEVICE 1)
(define DESCRIPTOR-CONFIGURATION 2)
(define DESCRIPTOR-STRING 3)
(define DESCRIPTOR-INTERFACE 4)
(define DESCRIPTOR-ENDPOINT 5)

(define-bytevector-struct device-descriptor (endianness little)
  (u8 dd-length-ref)
  (u8 dd-descriptor-type-ref)
  (u16 dd-usb-ref)
  (u8 dd-device-class-ref)
  (u8 dd-device-sub-class-ref)
  (u8 dd-device-protocol-ref)
  (u8 dd-max-packet-size0-ref)
  (u16 dd-vendor-ref)
  (u16 dd-product-ref)
  (u16 dd-device-ref)
  (u8 dd-manufacturer-index-ref)
  (u8 dd-product-index-ref)
  (u8 dd-serial-number-index-ref)
  (u8 dd-num-configurations-ref))

(define-bytevector-struct config-descriptor (endianness little)
  (u8 cd-length-ref)
  (u8 cd-descriptor-type-ref)
  (u16 cd-total-length-ref)    ; includes all interfaces and endpoints
  (u8 cd-num-interfaces-ref)
  (u8 cd-configuration-value-ref)
  (u8 cd-configuration-ref)
  (u8 cd-attributes-ref)
  (u8 cd-maxpower-ref))

(define-bytevector-struct interface-descriptor (endianness little)
  (u8 id-length-ref)
  (u8 id-descriptor-type-ref)
  (u8 id-interface-number-ref)
  (u8 id-alternate-setting-ref)
  (u8 id-num-endpoints-ref)
  (u8 id-interface-class-ref)
  (u8 id-interface-sub-class-ref)
  (u8 id-interface-protocol-ref)
  (u8 id-interface-index-ref))

(define-bytevector-struct endpoint-descriptor (endianness little)
  (u8 ed-length-ref)
  (u8 ed-descriptor-type-ref)
  (u8 ed-endpoint-address-ref)
  (u8 ed-attributes-ref)
  (u16 ed-max-packet-size-ref)
  (u8 ed-interval-ref))

(define (utf-16-list->string l)
  "Takes a list of 16-bit integers representing a complete UTF-16
encoded (as in RFC 2781) string and returns the string it represents.
Illegal surrogate pairs and such are replaced with code point #xFFFD."
  (let lp ((ints l)
           (chars '()))
    (cond ((null? ints)
           (list->string (reverse! chars)))
          ((or (<= 0 (car ints) #xD7FF)
               (<= #xE000 (car ints) #xFFFF))
           ;; A character that is legal on its own
           (lp (cdr ints) (cons (integer->char (car ints))
                                chars)))
          ((not (<= #xD800 (car ints) #xDBFF))
           ;; Illegal surrogate pair
           (lp (cdr ints) (cons (integer->char #xFFFD)
                                chars)))
          ((or (null? (cdr ints))
               (not (<= #xDC00 (cadr ints) #xDFFF)))
           ;; Prematurely terminated string or illegal surrogate pair
           (lp (cdr ints) (cons (integer->char #xFFFD)
                                chars)))
          (else
           (let ((U (+ (logior (ash (logand #b1111111111 (car ints)) 10)
                               (logand #b1111111111 (cadr ints)))
                       #x10000)))
             (lp (cddr ints) (cons (integer->char U)
                                   chars)))))))

(define (string-descriptor->string buf)
  "Takes a string descriptor (like the ones sent by USB devices) and
returns the string it represents."
  ;; The first byte of the buffer is the length of the bytevector, and
  ;; the second byte is 3. The rest of the buffer contains data
  ;; encoded with a UTF-16LE codec.
  (if (not (and (bytevector? buf)
                (= (bytevector-u8-ref buf 1) DESCRIPTOR-STRING)))
      (contract-violation 'string-descriptor->string
                          "not a string descriptor" buf))
  (if (> (bytevector-u8-ref buf 0) (bytevector-length buf))
      (contract-violation 'string-descriptor->string
                          "incomplete string descriptor" buf
                          (bytevector-u8-ref buf 0)
                          (bytevector-length buf)))
  ;; FIXME: use utf16->string
  (do ((i 2 (+ i 2))
       (chars '()
              (cons (bytevector-u16-ref buf i (endianness little))
                    chars)))
      ((= i (bytevector-u8-ref buf 0))
       (utf-16-list->string (reverse! chars)))))

;;; End of descriptor code

(define (do-setup-request usbdev endpoint
                          request-type request value index length)
  ;; TODO: hide this inside the USBD
  (let ((req (make-bytevector 8)))
    (setup-request-type-set! req request-type)
    (setup-request-set! req request)
    (setup-value-set! req value)
    (setup-index-set! req index)
    (setup-length-set! req length)
    (!? (usb-device-hcd usbdev)
        (list 'setup-request usbdev endpoint req))))

(define (usb-get-descriptor usbdev endpoint type index language
                            length)
  ;; TODO: hide this inside the USBD
  (define (total-length reply)
    (cond ((= (bytevector-u8-ref reply 1) DESCRIPTOR-CONFIGURATION)
           (cd-total-length-ref reply))
          (else
           (bytevector-u8-ref reply 0))))
  (let retry ((len length)
              (retried #f))
    (let ((reply (do-setup-request usbdev
                                   endpoint
                                   #b10000000
                                   GET-DESCRIPTOR
                                   (logior (ash type 8) index)
                                   language
                                   len)))
      (let ((totlen (total-length reply)))
        (if (< len totlen)
            (if retried
                (error 'usb-get-descriptor "short descriptor returned")
                (retry totlen #t))
            reply)))))

(define USBD
  (spawn
   (lambda ()
     (let ((timeout (make-tag)))
       (forever
         (match (? 1000 timeout)
           ;; These messages are supposed to be what the USB spec
           ;; calls the USBDI.
           ((pid tag ('register-usb hcd root-hub))
            ;; In the future this HCD needs to be supervised
            (! pid (list tag #t))       ; what should be said?
            )

           ((pid tag ('get-descriptor (? usb-device? device) 'configuration))
            #f)

           ((pid tag ('set-configuration (? usb-device? device) value))
            (! pid (list tag (do-setup-request device 0 0 SET-CONFIGURATION value 0 0))))

           (('device-connected hub port)
            (let* ((status (!? (usb-device-thread hub) (list 'get-port-status port)))
                   (low-speed (logtest #b1000000000
                                       (bytevector-u16-ref status 0 (endianness little)))))
              (print "New " (if low-speed "low-speed" "full-speed")
                     " device on port " port ".")

              (!? (usb-device-thread hub) (list 'set-port-feature port 'port-reset))

              ;; Now it's enabled and we can talk with it at address 0
              (let ((usbdev (make-usb-device (usb-device-hcd hub)
                                             hub port
                                             0 ; the default address
                                             low-speed
                                             8)))
                ;; Assign a unique address
                (do-setup-request usbdev 0 0 SET-ADDRESS port 0 0)
                (thread-msleep! 20)
                (usb-device-address-set! usbdev port)

                ;; Fetch the maximum packet size for packets going to
                ;; endpoint zero. It looks tempting to request the
                ;; whole device descriptor here, but don't do that!
                ;; Some devices insist that only 8 bytes be
                ;; transferred at this stage.

                (usb-device-max-packet0-set!
                 usbdev
                 (dd-max-packet-size0-ref
                  (do-setup-request usbdev
                                    0 #b10000000
                                    GET-DESCRIPTOR
                                    (ash DESCRIPTOR-DEVICE 8)
                                    0 8)))

                (usb-device-thread-set! usbdev (spawn (make-usb-dummy-driver usbdev))))))

           (_ #f)))))))


(define (usb-register-hcd hcd root-hub)
  (!? USBD (list 'register-usb hcd root-hub)))

(define (usb-register-hub usbdev)
  (!? USBD (list 'register-hub usbdev)))

(define (usb-device-connect-notify hub port)
  (! USBD (list 'device-connected hub port)))

(define (split-descriptors all)
  (let lp ((ret '())
           (offset 0))
    (cond ((= offset (bytevector-length all))
           (reverse! ret))
          (else
           (let* ((length (bytevector-u8-ref all offset))
                  (desc (make-bytevector length)))
             (bytevector-copy! all offset
                               desc 0
                               length)
             (lp (cons desc ret) (+ offset length)))))))

(define (group-by-type type descs)
  (let lp ((ret '())
           (rest descs))
    (if (null? rest)
        (reverse ret)
        (receive (init tail) (break (lambda (x) (= (descriptor-type-ref x)
                                                   type))
                                    (cdr rest))
          (lp (cons (cons (car rest) init) ret)
              tail)))))

(define (make-usb-dummy-driver usbdev)
  "Creates a thunk that probes the USB device and picks an
appropriate driver."
  (lambda ()
    (let ((devdesc (usb-get-descriptor usbdev 0
                                       DESCRIPTOR-DEVICE
                                       0 0 18)))
      (print "Number of configurations: "
             (dd-num-configurations-ref devdesc))
      (print "Class: " (dd-device-class-ref devdesc)
             " Subclass: " (dd-device-sub-class-ref devdesc)
             " Protocol: " (dd-device-protocol-ref devdesc))

      (print
       (do ((configurations (make-vector (dd-num-configurations-ref devdesc)))
            (conf 0 (+ conf 1)))
           ((= conf (dd-num-configurations-ref devdesc)) configurations)
         (vector-set! configurations conf
                      (split-descriptors
                       (usb-get-descriptor usbdev 0
                                           DESCRIPTOR-CONFIGURATION
                                           conf 0 9)))
         ))

      ;; QEMU's HID mouse returns these descriptors:
      ;;  (#vu8(9 2 34 0 1 1 4 160 50) #vu8(9 4 0 0 1 3 1 2 5)
      ;;   #vu8(9 33 1 0 0 1 34 50 0) #vu8(7 5 129 3 3 0 10))
      ;; This is QEMU's hub:
      ;;  (#vu8(9 2 25 0 1 1 0 192 0) #vu8(9 4 0 0 1 9 0 0 0)
      ;;   #vu8(7 5 129 3 2 0 255))
      ;; This is QEMU's disk:
      ;;  (#vu8(9 2 32 0 1 1 0 192 0) #vu8(9 4 0 0 2 8 6 80 0)
      ;;   #vu8(7 5 129 2 64 0 0) #vu8(7 5 2 2 64 0 0))
      (print "USB dummy driver exits"))))


          ;; (str (cd-configuration-ref confdesc))
          ;;   (unless (zero? str)
          ;;      (let ((string-desc (usb-get-descriptor usbdev 0
          ;;                                             DESCRIPTOR-STRING
          ;;                                             str 0 8)))
          ;;        (print (string-descriptor->string string-desc))))
