;; -*- coding: utf-8 -*-
;; The Unbelievable Operating System from Hell
;; Copyright © 2006-2007 Göran Weinholt <goran@weinholt.se>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; (r6rs bytevector)

;; Primitives:
;; (sys!make-bytevector size i/o? memory-base)

;; TODO: this code assumes a little endian host

(define-syntax endianness
  (syntax-rules (big little)
    ((_ big) 'big)
    ((_ little) 'little)))

(define (native-endianness) 'little)

(define (make-bytevector* who k i/o membase fill) ;not to be exported
  (unless (and (integer? k) (>= k 0))
    (assertion-violation who "k outside of domain" k))
  (unless (or (not membase) (integer? membase))
    (assertion-violation who "membase outside of domain" membase))
;   (unless (and (integer? fill) (<= -128 fill 255))
;     (assertion-violation who "fill outside of domain" fill))
  (let ((v (sys!make-bytevector k i/o membase)))
    (unless (zero? fill)
      (bytevector-fill! v fill))
    v))

(define (make-bytevector k . rest)
  (let ((fill (:optional rest 0)))
    (make-bytevector* 'make-bytevector k #f #f fill)))

(define (make-bytevector/direct k address) ;not part of (r6rs bytevector)
  (make-bytevector* 'make-bytevector/direct k #f address 0))

(define (make-bytevector/io k address)  ;not part of (r6rs bytevector)
  (unless (and (integer? address) (< address #xffff)
               (integer? k) (< (+ address k) #xffff))
    (assertion-violation 'make-bytevector/io "address or size outside of domain" address k))
  (make-bytevector* 'make-bytevector/io k #t address 0))

(define (bytevector-u16-ref bytevector k endianness)
  (case endianness
    ((little) (bytevector-u16-native-ref bytevector k))
    ((big)
     (let ((v (bytevector-u16-native-ref bytevector k)))
       (logior (ash v -8)
               (ash (logand v #xff) 8))))
    (else (error 'bytevector-u16-ref "unknown endianness" endianness))))

(define (bytevector-u16-set! bytevector k n endianness)
  (case endianness
    ((little) (bytevector-u16-native-set! bytevector k n))
    ((big) (bytevector-u16-native-set! bytevector k
                                       (logior (ash n -8)
                                               (ash (logand n #xff) 8))))
    (else (error 'bytevector-u16-set! "unknown endianness" endianness))))

(define (bytevector-u32-ref bytevector k endianness)
  (case endianness
    ((little) (bytevector-u32-native-ref bytevector k))
    ((big)
     (let ((v (bytevector-u32-native-ref bytevector k)))
       (logior (ash (logand v #xff000000) -24)
               (ash (logand v #x00ff0000) -8)
               (ash (logand v #x0000ff00) 8)
               (ash (logand v #x000000ff) 24))))
    (else (error 'bytevector-u32-ref "unknown endianness" endianness))))

(define (bytevector-u32-set! bytevector k n endianness)
  (case endianness
    ((little) (bytevector-u32-native-set! bytevector k n))
    ((big) (bytevector-u32-native-set! bytevector k
                                       (logior (ash (logand n #xff000000) -24)
                                               (ash (logand n #x00ff0000) -8)
                                               (ash (logand n #x0000ff00) 8)
                                               (ash (logand n #x000000ff) 24))))
    (else (error 'bytevector-u32-set! "unknown endianness" endianness))))

(define (bytevector-u64-ref bytevector k endianness)
  (case endianness
    ((little) (bytevector-u64-native-ref bytevector k))
    ((big)
     (let ((v (bytevector-u64-native-ref bytevector k)))
       (logior (logand #x000000ff (ash v -56))
               (logand #x0000ff00 (ash v -40))
               (logand #x00ff0000 (ash v -24))
               (logand #xff000000 (ash v -8))
               (ash (logand v #xff000000) 8)
               (ash (logand v #x00ff0000) 24)
               (ash (logand v #x0000ff00) 40)
               (ash (logand v #x000000ff) 56))))
    (else (error 'bytevector-u64-ref "unknown endianness" endianness))))

(define (bytevector->u8-list bv)
  (do ((i (- (bytevector-length bv) 1) (- i 1))
       (ret '() (cons (bytevector-u8-ref bv i) ret)))
      ((negative? i) ret)))

(define (u8-list->bytevector list)
  (do ((bv (make-bytevector (length list)))
       (i 0 (+ i 1))
       (l list (cdr l)))
      ((null? l) bv)
    (bytevector-u8-set! bv i (car l))))

(define (string->utf8 string)
  "Takes a string and returns a bytevector containing the UTF-8
representation of that string."
  (let* ((len (string-length string))
         (ret (make-bytevector (* 4 len)))) ;maximum possible size
    (let lp ((string-i 0)
             (ret-i 0))
      (if (= string-i len)
          (bytevector-section/shared ret 0 ret-i)
          (let* ((cp (char->integer (string-ref string string-i)))
                 (bytes (cond ((< cp #x80) 1)
                              ((< cp #x800) 2)
                              ((< cp #x10000) 3)
                              ((< cp #x800000) 4)
                              (else
                               (error 'string->utf8
                                      "Invalid character in string"
                                      string string-i cp)))))
            (when (>= bytes 1)
              (bytevector-u8-set! ret ret-i
                                  (logior (ash cp (* (- bytes 1) -6))
                                          (bytevector-u8-ref
                                           '#vu8(#x00 #x00 #xC0 #xE0 #xF0)
                                           bytes)))
              (when (>= bytes 2)
                (bytevector-u8-set! ret (+ ret-i 1)
                                    (logand (logior (ash cp (* (- bytes 2) -6))
                                                    #x80)
                                            #xBF))
                (when (>= bytes 3)
                  (bytevector-u8-set! ret (+ ret-i 2)
                                      (logand (logior (ash cp (* (- bytes 3) -6))
                                                      #x80)
                           #xBF))
                  (when (= bytes 4)
                    (bytevector-u8-set! ret (+ ret-i 3)
                                        (logand (logior cp #x80)
                             #xBF))))))
            (lp (+ string-i 1) (+ ret-i bytes)))))))


;;; non-standard

(define (bytevector-section bytevector start end)
  (let ((new (make-bytevector (- end start))))
    (bytevector-copy! bytevector start
                      new 0
                      (- end start))
    new))

(define (bytevectors-length l)
  (fold (lambda (x len) (+ (bytevector-length x) len))
        0
        l))

(define (bytevector-append . x)
  (let ((length (bytevectors-length x)))
    (do ((bv (make-bytevector length))
         (x x (cdr x))
         (n 0 (+ n (bytevector-length (car x)))))
        ((null? x) bv)
      (bytevector-copy! (car x) 0 bv n (bytevector-length (car x))))))

(define (u16-list->bytevector list end)
  (do ((bv (make-bytevector (* (length list) 2)))
       (i 0 (+ i 2))
       (l list (cdr l)))
      ((null? l) bv)
    (bytevector-u16-set! bv i (car l) end)))

(define-syntax define-bytevector-struct
  (syntax-rules ()
    ((_ name end fields ...)
     (define-bytevector-struct-iter (+) end fields ...))))

(define-syntax define-bytevector-struct-iter
  (syntax-rules ()
    ((_ (offset ...) end (type ref ...))
     (define-bytevector-struct-aux "field" (offset ...) end type ref ...))
    ((_ (offset ...) end (type ref ...) more-fields ...)
     (begin
       (define-bytevector-struct-aux "field" (offset ...) end type ref ...)
       (define-bytevector-struct-iter (offset ...
                                              (define-bytevector-struct-aux
                                                "size" type))
         end more-fields ...)))))

(define-syntax define-bytevector-struct-aux
  (syntax-rules (u8 u16 u32 u48 u64 ascii-string endianness big little)
    ((_ "field" (offset ...) end type)
     (unspecified))
    ((_ "field" (offset ...) end type ref)
     (define (ref reg)
       (define-bytevector-struct-aux "ref" type end reg
         (offset ...))))
    ((_ "field" (offset ...) end type ref set)
     (begin
       (define-bytevector-struct-aux "field" (offset ...) end type ref)
       (define (set reg v)
         (define-bytevector-struct-aux "set" type end reg
           (offset ...) v))))
    
    ((_ "size" u8) 1)
    ((_ "size" u16) 2)
    ((_ "size" u32) 4)
    ((_ "size" u48) 6)
    ((_ "size" u64) 8)
    ((_ "size" (ascii-string length)) length)

    ((_ "ref" u8 end reg off) (bytevector-u8-ref reg off))
    ((_ "ref" u16 end reg off) (bytevector-u16-ref reg off end))
    ((_ "ref" u32 end reg off) (bytevector-u32-ref reg off end))
    ((_ "ref" u48 (endianness big) reg off)
     (logior (ash (bytevector-u32-ref reg off (endianness big)) 16)
             (bytevector-u16-ref reg (+ off 4) (endianness big))))
    ((_ "ref" u48 (endianness little) reg off)
     (logior (ash (bytevector-u16-ref reg off (endianness little)) 32)
             (bytevector-u32-ref reg (+ off 2) (endianness little))))
    ((_ "ref" u64 end reg off) (bytevector-u64-ref reg off end))

    ((_ "ref" (ascii-string length) end reg off)
     (list->string
      (map integer->char
           (bytevector->u8-list
            (bytevector-section reg off (+ off length))))))

    ((_ "set" u8 end reg off v) (bytevector-u8-set! reg off v))
    ((_ "set" u16 end reg off v) (bytevector-u16-set! reg off v end))
    ((_ "set" u32 end reg off v) (bytevector-u32-set! reg off v end))

    ((_ "set" u48 (endianness big) reg off v)
     (begin
       (bytevector-u32-set! reg off (ash v -16) (endianness big))
       (bytevector-u16-set! reg (+ off 4) (logand v #xffff) (endianness big))))
    ((_ "set" u64 end reg off v) (bytevector-u64-set! reg off v end))))
