/* -*- coding: utf-8 -*-
 * The Unbelievable Operating System from Hell
 * Copyright © 2005-2007 Göran Weinholt <goran@weinholt.se>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <nygos.h>
#include <malloc.h>
#include <runtime.h>
#include <string.h>
#include <number.h>

SCM number(int64 i)
{
	if (((i << 2) >> 2) != i) {
		SCM s_num = new(sizeof(struct biggernum), NUMBER);
		struct biggernum *num = get_addrt(s_num, NUMBER);

		num->value = i;

		return s_num;
	}

	return (SCM) i << 2;
}

defun (number_p, "number?", 1, 0, (SCM s_number),
	   "Returns #t if s_number is a number.", {
	return TO_BOOL(IS_INUM(s_number) || IS_NUMBER(s_number));
})

defun (number_eq, "=", 0, 1, (SCM rest),
	   "Compares two numbers, return #t if they are numerically equal.", {
	if (arglen < 2)
		return BOOL_T;

	int64 first = to_int64(getarg(0));
	for (int i = 1; i < arglen; i++)
		if (!(first == to_int64(getarg(i))))
			return BOOL_F;

	return BOOL_T;
})

defun (gt, ">", 0, 1, (SCM rest),
	   "FIXME", {
	if (arglen < 2)
		return BOOL_T;

	int64 prev = to_int64(getarg(0));
	for (int i = 1; i < arglen; i++) {
		int64 num = to_int64(getarg(i));
		if (!(prev > num))
			return BOOL_F;
		prev = num;
	}

	return BOOL_T;
})

defun (ge, ">=", 0, 1, (SCM rest),
	   "FIXME", {
	if (arglen < 2)
		return BOOL_T;

	int64 prev = to_int64(getarg(0));
	for (int i = 1; i < arglen; i++) {
		int64 num = to_int64(getarg(i));
		if (!(prev >= num))
			return BOOL_F;
		prev = num;
	}

	return BOOL_T;
})

defun (lt, "<", 0, 1, (SCM rest),
	   "FIXME", {
	if (arglen < 2)
		return BOOL_T;

	int64 prev = to_int64(getarg(0));
	for (int i = 1; i < arglen; i++) {
		int64 num = to_int64(getarg(i));
		if (!(prev < num))
			return BOOL_F;
		prev = num;
	}

	return BOOL_T;
})

defun (le, "<=", 0, 1, (SCM rest),
	   "FIXME", {
	if (arglen < 2)
		return BOOL_T;

	int64 prev = to_int64(getarg(0));
	for (int i = 1; i < arglen; i++) {
		int64 num = to_int64(getarg(i));
		if (!(prev <= num))
			return BOOL_F;
		prev = num;
	}

	return BOOL_T;
})




defun (plus, "+", 0, 1, (SCM rest),
	   "FIXME", {
	int64 sum = 0;

	for (int i = 0; i < arglen; i++)
		sum += to_int64(getarg(i));

	return number(sum);
})

defun (minus, "-", 1, 1, (SCM init, SCM rest),
	   "FIXME", {
	int64 sum = to_int64(init);

	if (arglen == 1)
		return number(-sum);

	for (int i = 0; i < arglen - 1; i++)
		sum -= to_int64(getarg(i));

	return number(sum);
})

defun (mult, "*", 0, 1, (SCM rest),
	   "FIXME", {
	int64 sum = 1;

	for (int i = 0; i < arglen; i++)
		sum *= to_int64(getarg(i));

	return number(sum);
})

defun (div, "/", 1, 1, (SCM init, SCM rest),
	   "FIXME", {
	int64 sum = to_int64(init);

	for (int i = 0; i < arglen - 1; i++)
		sum /= to_int64(getarg(i));

	return number(sum);
})

defun (modulo, "modulo", 2, 0, (SCM n1, SCM n2),
	   "Returns n1 modulo n2.", {
	return number(to_int64(n1) % to_int64(n2));
})

defun (ash, "ash", 2, 0, (SCM s_number, SCM s_count),
	   "Arithmetically shifts `s_number' left by `s_count' bits,\n\
or to the right if `s_count' is negative.", {
	int64 num;
	int64 count;

	num = to_int64(s_number);
	count = to_int64(s_count);

	if (count < -63)
		num = 0;
	else if (count > 0)
		num <<= count;
	else
		num >>= -count;

	return number(num);
})

defun (bit_field, "bit-field", 3, 0, (SCM s_number, SCM s_start, SCM s_end),
	   "FIXME", {
	int64 num, ret;
	int64 start, end;

	num = to_int64(s_number);
	start = to_int64(s_start);
	end = to_int64(s_end);

	ret = ~(-1 << (end - start)) & (num >> start);

	return number(ret);
})

defun (logand, "logand", 0, 1, (SCM rest),
	   "Returns the bitwise logical AND of all its arguments.", {
	int64 sum = ~0;

	for (int i = 0; i < arglen; i++)
		sum &= to_int64(getarg(i));

	return number(sum);
})

defun (logior, "logior", 0, 1, (SCM rest),
	   "Returns the bitwise logical OR of all its arguments.", {
	int64 sum = 0;

	for (int i = 0; i < arglen; i++)
		sum |= to_int64(getarg(i));

	return number(sum);
})

defun (logxor, "logxor", 0, 1, (SCM rest),
	   "Returns the bitwise logical XOR of all its arguments.", {
	int64 sum = 0;

	for (int i = 0; i < arglen; i++)
		sum ^= to_int64(getarg(i));

	return number(sum);
})
