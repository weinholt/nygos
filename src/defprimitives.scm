;; -*- coding: utf-8 -*-
;; The Unbelievable Operating System from Hell
;; Copyright © 2007 Göran Weinholt <goran@weinholt.se>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; A script that extracts DEFUNs from the C source and generates code
;; that defines these functions in the environment at runtime. This
;; script will be the first against the wall when all the C code has
;; been purged.

(use-modules (srfi srfi-1)
             (srfi srfi-13)
             (srfi srfi-14)
             (ice-9 pretty-print))

(define cstring-lexer #f)
(define lexer #f)

(load "../silex/multilex.scm")
(load "defprimitives-clexer.l.scm")
(load "defprimitives-cstrlexer.l.scm")

(define (print . x) (for-each display x) (newline))

(define (conditional-include file pred?)
    (with-input-from-file file
      (lambda ()
        (do ((expr (read) (read)))
            ((eof-object? expr))
          (if (and (pair? expr) (pred? expr))
              (eval expr (interaction-environment)))))))

(conditional-include "compiler.scm"
                     (lambda (expr)
                       (and (eq? (car expr) 'define)
                            (pair? (cadr expr))
                            (eq? (caadr expr)
                                 'environment-pair-name))))

(define (grep-for-defuns file)
  (define (expect token)
    (let ((t (lexer)))
      (cond ((not (equal? t token))
             (with-output-to-port (current-error-port)
               (lambda ()
                 (print "Unexpected token while reading " file ", token was: " t)
                 (print "Expected: " token)
                 (exit 1)))))))
  (define (expect-type type)
    (let ((t (lexer)))
      (cond ((and (pair? t) (eq? (car t) type))
             (cdr t))
            (else
             (with-output-to-port (current-error-port)
               (lambda ()
                 (print "Unexpected token while reading " file ", token was: " t)
                 (print "Expected a token of type: " type)
                 (exit 1)))))))
  (let* ((fport (open-input-file file))
         (input (lexer-make-IS 'port fport)))
    (set! lexer (lexer-make-lexer c-table input))
    (set! cstring-lexer (lexer-make-lexer cstr-table input))
    (let lp ((funcs '()))
      (let ((token (lexer)))
        (cond ((eq? token 'eof)
               funcs)
              ((equal? token '(symbol . defun))
               ;; Can you say imperative?
               (let* ((skip1 (expect 'openp))
                      (cname (expect-type 'symbol))
                      (skip2 (expect 'comma))
                      (sname (expect-type 'string))
                      (skip3 (expect 'comma))
                      (reqargs (expect-type 'number))
                      (skip4 (expect 'comma))
                      (restargs (expect-type 'number))
                      (skip5 (expect 'comma))
                      (skip6 (expect 'openp))
                      (arglist (do ((i 0 (+ i 1))
                                    (args '() (cons (let* ((type (expect '(symbol . SCM)))
                                                           (name (expect-type 'symbol)))
                                                      (if (< (+ i 1) (+ reqargs restargs))
                                                          (expect 'comma))
                                                      name)
                                                    args)))
                                   ((= i (+ reqargs restargs))
                                    (expect 'closep)
                                    args)))
                      (skip7 (expect 'comma))
                      (help (expect-type 'string)))
                 
                 (lp (cons (list (string-append "prim__" (symbol->string cname))
                                 (string->symbol sname)
                                 reqargs restargs arglist help)
                           funcs))))
              (else
               (lp funcs)))))))

(define apply-primitive
  (list "apply" 'apply 2 1 '(proc arg1 args)
        "Call proc with the given arguments appended to args."))
(define global-environment-primitive
  (list "sys__global_environment" 'sys!global-environment 0 0 '()
        "System procedure that returns the global environment."))
(define global-symbol-table-primitive
  (list "sys__symbol_table" 'sys!symbol-table 0 0 '()
        "System procedure that returns the symbol table."))
(define sys!copy-stack-primitive
  (list "sys__copy_stack" 'sys!copy-stack 1 0 '(thread)
        "System procedure that copies the current thread's stack."))
(define sys!restore-stack-primitive
  (list "sys__restore_stack" 'sys!restore-stack 3 0 '(thread stack value)
        "System procedure that replaces the current thread's stack."))

(let ((new-primitives `(,apply-primitive
                        ,global-environment-primitive
                        ,global-symbol-table-primitive
                        ,sys!copy-stack-primitive
                        ,sys!restore-stack-primitive
                        ,@(append-map grep-for-defuns (cdr (command-line))))))
  (if (and (file-exists? ".primitives.scm")
           (equal? new-primitives (with-input-from-file ".primitives.scm" read)))
      (print ".primitives does not need updating.")
      (with-output-to-file ".primitives.scm"
        (lambda ()
          (pretty-print new-primitives)))))
