;; -*- coding: utf-8 -*-
;; The Unbelievable Operating System from Hell
;; Copyright © 2007 Göran Weinholt <goran@weinholt.se>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; RFC 793: Transmission Control Protocol

(define-bytevector-struct tcp-header (endianness big)
  (u16 tcp-source-port-ref tcp-source-port-set!)
  (u16 tcp-target-port-ref tcp-target-port-set!)
  (u32 tcp-sequence-number-ref tcp-sequence-number-set!)
  (u32 tcp-acknowledgement-number-ref tcp-acknowledgement-number-set!)
  (u16 tcp-data-offset/flags-ref tcp-data-offset/flags-set!)
  (u16 tcp-window-ref tcp-window-set!)
  (u16 tcp-checksum-ref tcp-checksum-set!)
  (u16 tcp-urgent-pointer-ref tcp-urgent-pointer-set!))

(define (tcp-data-offset-ref pkt)
  (ash (tcp-data-offset/flags-ref pkt) -12))
(define (tcp-flags-ref pkt)
  (logand (tcp-data-offset/flags-ref pkt) #b11111111))


(define TCP-FLAGS-CWR #b10000000)   ;; Congestion Window Reduced?
(define TCP-FLAGS-ECN #b01000000)   ;; ECN-Echo?
(define TCP-FLAGS-URG #b00100000)   ;; Urgent Pointer field significant
(define TCP-FLAGS-ACK #b00010000)   ;; Acknowledgment field significant
(define TCP-FLAGS-PSH #b00001000)   ;; Push function
(define TCP-FLAGS-RST #b00000100)   ;; Reset the connection
(define TCP-FLAGS-SYN #b00000010)   ;; Synchronize sequence numbers
(define TCP-FLAGS-FIN #b00000001)   ;; No more data from sender

(define (tcp-ack? pkt) (logtest TCP-FLAGS-ACK (tcp-flags-ref pkt)))
(define (tcp-rst? pkt) (logtest TCP-FLAGS-RST (tcp-flags-ref pkt)))
(define (tcp-syn? pkt) (logtest TCP-FLAGS-SYN (tcp-flags-ref pkt)))
(define (tcp-fin? pkt) (logtest TCP-FLAGS-FIN (tcp-flags-ref pkt)))

(define (tcp-data? pkt)
  (not (= (* 4 (tcp-data-offset-ref pkt))
          (bytevector-length pkt))))

(define (tcp-data pkt)
  (bytevector-section/shared pkt (* 4 (tcp-data-offset-ref pkt))
                             (bytevector-length pkt)))

(define-record-type tcp-stream-id (make-tcp-stream-id source sport target tport) tcp-stream-id?
  (source tcp-stream-id-source)
  (sport tcp-stream-id-sport)
  (target tcp-stream-id-target)
  (tport tcp-stream-id-tport))

(define (tcp-stream-id=? x y)
  (and (inetaddr=? (tcp-stream-id-source x)
                   (tcp-stream-id-source y))
       (equal? (tcp-stream-id-sport x)
               (tcp-stream-id-sport y))
       (inetaddr=? (tcp-stream-id-target x)
                   (tcp-stream-id-target y))
       (equal? (tcp-stream-id-tport x)
               (tcp-stream-id-tport y))))

(define (tcp-stream-id-hash x bound)
  ;; FIXME: hash the source and target as well
  (modulo (+ ;; (tcp-stream-id-source x)
             (tcp-stream-id-sport x)
             ;; (tcp-stream-id-target x)
             (tcp-stream-id-tport x))
          bound))

(define *tcp-connections* (make-hash-table tcp-stream-id=? tcp-stream-id-hash)) ;FIXME: protect in a thread
(define *tcp-listen* (make-hash-table)) ;FIXME: same as above

;;     (print "tcp "
;;            (integer->ipv4 source) ":" (tcp-source-port-ref pkt)
;;            " -> "
;;            (integer->ipv4 target) ":" (tcp-target-port-ref pkt)
;;            " seq=" (tcp-sequence-number-ref pkt)
;;            " ack=" (tcp-acknowledgement-number-ref pkt)
;;            " off=" (tcp-data-offset-ref pkt)
;;            " flags=" (tcp-flags-ref pkt)
;;            " win=" (tcp-window-ref pkt)
;;            " checksum=" (tcp-checksum-ref pkt)
;;            " urgent=" (tcp-urgent-pointer-ref pkt))
;;     (print "ip4 tos=" tos " opt=" opt)
;;       (print "options: " options)
;;       (print "payload: " payload)

(define (find-tcb source sport target tport)
  "Find a TCB given the source and target addresses and ports."
  ;; FIXME: don't allocate memory just to look up the TCB...
  (or (hash-table-ref/default *tcp-connections*
                              (make-tcp-stream-id source sport
                                                  target tport)
                              #f)
      (hash-table-ref/default *tcp-listen* tport #f)))

(define (process-tcp-packet pkt source target)
  ;; FIXME: verify checksum, etc. verify that source and target are
  ;; not group addresses.
  (let (;; (options (bytevector-section/shared pkt 20 (* 4 (tcp-data-offset-ref pkt))))
;;         (payload (bytevector-section/shared pkt (* 4 (tcp-data-offset-ref pkt))
;;                                             (bytevector-length pkt)))
        )
    (cond ((find-tcb source (tcp-source-port-ref pkt) target (tcp-target-port-ref pkt)) =>
           (lambda (conn)
             ;; There is an active connection
             (! conn (list source target pkt))))
          (else
           ;; No connection on this port
           (unless (logtest TCP-FLAGS-RST (tcp-flags-ref pkt))
             (tcp-send-reset source target pkt))))))

(define (tcp-listen! port)
  (hash-table-set! *tcp-listen* port
                   (spawn tcp-state-listen)))

;;; Sliding window code

;; This is also inspired by Luke Gorrie's Slitch, but avoids copying
;; big segments.

;; This data structure keeps track of TCP segments.

(define-record-type window (really-make-window max-size start regions)
  window?
  (max-size window-max-size)
  (start window-start window-start-set!)
  (regions window-regions window-regions-set!))

(define (make-window max-size start)
  (really-make-window max-size start '()))

(define (window-insert-segment! window seq-number bv)
  ;; FIXME: don't just gobble up all data...
  (window-start-set! window (+ seq-number (bytevector-length bv))))

(define (window-filled-position window)
  ;; FIXME: when data is added, this will be different...
  (window-start window))

(define (window-end window)
  (+ (window-start window)
     (window-max-size window)))

;;; TCP connections are threads that receive packets as messages:

(define-record-type tcb (make-tcb irs iss snd.nxt id rwin swin)
  tcb?
  (irs tcb-irs)
  (iss tcb-iss)
  (snd.nxt tcb-snd.nxt tcb-snd.nxt-set!)
  (id tcb-id)
  (rwin tcb-rwin)
  (swin tcb-swin))

(define (tcp-state-listen)
  (forever
    (match (?)
      ((source target pkt)
       (cond ((tcp-rst? pkt)
              #f)
             ((and (tcp-syn? pkt)
                   (not (tcp-ack? pkt)))
              (print "Make a new connection: "
                     source ":" (tcp-source-port-ref pkt) " -> "
                     target ":" (tcp-target-port-ref pkt))
              (let* ((iss 42)           ;FIXME: randomize
                     (id (make-tcp-stream-id source (tcp-source-port-ref pkt)
                                             target (tcp-target-port-ref pkt)))
                     (tcb (make-tcb (tcp-sequence-number-ref pkt)
                                    iss
                                    (+ iss 1)
                                    id
                                    (make-window 4096 (+ 1 (tcp-sequence-number-ref pkt)))
                                    (make-window 4096 (+ 1 (tcp-sequence-number-ref pkt))))))
                (hash-table-set! *tcp-connections* id
                                 (spawn tcp-state-syn-received tcb))))
             (else
              (tcp-send-reset source target pkt)))))))

(define (tcp-state-syn-received tcb)
  (print (list 'becomes 'tcp-syn-received-state))
  (tcp-send-synack! tcb)
  (match (?)
    ((source target pkt)
     (print (list 'tcp-syn-received-state pkt))
     (cond ((tcp-rst? pkt)
            (halt!))
           ((tcp-syn? pkt)
            (tcp-send-reset (tcp-stream-id-source (tcb-id tcb))
                            (tcp-stream-id-target (tcb-id tcb))
                            pkt)
            (halt!))
           (else
            (tcp-state-established tcb))))))

(define (tcp-state-established tcb)
  (print (list 'in 'tcp-established-state))
  (match (?)
    ((source target pkt)
     (print (list 'tcp-established-state pkt))
     (cond ((tcp-rst? pkt)
            (halt!))
           ((tcp-syn? pkt)
            (tcp-send-reset (tcp-stream-id-source (tcb-id tcb))
                            (tcp-stream-id-target (tcb-id tcb))
                            pkt)
            (halt!))
           (else
            (when (tcp-data? pkt)
              ;; There is new data in the stream...
              (print "data: " (tcp-data pkt))
              (window-insert-segment! (tcb-rwin tcb)
                                      (tcp-sequence-number-ref pkt)
                                      (tcp-data pkt)))

            (tcp-send-ack! tcb #f)
            (when (tcp-data? pkt)
              (tcp-send-data! tcb '#vu8(72 101 106 13 10)))
            (cond ((tcp-fin? pkt)
                   ;; Our receiving end is closed now
                   ;; TODO: send FIN/ACK, close the local input-port
                   (print "stream closed... FIXME")
                   (halt!))
                  (else
                   (tcp-state-established tcb))))))))

;;; Sending TCP

(define (tcp-send-data! tcb payload)
  ;; FIXME: check the send window
  (print "sending " payload)
  (tcp-send-ack! tcb payload)
  (tcb-snd.nxt-set! tcb (+ (tcb-snd.nxt tcb)
                           (bytevector-length payload))))


(define (tcp-send! hdr payload source target)
  (cond ((inetaddr-ipv4-mapped? target)
         ;; IP version 4
         (let ((phdr (make-bytevector 12))) ;pseudo header
           (bytevector-u32-set! phdr 0 (inetaddr->ip4-integer source)
                                (endianness big))
           (bytevector-u32-set! phdr 4 (inetaddr->ip4-integer target)
                                (endianness big))
           (bytevector-u8-set! phdr 9 PROTOCOL-TCP)
           (bytevector-u16-set! phdr 10
                                (+ (bytevector-length hdr)
                                   (if payload (bytevector-length payload) 0))
                                (endianness big))

           ;; Checksum
           (bytevector-u32-set! hdr 16
                                (ip-native-checksum-finish
                                 (+ (ip-native-checksum-part phdr)
                                    (ip-native-checksum-part hdr)
                                    (if payload (ip-native-checksum-part payload) 0)))
                                (native-endianness))

           (ip4-send (if payload
                         (list hdr payload)
                         (list hdr))
                     source target 0
                     PROTOCOL-TCP DEFAULT-TTL)))
        (else
         ;; IP version 6
         (let ((phdr (make-bytevector 8))) ;part of the pseudo header
           (bytevector-u32-set! phdr 0
                                (+ (bytevector-length hdr)
                                   (if payload (bytevector-length payload) 0))
                                (endianness big))
           (bytevector-u8-set! phdr 7 PROTOCOL-TCP)

           ;; Checksum:
           (bytevector-u32-set! hdr 16
                                (ip-native-checksum-finish
                                 (+
                                  ;; Pseudo header:
                                  (ip-native-checksum-part (inetaddr-bytevector source))
                                  (ip-native-checksum-part (inetaddr-bytevector target))
                                  (ip-native-checksum-part phdr)
                                  ;; Not pseudo header:
                                  (ip-native-checksum-part hdr)
                                  (if payload (ip-native-checksum-part payload) 0)))
                                (native-endianness))

           (ip6-send (if payload
                         (list hdr payload)
                         (list hdr))
                     source target PROTOCOL-TCP)))))



(define (tcp-send-reset source target pkt)
  (let ((hdr (make-bytevector 20)))
    (tcp-source-port-set! hdr (tcp-target-port-ref pkt))
    (tcp-target-port-set! hdr (tcp-source-port-ref pkt))
    (tcp-data-offset/flags-set! hdr (logior (ash 5 12) ;5*4=20 meaning no options
                                            TCP-FLAGS-RST
                                            (if (logtest TCP-FLAGS-ACK
                                                         (tcp-flags-ref pkt))
                                                0
                                                TCP-FLAGS-ACK)))

    (cond ((logtest TCP-FLAGS-ACK (tcp-flags-ref pkt))
           (tcp-sequence-number-set! hdr (tcp-acknowledgement-number-ref pkt)))
          (else
           (tcp-acknowledgement-number-set!
            hdr (+ (tcp-sequence-number-ref pkt)
                   (if (or (tcp-syn? pkt)
                           (tcp-fin? pkt))
                       1 0)
                   (- (bytevector-length pkt)
                      (* 4 (tcp-data-offset-ref pkt)))))))
    (tcp-send! hdr #f target source)))

(define (tcp-send-synack! tcb)
  (let ((hdr (make-bytevector 20))
        (rwin (tcb-rwin tcb)))
    (tcp-source-port-set! hdr (tcp-stream-id-tport (tcb-id tcb)))
    (tcp-target-port-set! hdr (tcp-stream-id-sport (tcb-id tcb)))
    (tcp-data-offset/flags-set! hdr (logior (ash 5 12) ;5*4=20 meaning no options
                                            TCP-FLAGS-SYN
                                            TCP-FLAGS-ACK))

    (tcp-sequence-number-set! hdr (tcb-snd.nxt tcb))
    (tcp-acknowledgement-number-set! hdr (window-filled-position
                                          rwin)
                                     ;; FIXME: FIN
                                     )
    (tcp-window-set! hdr (- (window-end rwin)
                            (window-filled-position rwin)))

    (tcp-send! hdr #f
               (tcp-stream-id-target (tcb-id tcb))
               (tcp-stream-id-source (tcb-id tcb)))
    (tcb-snd.nxt-set! tcb (+ 1 (tcb-snd.nxt tcb)))))

(define (tcp-send-ack! tcb payload)
  (let ((hdr (make-bytevector 20))
        (rwin (tcb-rwin tcb)))
    (tcp-source-port-set! hdr (tcp-stream-id-tport (tcb-id tcb)))
    (tcp-target-port-set! hdr (tcp-stream-id-sport (tcb-id tcb)))
    (tcp-data-offset/flags-set! hdr (logior (ash 5 12) ;5*4=20 meaning no options
                                            ;; TCP-FLAGS-SYN
                                            TCP-FLAGS-ACK))

    (tcp-sequence-number-set! hdr (tcb-snd.nxt tcb))
    (tcp-acknowledgement-number-set! hdr (window-filled-position
                                          rwin)
                                     ;; FIXME: FIN
                                     )
    (tcp-window-set! hdr (- (window-end rwin)
                            (window-filled-position rwin)))

    (tcp-send! hdr payload
               (tcp-stream-id-target (tcb-id tcb))
               (tcp-stream-id-source (tcb-id tcb)))
    ;;  (tcb-snd.nxt-set! tcb (+ 1 (tcb-snd.nxt tcb)))
    ))


;; XXX: for debugging:
(tcp-listen! 80)
