;; -*- coding: utf-8 -*-
;; Modified reference implementation of SRFI 9: Defining Record Types
;; Copyright (C) Richard Kelsey (1999). All Rights Reserved.
;; Copyright © 2006, 2007 Göran Weinholt <goran@weinholt.se>

;; Permission is hereby granted, free of charge, to any person
;; obtaining a copy of this software and associated documentation
;; files (the "Software"), to deal in the Software without
;; restriction, including without limitation the rights to use, copy,
;; modify, merge, publish, distribute, sublicense, and/or sell copies
;; of the Software, and to permit persons to whom the Software is
;; furnished to do so, subject to the following conditions:

;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;; NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
;; BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
;; ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
;; CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
;; SOFTWARE.

;; These modifications have been made:
;; The vector-based implementation of records was replaced with a
;; primitive-based type. Functions for displaying records was added.

;; Replaced RECORD-CONSTRUCTOR with RECORD-CONS, which builds a
;; contructor at compile time. /weinholt 2007-05-24

;;; SRFI 9: Defining Record Types

;; Non-portable low-level code...
(define (make-record size)
  (unless (and (fixnum? (+ size 1)) (>= size 0))
    ;; XXX: should be &implementation-restriction
    (contract-violation 'make-record
                        "size must be a non-negative fixnum" length))
  (unsafe-make-value (+ size 1) record))

(define (record-ref x i)
  (unless (record? x) (error "record-ref takes a record" x))
  (value-ref x (+ i 1)))

(define (record-set! x i v)
  (unless (record? x) (error "record-set! takes a record" x))
  (value-set! x (+ i 1) v))

;;; Syntax definitions

;; Definition of DEFINE-RECORD-TYPE

(define-syntax define-record-type
   (syntax-rules ()
     ((define-record-type type
        (constructor constructor-tag ...)
        predicate
        (field-tag accessor . more) ...)
      (begin
        (define type
          (make-record-type 'type '(field-tag ...)))
        (define constructor
          (record-cons type (field-tag ...) (constructor-tag ...)))
        (define predicate
          (record-predicate type))
        (define-record-field type field-tag accessor . more)
        ...))))

;; An auxilliary macro for define field accessors and modifiers.
;; This is needed only because modifiers are optional.

(define-syntax define-record-field
   (syntax-rules ()
     ((define-record-field type field-tag accessor)
      (define accessor (record-accessor type 'field-tag)))
     ((define-record-field type field-tag accessor modifier)
      (begin
        (define accessor (record-accessor type 'field-tag))
        (define modifier (record-modifier type 'field-tag))))))

;; id-eq?? is from http://okmij.org/ftp/Scheme/macro-symbol-p.txt
;; It was created by Oleg Kiselyov, genius.

(define-syntax id-eq??
  (syntax-rules ()
    ((id-eq?? id b kt kf)
     (let-syntax
         ((id (syntax-rules ()
                ((id) kf)))
          (ok (syntax-rules ()
                ((ok) kt))))
       (let-syntax
           ((test (syntax-rules ()
                    ((_ b) (id)))))
         (test ok))))))

;; These macros build a record constructor for the passed type and
;; fields. The original reference implementation did this at runtime,
;; but id-eq?? makes it possible to do this in a macro.

(define-syntax record-cons
  (syntax-rules ()
    ((_ type fields init)
     (let ((unchanging-type type))
       (lambda init
         (let ((new (make-record (record-cons-aux "length" (1) fields))))
           (record-set! new 0 unchanging-type)
           (record-cons-aux "fill" new fields init)))))))

(define-syntax record-cons-aux
  (syntax-rules ()
    ((_ "length" (sum ...) (x y ...)) (record-cons-aux "length" (1 sum ...) (y ...)))
    ((_ "length" (sum ...) ())        (+ sum ...))

    ((_ "index" (indexes ...) e (x y ...))
     (id-eq?? e x (+ indexes ...)
              (record-cons-aux "index" (1 indexes ...) e (y ...))))
    ((_ "index" (indexes ...) e ())
     (record-cons-aux "syntax-error"
                      "Constructor takes a non-existing field"
                      e))

    ((_ "fill" new fields (x y ...))
     (begin
       (record-set! new (record-cons-aux "index" (1) x fields) x)
       (record-cons-aux "fill" new fields (y ...))))
    ((_ "fill" new fields ())
     new)))

;;; Display functions for records (compatible with chicken)

(define-syntax define-record-printer
  (syntax-rules ()
    ((_ (name record port) body ...)
     (record-type-printer-set!
      name
      (lambda (record port)
        body ...)))
    ((_ name proc)
     (record-type-printer-set! name proc))))

(define (record-printer-default thing port)
  (display "#<")
  (display (record-type-name (record-type thing)))
  (display ">"))

(define (record-type-printer-set! type f)
  (record-set! type 3 f))

(define (record-type-printer type)
  (record-ref type 3))

(define (record-display record)
  ((record-type-printer (record-type record)) record #f))

;;; Record types

;; We define the following procedures:
;; 
;; (make-record-type <type-name <field-names>)    -> <record-type>
;; (record-constructor <record-type<field-names>) -> <constructor>
;; (record-predicate <record-type>)               -> <predicate>
;; (record-accessor <record-type <field-name>)    -> <accessor>
;; (record-modifier <record-type <field-name>)    -> <modifier>
;;   where
;; (<constructor> <initial-value> ...)         -> <record>
;; (<predicate> <value>)                       -> <boolean>
;; (<accessor> <record>)                       -> <value>
;; (<modifier> <record> <value>)         -> <unspecific>

;; Record types are implemented using vector-like records.  The first
;; slot of each record contains the record's type, which is itself a
;; record.

(define (record-type record)
  (record-ref record 0))

;;----------------
;; Record types are themselves records, so we first define the type for
;; them.  Except for problems with circularities, this could be defined as:
;;  (define-record-type :record-type
;;    (make-record-type name field-tags)
;;    record-type?
;;    (name record-type-name)
;;    (field-tags record-type-field-tags))
;; As it is, we need to define everything by hand.

(define :record-type (make-record 4))
(record-set! :record-type 0 :record-type)   ; Its type is itself.
(record-set! :record-type 1 ':record-type)
(record-set! :record-type 2 '(name field-tags))
(record-set! :record-type 3 record-printer-default)

(define-record-printer (:record-type type port)
  (display "#<:record-type ")
  (display (record-ref type 1))
  (display ">"))

;; Now that :record-type exists we can define a procedure for making more
;; record types.

(define (make-record-type name field-tags)
  (let ((new (make-record 4)))
    (record-set! new 0 :record-type)
    (record-set! new 1 name)
    (record-set! new 2 field-tags)
    (record-set! new 3 record-printer-default)
    new))

;; Accessors for record types.

(define (record-type-name record-type)
  (record-ref record-type 1))

(define (record-type-field-tags record-type)
  (record-ref record-type 2))

;;----------------
;; A utility for getting the offset of a field within a record.

(define (field-index type tag)
  (let loop ((i 1) (tags (record-type-field-tags type)))
    (cond ((null? tags)
           (error "record type has no such field" (record-type-name type) tag))
          ((eq? tag (car tags))
           i)
          (else
           (loop (+ i 1) (cdr tags))))))

;;----------------
;; Now we are ready to define RECORD-PREDICATE and the rest of the
;; procedures used by the macro expansion of DEFINE-RECORD-TYPE.

(define (record-predicate type)
  (lambda (thing)
    (and (record? thing)
         (eq? (record-type thing)
              type))))

(define (record-accessor type tag)
  (let ((index (field-index type tag)))
    (lambda (thing)
      (if (and (record? thing)
               (eq? (record-type thing)
                    type))
          (record-ref thing index)
          (error "accessor applied to bad value" (record-type-name type) tag thing)))))

(define (record-modifier type tag)
  (let ((index (field-index type tag)))
    (lambda (thing value)
      (if (and (record? thing)
               (eq? (record-type thing)
                    type))
          (record-set! thing index value)
          (error "modifier applied to bad value" (record-type-name type) tag thing)))))
