/* -*- coding: utf-8 -*-
 * The Unbelievable Operating System from Hell
 * Copyright © 2005-2007 Göran Weinholt <goran@weinholt.se>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <nygos.h>
#include <malloc.h>
#include <runtime.h>
#include <stdio.h>
#include <multitasking.h>

SCM assertion_violation(const char *msg)
{
	/* TODO: find the procedure that signalled the error */
	puts("Assertion violation in primitive procedure: ");
	puts(msg);
	putchar('\n');

	thread_terminate_ex(get_current_thread());
	return UNSPEC;
}

SCM signal_error(const char *func_name, const char *error_msg, SCM vals)
{
	SCM i;

	puts("Error signaled from ");
	puts(func_name);
	puts(": ");
	puts(error_msg);
	putchar('\n');
	for (i = vals; i != EOL; i = CDR(i)){
		write(CAR(i));
		putchar('\n');
	}

	thread_terminate_ex(get_current_thread());
	return UNSPEC;
}

void badargs(uint32 args, SCM closure)
{
	puts("Scheme procedure called with bad arguments.\n");
	thread_terminate_ex(get_current_thread());
}

void badcall(SCM proc, SCM closure)
{
	puts("Wrong type to apply: ");
	write(proc);
	putchar('\n');
	thread_terminate_ex(get_current_thread());
}

void cons_stack(uint64 non_rest, SCM *stack)
{
	SCM ret = EOL, tail = EOL;
	SCM *dst = stack;
	uint64 numargs = dst[1];

	stack += 3;		/* skip past dst, numargs, return address */
	numargs -= non_rest;
	stack += non_rest;

	for (unsigned int i = 0; i < numargs; i++) {
		SCM next = cons(stack[i], EOL);

		if (ret == EOL)
			ret = tail = next;
		else {
			set_cdr_ex(tail, next);
			tail = next;
		}
	}

	*dst = ret;
}

/***
 * Functions callable from Scheme are below.
 **/

/* Non-standard stuff (to be replaced with better primitives): */

defun (value_ref, "value-ref", 2, 0, (SCM s_value, SCM s_index),
	   "Should be removed.", {
	TYPE_ASSERT(IS_STRING(s_value) || IS_RECORD(s_value) ||
				IS_VECTOR(s_value) || IS_SYMBOL(s_value), s_value, 1);
	TYPE_ASSERT(IS_INUM(s_index), s_index, 2);
	int index = INUM_TO_INT(s_index);
	
	SCM *value = get_addr(s_value);

	return value[index];
})

defun (value_set_ex, "value-set!", 3, 0, (SCM s_value, SCM s_index, SCM s_v),
	   "Should be removed.", {
	TYPE_ASSERT(IS_STRING(s_value) || IS_RECORD(s_value) ||
				IS_VECTOR(s_value) || IS_SYMBOL(s_value), s_value, 1);
	TYPE_ASSERT(IS_INUM(s_index), s_index, 2);
	int index = INUM_TO_INT(s_index);
/* 	TYPE_ASSERT(index >= 0 && index < get_size(s_value), s_index, 2); */
	
	SCM *value = get_addr(s_value);

	value[index] = s_v;

	return UNSPEC;
})
