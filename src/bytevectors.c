/* -*- coding: utf-8 -*-
 * The Unbelievable Operating System from Hell
 * Copyright © 2005-2007 Göran Weinholt <goran@weinholt.se>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/* Primitives for (r6rs bytevectors). */
#include <nygos.h>
#include <malloc.h>
#include <string.h>
#include <runtime.h>
#include <number.h>

static const int DEBUG = 0;

struct bytevector {
	void *mem_base;
	unsigned long size:63;
	unsigned iomem:1;			/* is this I/O instead of memory? */
	char extension[];
};

defun (bytevector_length, "bytevector-length", 1, 0, (SCM bytevector),
	   "FIXME", {
	TYPE_ASSERT(IS_BYTEVECTOR(bytevector), bytevector, 1);

	struct bytevector *v = get_addrt(bytevector, BYTEVECTOR);

	return number(v->size);
})

defun (bytevector_u8_ref, "bytevector-u8-ref", 2, 0, (SCM bytevector, SCM k),
	   "FIXME", {
	TYPE_ASSERT(IS_BYTEVECTOR(bytevector), bytevector, 1);
	struct bytevector *v = get_addrt(bytevector, BYTEVECTOR);
 	TYPE_ASSERT(IS_INUM(k) && INUM_TO_INT(k) >= 0 &&
 				INUM_TO_INT(k) < v->size, k, 2);
	uint8 *loc = (uint8 *) v->mem_base + INUM_TO_INT(k);
	if (UNLIKELY(v->iomem)) {
		return fixnum(inb((int64) loc));
	} else {
		return fixnum(*loc);
	}
})

defun (bytevector_u8_set_ex, "bytevector-u8-set!", 3, 0,
	   (SCM bytevector, SCM k, SCM octet),
	   "FIXME", {
	TYPE_ASSERT(IS_BYTEVECTOR(bytevector), bytevector, 1);
	struct bytevector *v = get_addrt(bytevector, BYTEVECTOR);
	TYPE_ASSERT(IS_INUM(k) && INUM_TO_INT(k) >= 0 &&
				INUM_TO_INT(k) < v->size, k, 2);
	TYPE_ASSERT(IS_INUM(octet) &&
				INUM_TO_INT(octet) >= 0 &&
				INUM_TO_INT(octet) <= 0xff, octet, 5);
	uint8 *loc = (uint8 *) v->mem_base + INUM_TO_INT(k);
	uint8 val = INUM_TO_INT(octet);
	if (UNLIKELY(v->iomem)) {
		outb(val, (int64) loc);
	} else {
		*loc = val;
	}
	return UNSPEC;
})

defun (bytevector_u16_native_ref, "bytevector-u16-native-ref", 2, 0,
	   (SCM bytevector, SCM k),
	   "FIXME", {
	TYPE_ASSERT(IS_BYTEVECTOR(bytevector), bytevector, 1);
	struct bytevector *v = get_addrt(bytevector, BYTEVECTOR);
 	TYPE_ASSERT(IS_INUM(k) && INUM_TO_INT(k) >= 0 &&
 				INUM_TO_INT(k) + 1 < v->size
				/* FIXME: These are used to implement the other ref
				 * and set! operations, so don't check for alignment
				 * here, but do implement that later. */
				/* && INUM_TO_INT(k) % 2 == 0*/, k, 2);
	uint16 *loc = (uint16 *) ((uint8 *) v->mem_base + INUM_TO_INT(k));
	if (UNLIKELY(v->iomem)) {
		return fixnum(inw((int64) loc));
	} else {
		return fixnum(*loc);
	}
})

defun (bytevector_u16_native_set_ex, "bytevector-u16-native-set!", 3, 0,
	   (SCM bytevector, SCM k, SCM n),
	   "FIXME", {
	TYPE_ASSERT(IS_BYTEVECTOR(bytevector), bytevector, 1);
	struct bytevector *v = get_addrt(bytevector, BYTEVECTOR);
	TYPE_ASSERT(IS_INUM(k) && INUM_TO_INT(k) >= 0 &&
				INUM_TO_INT(k) + 1 < v->size
				/* && INUM_TO_INT(k) % 2 == 0*/, k, 2);
	TYPE_ASSERT(IS_INUM(n) &&
				INUM_TO_INT(n) >= 0 &&
				INUM_TO_INT(n) <= 0xffff, n, 3);
	uint16 *loc = (uint16 *) ((uint8 *) v->mem_base + INUM_TO_INT(k));
	uint16 val = INUM_TO_INT(n);
	if (UNLIKELY(v->iomem)) {
		outw(val, (int64) loc);
	} else {
		*loc = val;
	}
	return UNSPEC;
})

defun (bytevector_u32_native_ref, "bytevector-u32-native-ref", 2, 0,
	   (SCM bytevector, SCM k),
	   "FIXME", {
	TYPE_ASSERT(IS_BYTEVECTOR(bytevector), bytevector, 1);
	struct bytevector *v = get_addrt(bytevector, BYTEVECTOR);
 	TYPE_ASSERT(IS_INUM(k) && INUM_TO_INT(k) >= 0 &&
 				INUM_TO_INT(k) + 3 < v->size
				/* && INUM_TO_INT(k) % 4 == 0*/, k, 2);
	uint32 *loc = (uint32 *) ((uint8 *) v->mem_base + INUM_TO_INT(k));
	if (UNLIKELY(v->iomem)) {
		return fixnum(inl((int64) loc));
	} else {
		return fixnum(*loc);
	}
})

defun (bytevector_u32_native_set_ex, "bytevector-u32-native-set!", 3, 0,
	   (SCM bytevector, SCM k, SCM n),
	   "FIXME", {
	TYPE_ASSERT(IS_BYTEVECTOR(bytevector), bytevector, 1);
	struct bytevector *v = get_addrt(bytevector, BYTEVECTOR);
	TYPE_ASSERT(IS_INUM(k) && INUM_TO_INT(k) >= 0 &&
				INUM_TO_INT(k) + 3 < v->size
				/* && INUM_TO_INT(k) % 4 == 0*/, k, 2);
	TYPE_ASSERT(IS_INUM(n) &&
				INUM_TO_INT(n) >= 0 &&
				INUM_TO_INT(n) <= 0xffffffff, n, 3);
	uint32 *loc = (uint32 *) ((uint8 *) v->mem_base + INUM_TO_INT(k));
	uint32 val = INUM_TO_INT(n);
	if (UNLIKELY(v->iomem)) {
		outl(val, (int64) loc);
	} else {
		*loc = val;
	}
	return UNSPEC;
})

defun (bytevector_u64_native_ref, "bytevector-u64-native-ref", 2, 0,
	   (SCM bytevector, SCM k),
	   "FIXME", {
	TYPE_ASSERT(IS_BYTEVECTOR(bytevector), bytevector, 1);
	struct bytevector *v = get_addrt(bytevector, BYTEVECTOR);
 	TYPE_ASSERT(IS_INUM(k) && INUM_TO_INT(k) >= 0 &&
 				INUM_TO_INT(k) + 7 < v->size
				/* && INUM_TO_INT(k) % 8 == 0*/, k, 2);
	uint64 *loc = (uint64 *) ((uint8 *) v->mem_base + INUM_TO_INT(k));
	if (UNLIKELY(v->iomem)) {
		int64 l = inl((int64) loc);
		return number(l | ((int64) inl((int64) loc + 4) << 32));
	} else {
		return number(*loc);
	}
})

/* Non-standard */

defun (bytevector_dev_addr, "bytevector-dev-addr", 1, 0, (SCM bytevector),
	   "Description missing (used for DMA)", {
	TYPE_ASSERT(IS_BYTEVECTOR(bytevector), bytevector, 1);

	struct bytevector *v = get_addrt(bytevector, BYTEVECTOR);

	return number((int64) v->mem_base);
})

defun (sys_make_bytevector, "sys!make-bytevector", 3, 0,
	   (SCM s_size, SCM s_io, SCM s_mem_base),
	   "System function used to implement make-bytevector.", {
	/* Unsafe code. */
	SCM s_bytevector = new(sizeof(struct bytevector) +
						   (IS_INUM(s_mem_base) ? 0 : INUM_TO_INT(s_size)),
						   BYTEVECTOR);
	struct bytevector *bytevector = get_addrt(s_bytevector, BYTEVECTOR);

	bytevector->size = INUM_TO_INT(s_size);
	bytevector->iomem = !IS_FALSE(s_io);

	if (IS_INUM(s_mem_base)) {
		bytevector->mem_base = (void *) get_fixnum(s_mem_base);
	} else {
		bytevector->mem_base = bytevector->extension;
	}
	
	return s_bytevector;
})

defun (bytevector_copy_ex, "bytevector-copy!", 5, 0,
	   (SCM s_source, SCM s_source_start,
		SCM s_target, SCM s_target_start,
		SCM s_n),
	   "FIXME", {
	TYPE_ASSERT(IS_BYTEVECTOR(s_source), s_source, 1);
	TYPE_ASSERT(IS_INUM(s_source_start), s_source_start, 2);
	TYPE_ASSERT(IS_BYTEVECTOR(s_target), s_target, 3);
	TYPE_ASSERT(IS_INUM(s_target_start), s_target_start, 4);
	TYPE_ASSERT(IS_INUM(s_n), s_n, 5);

	struct bytevector *source, *target;
	int64 source_start, target_start, n;
	source = get_addrt(s_source, BYTEVECTOR);
	source_start = INUM_TO_INT(s_source_start);
	target = get_addrt(s_target, BYTEVECTOR);
	target_start = INUM_TO_INT(s_target_start);
	n = INUM_TO_INT(s_n);

	/* 0 <= source-start <= source-start + n <= (blob-length source) */
	if (!((0 <= source_start) &&
		  (source_start <= source_start + n) &&
		  (source_start + n <= source->size)))
		ERROR("out of bounds", n);

	/* 0 <= target-start <= target-start + n <= (blob-length target) */
	if (!((0 <= target_start) &&
		  (target_start <= target_start + n) &&
		  (target_start + n <= target->size)))
		ERROR("out of bounds", n);

	TYPE_ASSERT(!target->iomem, s_target, 3);

	if (source->iomem) {
		char *to = target->mem_base + target_start,
			*from = source->mem_base + source_start;
		for (int i = 0; i < n; i++) {
			to[i] = inb((int64) from + i);
		}
	} else {
		memmove((uint8 *) target->mem_base + target_start,
				(uint8 *) source->mem_base + source_start, n);
	}
	
	return UNSPEC;
})

defun (bytevector_section_shared, "bytevector-section/shared", 3, 0,
	   (SCM s_bytevector, SCM s_start, SCM s_end),
	   "", {
	SCM s_newbv;
	int64 start, end;
	struct bytevector *newbv, *oldbv;

	TYPE_ASSERT(IS_BYTEVECTOR(s_bytevector), s_bytevector, 1);
	TYPE_ASSERT(IS_INUM(s_start), s_start, 2);
	TYPE_ASSERT(IS_INUM(s_end), s_end, 3);

	oldbv = get_addrt(s_bytevector, BYTEVECTOR);
	start = INUM_TO_INT(s_start);
	end = INUM_TO_INT(s_end);

	if (start < 0 || start > oldbv->size)
		ERROR("start out of bounds", start);
	if (end < start || end > oldbv->size)
		ERROR("end out of bounds", end);

	s_newbv = new(sizeof(struct bytevector), BYTEVECTOR);
	newbv = get_addrt(s_newbv, BYTEVECTOR);

	newbv->mem_base = oldbv->mem_base + start;
	newbv->size = (end - start);
	newbv->iomem = oldbv->iomem;

	return s_newbv;
})
