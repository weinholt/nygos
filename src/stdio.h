/* -*- coding: utf-8 -*-
 * The Unbelievable Operating System from Hell
 * Copyright © 2005, 2007 Göran Weinholt <goran@weinholt.se>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _STDIO_H
#define _STDIO_H

#include <malloc.h>

void cls(void);
void puts(const char *s);		/* print a string */
void puti(int64 i, int base);	/* print a number */
void putchar(unsigned char c);	/* print a character */
void write(SCM v);				/* print a scheme value */

void getxy(int *x, int *y);
void setxy(int x, int y);

#endif
