/* -*- coding: utf-8 -*-
 * The Unbelievable Operating System from Hell
 * Copyright © 2005-2007 Göran Weinholt <goran@weinholt.se>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __MULTITASKING_H
#define __MULTITASKING_H

#include <malloc.h>

volatile register SCM current_thread asm ("r15");

static inline SCM get_current_thread(void)
{
	return current_thread;
}

void multitasking_send(SCM s_thread, SCM message);
void multitasking_forever(void) __attribute__ ((noreturn));
SCM multitasking_new(void *rip, uint64 rbx, SCM name);

SCM get_current_thread(void);
SCM thread_name(SCM s_thread);
SCM thread_terminate_ex(SCM s_thread);

#endif
