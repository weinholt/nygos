;; -*- coding: utf-8 -*-
;; The Unbelievable Operating System from Hell
;; Copyright © 2007 Göran Weinholt <goran@weinholt.se>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; RFC 768: User Datagram Protocol

(define PROTOCOL-UDP 17)

(define-bytevector-struct udp-header (endianness big)
  (u16 udp-source-port-ref udp-source-port-set!)
  (u16 udp-target-port-ref udp-target-port-set!)
  (u16 udp-length-ref udp-length-set!)
  (u16 udp-checksum-ref udp-checksum-set!))

(define (process-udp-packet pkt source target)
  (print "udp " source ":"
         (udp-source-port-ref pkt)
         " -> "
         target ":"
         (udp-target-port-ref pkt) " (" (udp-length-ref pkt) ")"))

;; FIXME: implement path mtu discovery and hold on to outgoing packets
;; for a little bit, in case they need to be fragmented and
;; retransmitted.

(define (send-udp-packet payload target target-port tos opt)
  (let ((hdr (make-bytevector 8)))
    (udp-source-port-set! hdr 1)
    (udp-target-port-set! hdr target-port)
    (udp-length-set! hdr (+ 8 (bytevector-length payload)))
    ;; FIXME: checksum
    (ip4-send (list hdr payload) #f target tos opt
              PROTOCOL-UDP DEFAULT-TTL)))
