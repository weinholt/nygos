;; -*- coding: utf-8 -*-
;; The Unbelievable Operating System from Hell
;; Copyright © 2005-2007 Göran Weinholt <goran@weinholt.se>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(newline)

(! vga-text-process '(attr . 4))
(display "\xb4\xb5 ")
(! vga-text-process '(attr . 7))

(print "The Unbelievable Operating System from Hell (unreleased)")

(! vga-text-process '(attr . 4))
(display "\xb6\xb7 ")
(! vga-text-process '(attr . 7))

(print "Copyright \xA9 2005-2007 G\xF6ran Weinholt <goran@weinholt.se>\n\
NYGOS comes with ABSOLUTELY NO WARRANTY; for details type C-h C-w.\n\
This is free software, and you are welcome to redistribute it\n\
under certain conditions; type C-h C-c for details.\n")

(display "> ")

(define timer0 (make-bytevector/io 4 #x40))
(define timer1 (make-bytevector/io 4 #x50))


(define (pcspkr-set! hz)
  (cond ((and (number? hz) (> hz 20) (< hz 32767))
         (bytevector-u8-set! keyboard 1
                             (logior 3 (bytevector-u8-ref keyboard 1)))
         (bytevector-u8-set! timer0 3 #xb6)
         (let ((count (/ #x1234DC hz)))
           (bytevector-u8-set! timer0 2 (logand #xff count))
           (bytevector-u8-set! timer0 2 (logand #xff (ash count -8)))))
        (else
         (bytevector-u8-set! keyboard 1 (logand #xfc (bytevector-u8-ref keyboard 1))))))

#;
(do ((i 1000 (+ i 300)))
    ((>= i 5000))
  (pcspkr-set! i)
  (thread-sleep! 1))
(pcspkr-set! #f)

;;; Silly makeshift input-port support

(define read-input-port
  (spawn-supervised
   (lambda ()
     (let ((buf (make-queue))
           (waiting-for-read '())
           (waiting-for-peek '()))
       (forever
         (match (?)
           ((from tag 'ping)
            (! from (list tag 'pong)))
           ((from tag 'read-char)
            (if (queue-empty? buf)
                (set! waiting-for-read (cons (cons from tag)
                                             waiting-for-read))
                (! from (list tag (dequeue! buf)))))
           ((from tag 'peek-char)
            (if (queue-empty? buf)
                (set! waiting-for-peek (cons (cons from tag)
                                             waiting-for-peek))
                (! from (list tag (caar buf)))))
           (('write-char (? char? x))
            (cond ((pair? waiting-for-read)
                   (! (caar waiting-for-read)
                      (list
                       (cdar waiting-for-read)
                       x))
                   (set! waiting-for-read (cdr waiting-for-read)))
                  ((pair? waiting-for-peek)
                   (! (caar waiting-for-peek)
                      (list
                       (cdar waiting-for-peek)
                       x))
                   (set! waiting-for-peek (cdr waiting-for-peek)))
                  (else
                   (enqueue! buf x))))
           ('flush-input-port
            (set! buf (make-queue)))
           (_ #f)))))))

(define (read-char)
  (!? read-input-port 'read-char))
(define (peek-char)
  (!? read-input-port 'peek-char))
(define (flush-input-port)
  (! read-input-port 'flush-input-port))

(define gensym
  (let ((c 0))
    (lambda (key)
      (set! c (+ c 1))
      (string->symbol (string-append key (number->string c))))))

(define *evaluating* #f)

(define evaluator
  (spawn-supervised
   (lambda ()
     (flush-input-port)
     (forever
       (set! *evaluating* #f)
       (match (?)
         ((from tag 'ping)
          (! from (list tag 'pong)))
         ('eval
          (set! *evaluating* #t)
          (let ((expr (read)))
            (unless (eof-object? expr)
              (let ((ret (eval expr)))
                (unless (eq? ret (unspecified))
                  (write ret)
                  (newline)))))
          (display "> ")))))))

(define (eval-last-line)
  (unless (queue-empty? kbd-buf)
    (newline)
    (while (not (queue-empty? kbd-buf))
      (! read-input-port (list 'write-char (dequeue! kbd-buf))))
    (! read-input-port (list 'write-char #\newline))
    (unless *evaluating*
      (! evaluator 'eval))))

(define-key global-map '#(#\return) 'eval-last-line)
(define-key global-map '#(#\newline) 'eval-last-line)

(define-syntax benchmark
  (syntax-rules ()
    ((_ times
        body ...)
     (let ((x (rdtsc)))
       (do ((i times (- i 1)))
           ((zero? i)
            (let ((y (rdtsc)))
              (print "Timed " '(body ...))
              (print "Average for " times " iterations: " (/ (- y x)
                                                             times))))
         body ...)))))

(define (print-simple-timing)
  (let* ((x (rdtsc))
         (y (- (rdtsc) x)))
    (print "Timing1: " y)
    (print "Timing2: " (- (rdtsc) x)))

  (benchmark 500 (number->string 12345678))
  (benchmark 500 (number->string 12345678 2))
  (benchmark 500 (number->string 12345678 8))
  (benchmark 500 (number->string 12345678 16))

  (benchmark 500 (number->string-2 12345678))
  (benchmark 500 (number->string-2 12345678 2))
  (benchmark 500 (number->string-2 12345678 8))
  (benchmark 500 (number->string-2 12345678 16)))

(global-set-key '#(#\x18 #\t) 'print-simple-timing)
