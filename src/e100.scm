;; -*- coding: utf-8 -*-
;; The Unbelievable Operating System from Hell
;; Copyright © 2007 Göran Weinholt <goran@weinholt.se>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; Intel 8255x 10/100 Mbps Ethernet Controller driver

(define-bytevector-struct i8255x-regs (endianness little)
  (u8 SCB-status-ref)                ;System Control Block status word
  (u8 SCB-STAT/ACK-ref SCB-STAT/ACK-set!)
  (u8 SCB-command-ref SCB-command-set!)
  (u8 SCB-irq-mask-ref SCB-irq-mask-set!)
  (u32 SCB-pointer-ref SCB-pointer-set!)
  (u32 SCB-port-ref SCB-port-set!)
  (u16)
  (u16 EEPROM-ref EEPROM-set!))

(define SCB-STAT-FR #b100000)           ;frame received
(define SCB-STAT-CNA #b10000)           ;CU left the active state
(define SCB-STAT-RNR  #b1000)           ;RU left the ready state

;; Command Unit commands
(define CU-nop                                 #b00000000)
(define CU-start                               #b00010000)
(define CU-resume                              #b00100000)
(define CU-load-dump-counters-address          #b01000000)
(define CU-dump-statistical-counters           #b01010000)
(define CU-load-CU-base                        #b01100000)
(define CU-dump-and-reset-statistical-counters #b01110000)
(define CU-static-resume                       #b10100000)
;; Receive Unit commands
(define RU-nop                   #b0000)
(define RU-start                 #b0001)
(define RU-resume                #b0010)
(define RU-receive-DMA-redirect  #b0011)
(define RU-abort                 #b0100)
(define RU-load-header-data-size #b0101)
(define RU-load-RU-base          #b0110)

(define-bytevector-struct RX-frame-descriptor (endianness little)
  (u16 RX-status-ref RX-status-set!)
  (u16 RX-command-ref RX-command-set!)
  (u32 RX-link-address-ref RX-link-address-set!)
  (u32)
  (u16 RX-actual-count-ref)
  (u16 RX-size-ref RX-size-set!))

(define RX-cmd-end-link #b1000000000000000)
(define RX-cmd-suspend   #b100000000000000)
;; ...

(define RX-status-complete #b1000000000000000)
(define RX-status-ok         #b10000000000000)
;; ...

(define-bytevector-struct command-block (endianness little)
  (u16 CB-status-ref CB-status-set!)
  (u16 CB-command-ref CB-command-set!)
  (u32 CB-link-offset-ref CB-link-offset-set!)
  ;; optional address and data fields follow, the ones for
  ;; transmitting are given here:
  (u32 CB-TBD-address-ref CB-TBD-address-set!)
  (u32 CB-TBD-count-ref CB-TBD-count-set!))

(define CB-cmd-end-link #b1000000000000000)
(define CB-cmd-suspend   #b100000000000000)
(define CB-cmd-interrupt  #b10000000000000)
(define CB-cmd-NOP                   #b000)
(define CB-cmd-mac-address-setup     #b001)
(define CB-cmd-configure             #b010)
(define CB-cmd-multicast-setup       #b011)
(define CB-cmd-transmit              #b100)
(define CB-cmd-load-microcode        #b101)
(define CB-cmd-dump                  #b110)
(define CB-cmd-diagnose              #b111)

(define CB-cmd-transmit-NC         #b10000) ;0=insert CRC and source address
(define CB-cmd-transmit-flexible    #b1000) ;1=flexible mode

(define CB-status-complete #b1000000000000000)
(define CB-status-ok         #b10000000000000)

(define CB-TBD-cmd-EOF     #b1000000000000000)

;;; Code for reading the EEPROM (where the MAC address is stored)

(define-bytevector-struct e100-eeprom (endianness big)
  (u48 e100-MAC-address-ref))

;; SPI bus interface to the EEPROM
(define EESK    #b1)                    ;Serial Clock
(define EECS   #b10)                    ;Chip Select
(define EEDI  #b100)                    ;Serial Data In
(define EEDO #b1000)                    ;Serial Data Out
(define EE-read  #b0110)                ;'read' opcode with start bits

(define (eeprom-send-bits! regs bits length detect-size)
  ;; Send bits in MSB order
  (do ((ret #f)
       (i 0 (+ i 1)))
      ((or ret (= i length)) ret)
    (let ((data (logand EEDI (ash bits (+ (- 3 length) i)))))
      (EEPROM-set! regs (logior EECS data))
      (thread-usleep! 4)
      (EEPROM-set! regs (logior EECS data EESK))
      (thread-usleep! 4)
      ;; Look for the "dummy0" bit, which indicates the end of the
      ;; address field.
      (if (and detect-size (zero? (logand EEDO (EEPROM-ref regs))))
          (set! ret (+ i 1))))))

(define (eeprom-read-bits! regs length)
  ;; Read bits in MSB order
  (do ((sum 0)
       (i 0 (+ i 1)))
      ((> i length) sum)
    (EEPROM-set! regs (logior EECS EESK))
    (thread-usleep! 4)
    (set! sum (logior (ash (logand (EEPROM-ref regs) EEDO) -3)
                      (ash sum 1)))
    (EEPROM-set! regs (logior EECS))
    (thread-usleep! 4)))

(define (eeprom-read! regs address address-length)
  (EEPROM-set! regs EECS)               ;activate the EEPROM
  (thread-usleep! 4)
  (eeprom-send-bits! regs EE-read 4 #f)
  (eeprom-send-bits! regs address address-length #f)
  (let ((result (eeprom-read-bits! regs 16)))
    (EEPROM-set! regs 0)
    result))

(define (eeprom-detect-size! regs)
  (EEPROM-set! regs EECS)
  (thread-usleep! 4)
  (eeprom-send-bits! regs EE-read 4 #f)
  (let ((size (eeprom-send-bits! regs 0 8 #t)))
    (eeprom-read-bits! regs 16)
    (EEPROM-set! regs 0)
    size))

(define (eeprom-read-everything! regs)
  (let* ((eeprom-size (eeprom-detect-size! regs))
         (eeprom (make-bytevector (ash 1 eeprom-size)))
         (maxaddr (ash 1 (- eeprom-size 1))))
    (do ((i 0 (+ i 1)))
        ;; FIXME: remove (> i 6) when usleep is working!
        ((or (> i 6) (= i maxaddr))
         eeprom)
      (bytevector-u16-set! eeprom (* i 2)
                           (eeprom-read! regs i eeprom-size)
                           (endianness little)))))

;;;

(define (sleep-until-command-is-accepted! regs)
  (let lp ()
    (unless (zero? (SCB-command-ref regs))
      (thread-usleep! 1)
      (lp))))

(define (make-new-rx-desc! regs)
  (let ((rxdesc (make-bytevector 1600)))
    (RX-command-set! rxdesc RX-cmd-suspend) ;suspend after receive
    (RX-link-address-set! rxdesc (bytevector-dev-addr rxdesc))
    (RX-size-set! rxdesc (- (bytevector-length rxdesc) 16))
    (SCB-pointer-set! regs (bytevector-dev-addr rxdesc))
    (SCB-command-set! regs RU-start)
    rxdesc))

(define (e100-send-configuration! dev regs promiscuous)
  (let ((c (make-bytevector (+ 8 32))))
    (let-syntax ((byte (syntax-rules ()
                         ((_ i v) (bytevector-u8-set! c (+ i 8) v)))))
      (CB-command-set! c (logior CB-cmd-suspend CB-cmd-configure))
      (CB-link-offset-set! c (bytevector-dev-addr c)) ;link to itself
      
      ;; See the 8255x open source software developer manual for a
      ;; complete map of these configuration bytes.
      (byte 0 22)                       ;22 bytes long configuration
      (byte 1 8)                        ;receive FIFO limit
      ;; TODO: enable MWI if the model is not 82557. Use extended TCB
      ;; if the model is not 82557.
      (byte 6 (logior (if promiscuous
                          #b10000000 #b0) ;save bad frames
                      #b100000          ;standard statistical counters
                      #b10000))         ;standard TCB
                      
      ;; TODO: enable dynamic TBD unless the device is an 82557
      (byte 7 (logior #b10             ;one retransmission on underrun
                      (if promiscuous
                          #b0 #b1)))    ;1=discard short frames

      ;; TODO: on the 82557 bit 0 should be 0 for 503 mode, 1 for MII
      ;; mode. Detect the PHY...
      (byte 8 (logior #b1))

      (byte 10 #b101000)                ;7 bytes pre-amble, no source
                                        ;address insertion

      (byte 12 (logior (ash 6 4)        ;interframe spacing: 6*16 bit times
                       #b1))            ;reserved except for the
                                        ;82557, where it changes the
                                        ;linear priority mode

      (byte 13 0)                       ;these two are some ARP filter...
      (byte 14 #xF2)

      ;; TODO: for 82557/82503 devices, bit 7 should be 1
      (byte 15 (if promiscuous #b1 #b0)) ;promiscous mode

      ;; TODO: for 82557 bit 3 should be 0 here
      (byte 18 #b1111011)               ;frames longer than 1500 bytes
                                        ;are ok, transmitted packets
                                        ;are padded to 64 bytes, short
                                        ;received frames are stripped
                                        ;of their padding

      (byte 19 #b10000000)              ;full duplex pin enable

      (byte 20 #b100000)                ;priority FC location
      (byte 21 #b1000)                  ;FIXME: receive all multicast
                                        ;frames, change this when
                                        ;filtering is implemented

      (SCB-pointer-set! regs (bytevector-dev-addr c))
      (SCB-command-set! regs CU-start)
      (let lp ()
        (unless (logtest CB-status-complete (CB-status-ref c))
          (thread-msleep! 1)
          (lp))))))

(define (e100-init driver dev)
  (let* ((regs (pci-device-get-resource dev 1))
         (irq (pci-device-irq dev))
         (eeprom (eeprom-read-everything! regs))
         (mac-address (e100-MAC-address-ref eeprom))
         (interface (register-ethernet-interface mac-address)))
    (print (interface-name interface) ": Intel 8255x NIC found.")
    (SCB-port-set! regs #b0000)         ;software reset
    (thread-usleep! 10)

    ;; Unmask interrupts...
    (set-irq-handler! irq (self))
    (irq-acknowledge! irq)
    (SCB-irq-mask-set! regs 0)

    ;; Load offset registers
    (SCB-pointer-set! regs 0)           ;RU and CU data is relative to this base
    (SCB-command-set! regs (logior CU-load-CU-base RU-load-RU-base))
    (sleep-until-command-is-accepted! regs)

    ;; FIXME: Do CB-cmd-mac-address-setup and CB-cmd-configure. Add
    ;; support in the networking code for adding multicast addresses
    ;; to an interface, and then use CB-cmd-multicast-setup.
    (e100-send-configuration! dev regs #f)

    (let ((irq? (lambda (x) (eq? x irq)))
          (rxdesc (make-new-rx-desc! regs))
          (txdesc (make-bytevector 128)))
      (forever
        (match (?)
          ;; FIXME: optimize both receiving and sending! This is not
          ;; the way it should be done :)
          ((? irq? _)
           (when (logtest RX-status-complete (RX-status-ref rxdesc))
             (process-ethernet-frame
              interface
              (bytevector-section
               rxdesc 16 (+ (logand #x3fff (RX-actual-count-ref rxdesc))
                            16)))
             ;; Reuse the rxdesc... this needs to be reworked. The
             ;; networking code should decide if the buffer needs to
             ;; be copied or removed from the queue.
             (RX-status-set! rxdesc 0)
             (RX-command-set! rxdesc RX-cmd-suspend) ;suspend after receive
             (RX-link-address-set! rxdesc (bytevector-dev-addr rxdesc))
             (RX-size-set! rxdesc (- (bytevector-length rxdesc) 16))
             (SCB-command-set! regs RU-resume))

           (SCB-STAT/ACK-set! regs #xff)
           (irq-acknowledge! irq))

          (('send (? list? parts))
           ;; `parts' is a list of bytevectors that together
           ;; constitute an ethernet frame.
           (CB-status-set! txdesc 0)
           (CB-command-set! txdesc (logior CB-cmd-suspend CB-cmd-transmit CB-cmd-transmit-flexible))
           (CB-link-offset-set! txdesc (bytevector-dev-addr txdesc)) ;link to itself
           (CB-TBD-address-set! txdesc (+ (bytevector-dev-addr txdesc) 16))
           ;; XXX: add 16 if extended TCBs are implemented

           (do ((iter parts (cdr iter))
                (i 0 (+ i 1)))
               ((null? iter)
                ;; Set the EL bit in the last entry
                (bytevector-u8-set! txdesc (+ 16 (* i 8) 6) 1)
                (CB-TBD-count-set! txdesc (logior (ash 16 16) ;threshold
                                                  (ash i 24) ;TBD count
                                                  (ash 1 16)))) ;EOF
             (bytevector-u32-set! txdesc (+ 16 (* i 8))
                                  (bytevector-dev-addr (car iter))
                                  (endianness little))
             (bytevector-u32-set! txdesc (+ 16 (* i 8) 4)
                                  (bytevector-length (car iter))
                                  (endianness little)))

           (SCB-pointer-set! regs (bytevector-dev-addr txdesc))
           (SCB-command-set! regs CU-start))

          ((from tag 'get-hwaddr)
           (! from (list tag mac-address)))
          (_ #f))))))


(register-driver (make-pci-driver "Intel 8255x 10/100 Mbps Ethernet Controller"
                                  '((#x8086 #x1209 #f #f #f #f #f)
                                    (#x8086 #x1229 #f #f #f #f #f)
                                    (#x8086 #x103d #f #f #f #f #f))
                                  e100-init))

;; debugging aide:
;; (define dev (last *pci-devices*))
;; (define regs (pci-device-get-resource dev 1))
