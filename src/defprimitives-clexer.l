;; -*- coding: utf-8 -*-
;; NYGOS - an operating system
;; Copyright © 2007 Göran Weinholt <goran@weinholt.se>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; Lexer for a subset of C

%%

;; Ignore errors and non-interesting things
<<ERROR>>      (begin (yygetc) (yycontinue))
[ \n\t\r]
^$
\#include      'include

\<[A-Za-z.]+\> (cons 'sysinclude yytext)

\/\*           (let lp ()
                 (case (lexer)
                   ((endcomment) (yycontinue))
                   ((eof)        (lex-error #f #f "EOF inside a comment"))
                   (else         (lp))))

\*\/           'endcomment

[A-Za-z_]+[A-Za-z0-9_]* (cons 'symbol (string->symbol yytext))

[0-9]+         (cons 'number (string->number yytext 10))

,              'comma

\(             'openp
\)             'closep
\{             'openc
\}             'closec
\[             'openb
\]             'closeb
\"             (let lp ((chars '()))
                 (let ((c (cstring-lexer)))
                   (cond ((or (eq? c 'eos))
                          (cons 'string (list->string (reverse chars))))
                         ((char? c)
                          (lp (cons c chars)))
                         (else c))))

<<EOF>>        'eof

%%
