;; -*- coding: utf-8 -*-
;; The Unbelievable Operating System from Hell
;; Copyright © 2005-2006 Göran Weinholt <goran@weinholt.se>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; Library functions from R5RS

(define (eqv? x y)
  (cond ((and (number? x) (number? y))
         (= x y))
        ((and (transcoder? x) (transcoder? y))
         (and (eqv? (transcoder-codec x)
                    (transcoder-codec y))
              (eqv? (transcoder-eol-style x)
                    (transcoder-eol-style y))
              (eqv? (transcoder-error-handling-mode x)
                    (transcoder-error-handling-mode y))))
        (else
         (eq? x y))))

;;;
;; 6.2.5. Numerical operations
;; FIXME: These are dumb like this because only fixnums are implemented so far
(define complex? number?)
(define real? number?)
(define rational? number?)
(define integer? number?)
(define exact? number?)
(define (inexact? x) #f)


(define (abs x)
  (if (negative? x)
      (- x)
      x))

(define quotient /)

(define (numerator x)
  x)
(define (denominator x)
  1)
(define (imag-part x)
  0)
(define (real-part x)
  x)

(define (positive? x)
  (> x 0))

(define (negative? x)
  (< x 0))

(define (odd? x)
  (not (even? x)))

(define (even? x)
  (zero? (modulo x 2)))

(define (max . x)
  (reduce (lambda (x y)
            (if (> x y) x y))
          (car x)
          x))

(define (min . x)
  (reduce (lambda (x y)
            (if (< x y) x y))
          (car x)
          x))

(define (expt n pow)
  (cond ((zero? pow)
         1)
        ((= pow 1)
         n)
        (else
         (expt (+ n n) (- pow 1)))))

;;;
;; 6.1. Equivalence predicates
(define (equal? x y)
  (define (vector=? x y)
    (and (= (vector-length x) (vector-length y))
         (let lp ((i 0))
           (or (= i (vector-length x))
               (and (equal? (vector-ref x i)
                            (vector-ref y i))
                    (lp (+ i 1)))))))
  (define (bytevector=? x y)
    (and (= (bytevector-length x) (bytevector-length y))
         (if (zero? (modulo (bytevector-length x) 2))
             (let lp ((i 0))
               (or (= i (bytevector-length x))
                   (and (= (bytevector-u16-ref x i (native-endianness))
                           (bytevector-u16-ref y i (native-endianness)))
                        (lp (+ i 2)))))
             (let lp ((i 0))
               (or (= i (bytevector-length x))
                   (and (= (bytevector-u8-ref x i)
                           (bytevector-u8-ref y i))
                        (lp (+ i 1))))))))
  (cond ((and (pair? x) (pair? y))
         (and (equal? (car x) (car y))
              (equal? (cdr x) (cdr y))))
        ((and (string? x) (string? y))
         (string=? x y))
        ((and (vector? x) (vector? y))
         (vector=? x y))
        ((and (bytevector? x) (bytevector? y))
         (bytevector=? x y))
        (else
         (eqv? x y))))

;; 6.2.6. Numerical input and output
(define (number->string number . rest)
  (define (digit->char digit)
    (cond ((< digit 10)
           (integer->char (+ (char->integer #\0) digit)))
          (else
           (integer->char (+ (char->integer #\a) (- digit 10))))))
  (if (zero? number)
      (string-copy "0")
      (do ((base (:optional rest 10))
           (i (abs number) (quotient i base))
           (chars '() (cons (digit->char (modulo i base)) chars)))
          ((zero? i)
           (list->string (if (negative? number)
                             (cons #\- chars)
                             chars))))))

;; benchmarking. currently this is faster:

(define (number->string-2 number . rest)
  (let-optionals* rest ((base 10))
    (let-syntax
        ((n->s (syntax-rules ()
                 ((_ number i (shift-i ...) (and-i ...))
                  (let ((length
                         (do ((i (abs number) (shift-i ...))
                              (n (if (negative? number) 1 0) (+ n 1)))
                             ((zero? i) (if (zero? number) 1 n)))))
                    (do ((ret (make-string length #\0))
                         (i (abs number) (shift-i ...))
                         (n (- length 1) (- n 1)))
                        ((zero? i)
                         (if (negative? number)
                             (string-set! ret 0 #\-))
                         ret)
                      (string-set! ret n (string-ref "0123456789abcdef"
                                                     (and-i ...)))))))))
      (case base
        ((10) (n->s number i (quotient i 10) (modulo i 10)))
        ((16) (n->s number i (ash i -4) (logand i #xf)))
        ((8)  (n->s number i (ash i -3) (logand i #x7)))
        ((2)  (n->s number i (ash i -1) (logand i #x1)))
        (else
         (error 'number->string "Unsupported base" (car rest)))))))

(define (string->number x . rest)
  (define (char->digit x)
    (cond ((char-numeric? x)
           (- (char->integer x) (char->integer #\0)))
          ((and (char>=? x #\a)
                (char<=? x #\z))
           (+ 10 (- (char->integer x) (char->integer #\a))))
          ((and (char>=? x #\A)
                (char<=? x #\Z))
           (+ 10 (- (char->integer x) (char->integer #\A))))
          (else #f)))
  (define (list->number numbers base)
    (if (null? numbers)
        #f
        (let lp ((numbers numbers)
                 (sum 0))
          (if (null? numbers)
              sum
              (let ((digit (char->digit (car numbers))))
                (and digit
                     (< digit base)
                     (lp (cdr numbers)
                         (+ (* sum base) digit))))))))
  (let ((base (:optional rest 10))
        (numbers (string->list x)))
    (cond ((null? numbers)
           #f)
          ((equal? numbers '(#\-)) #f)
          ((eq? (car numbers) #\-)
           (- (list->number (cdr numbers) base)))
          ((eq? (car numbers) #\+)
           (list->number (cdr numbers) base))
          (else
           (list->number numbers base)))))

;; 6.3.1. Booleans
(define (not obj)
  (if obj #f #t))

(define (boolean? obj)
  (or (eq? obj #f) (eq? obj #t)))

;;;
;; 6.3.2. Pairs and lists

;;; CAAR to CDDDDR are defined in SRFI 1

(define (null? obj)
  (eq? obj '()))

(define list? proper-list?)

;;; LIST, LENGTH, APPEND, REVERSE, and LIST-REF are defined in SRFI 1

(define list-tail drop)

;;; MEMBER, MEMQ, MEMV, ASSOC are defined in SRFI 1

(define (assq obj list)
  (assoc obj list eq?))

(define (assv obj list)
  (assoc obj list eqv?))

;;;
;; 6.3.4. Characters
(define (char=? char1 char2)
  (= (char->integer char1) (char->integer char2)))

(define (char<? char1 char2)
  (< (char->integer char1) (char->integer char2)))

(define (char>? char1 char2)
  (> (char->integer char1) (char->integer char2)))

(define (char<=? char1 char2)
  (<= (char->integer char1) (char->integer char2)))

(define (char>=? char1 char2)
  (>= (char->integer char1) (char->integer char2)))

(define (char-ci=? char1 char2)
  (char=? (char-downcase char1) (char-downcase char2)))

(define (char-ci<? char1 char2)
  (char<? (char-downcase char1) (char-downcase char2)))

(define (char-ci>? char1 char2)
  (char>? (char-downcase char1) (char-downcase char2)))

(define (char-ci<=? char1 char2)
  (char<=? (char-downcase char1) (char-downcase char2)))

(define (char-ci>=? char1 char2)
  (char>=? (char-downcase char1) (char-downcase char2)))

(define (char-alphabetic? char)
  (or (char-upper-case? char) (char-lower-case? char)))

(define (char-numeric? char)
  (and (char>=? char #\0) (char<=? char #\9)))

;; FIXME: unicode support
(define (char-whitespace? char)
  (let ((i (char->integer char)))
    (if (memq i '(32 9 10 12 13)) #t #f)))

(define (char-upper-case? letter)
  (and (char>=? letter #\A) (char<=? letter #\Z)))

(define (char-lower-case? letter)
  (and (char>=? letter #\a) (char<=? letter #\z)))

(define (char->integer char)
  (if (char? char)
      (unsafe-char->integer char)
      (contract-violation 'char->integer
                          "Not a character" char)))

(define (integer->char sv)
  (if (and (fixnum? sv)
           (or (<= 0 sv #xD7FF) (<= #xE000 sv #x10FFFF)))
      (unsafe-integer->char sv)
      (contract-violation 'integer->char
                          "Value outside of the unicode range" sv)))

(define (char-upcase char)
  (if (and (char>=? char #\a) (char<=? char #\z))
      (integer->char (- (char->integer char) 32))
      char))

(define (char-downcase char)
  (if (and (char>=? char #\A) (char<=? char #\Z))
      (integer->char (+ (char->integer char) 32))
      char))

;;;
;; 6.3.5. Strings

(define (make-string length . rest)
  (let ((fill (:optional rest #\nul)))
    (cond ((not (char? fill))
           (contract-violation 'make-string "expected a character" fill))
          ((char=? fill #\nul)
           ;; Newly allocated memory is always filled with NULs
           (unsafe-bytevector->string
            (make-bytevector (* length 3))))
          (else
           (do ((bv (make-bytevector (* length 3)))
                (fill0 (ash (char->integer fill) -16))
                (fill1 (logand #xff (ash (char->integer fill) -8)))
                (fill2 (logand #xff (char->integer fill)))
                (i 0 (+ i 3)))
               ((= i (bytevector-length bv))
                (unsafe-bytevector->string bv))
             (bytevector-u8-set! bv i fill0)
             (bytevector-u8-set! bv (+ i 1) fill1)
             (bytevector-u8-set! bv (+ i 2) fill2))))))

(define (string . x)
  (list->string x))

(define (string-length x)
  (if (not (string? x))
      (contract-violation 'string-length
                          "not a string argument" x))
  (/ (bytevector-length (unsafe-string->bytevector x))
     3))

(define (string-ref x i)
  (if (not (string? x))
      (contract-violation 'string-ref
                          "not a string argument" x))
  (if (or (negative? i) (>= i (string-length x)))
      (contract-violation 'string-ref
                          "index out of bounds" i))
  (let ((bv (unsafe-string->bytevector x))
        (base (* i 3)))
    (unsafe-integer->char
     (logior (ash (bytevector-u8-ref bv base) 16)
             (ash (bytevector-u8-ref bv (+ base 1)) 8)
             (bytevector-u8-ref bv (+ base 2))))))

(define (string-set! x i c)
  (if (not (string? x))
      (contract-violation 'string-set!
                          "not a string argument" x))
  (if (not (char? c))
      (contract-violation 'string-set!
                          "not a char argument" c))
  (if (or (negative? i) (>= i (string-length x)))
      (contract-violation 'string-ref
                          "index out of bounds" i))
  (let ((bv (unsafe-string->bytevector x))
        (base (* i 3))
        (i (unsafe-char->integer c)))
    (bytevector-u8-set! bv base (ash i -16))
    (bytevector-u8-set! bv (+ base 1) (logand #xff (ash i -8)))
    (bytevector-u8-set! bv (+ base 2) (logand #xff i))))

(define (string=? x1 x2)
  (and (= (string-length x1) (string-length x2))
       (let ((len (string-length x1)))
         (let lp ((i 0))
           (or (= i len)
               (and (char=? (string-ref x1 i)
                            (string-ref x2 i))
                    (lp (+ i 1))))))))

(define (string-ci=? x1 x2)
  (and (= (string-length x1) (string-length x2))
       (let ((len (string-length x1)))
         (let lp ((i 0))
           (or (= i len)
               (and (char-ci=? (string-ref x1 i)
                               (string-ref x2 i))
                    (lp (+ i 1))))))))

(define (string-append . x)
  (let ((length (apply + (map string-length x))))
    (do ((str (make-string length))
         (x x (cdr x))
         (n 0 (+ n (string-length (car x)))))
        ((null? x) str)
      (do ((s (car x))
           (i 0 (+ i 1)))
          ((= i (string-length s)))
        (string-set! str (+ n i) (string-ref s i))))))

(define (string->list v)
  (do ((i (- (string-length v) 1) (- i 1))
       (ret '() (cons (string-ref v i) ret)))
      ((negative? i) ret)))

(define (list->string list)
  (do ((string (make-string (length list)))
       (n 0 (+ n 1))
       (l list (cdr l)))
      ((null? l) string)
    (string-set! string n (car l))))

(define (string-copy string)
  (do ((length (string-length string))
       (copy (make-string (string-length string)))
       (n 0 (+ n 1)))
      ((= n length) copy)
    (string-set! copy n (string-ref string n))))

(define (string-fill! string fill)
  (do ((length (string-length string))
       (n 0 (+ n 1)))
      ((= n length))
    (string-set! string n fill)))

(define (substring string start end)
  (let ((len (- end start)))
    (do ((new (make-string len))
         (i 0 (+ i 1)))
        ((= i len) new)
      (string-set! new i (string-ref string (+ i start))))))



;;;
;; 6.3.6. Vectors

(define (make-vector length . rest)
  (unless (and (fixnum? (+ length 1)) (>= length 0))
    ;; XXX: should be &implementation-restriction
    (contract-violation 'make-vector "length+1 must be a non-negative fixnum" length))
  (do ((fill (:optional rest #f))
       (vector (unsafe-make-value (+ length 1) vector))
       (i 0 (+ i 1)))
      ((= i length)
       (value-set! vector 0 length)
       vector)
    (value-set! vector (+ i 1) fill)))

(define (vector . list)
  (list->vector list))

(define (vector-length x)
  (if (not (vector? x))
      (contract-violation 'vector-length
                          "not a vector argument" x))
  (value-ref x 0))

(define (vector-ref x i)
  (if (not (vector? x))
      (contract-violation 'vector-ref
                          "not a vector argument" x))
  (if (or (negative? i) (>= i (value-ref x 0)))
      (contract-violation 'vector-ref
                          "index out of bounds" x i))
  (value-ref x (+ i 1)))

(define (vector-set! x i v)
  (if (not (vector? x))
      (contract-violation 'vector-set!
                          "not a vector argument" x))
  (if (or (negative? i) (>= i (value-ref x 0)))
      (contract-violation 'vector-ref
                          "index out of bounds" i))
  (value-set! x (+ i 1) v))

(define (vector->list v)
  (do ((i (- (vector-length v) 1) (- i 1))
       (ret '() (cons (vector-ref v i) ret)))
      ((negative? i) ret)))

(define (list->vector list)
  (do ((vector (make-vector (length list)))
       (n 0 (+ n 1))
       (l list (cdr l)))
      ((null? l) vector)
    (vector-set! vector n (car l))))

(define (vector-fill! vector fill)
  (do ((length (vector-length vector))
       (n 0 (+ n 1)))
      ((= n length))
    (vector-set! vector n fill)))


;;;
;; 6.4. Control features

;;; MAP is defined in SRFI 1

(define (for-each proc list . extra)
  (let lp ((l list) (x extra))
    (unless (null? l)
      (apply proc (car l) (map car x))
      (lp (cdr l) (map cdr x)))))

;;; XXX: remove!
(define (map proc list . extra)
  (let lp ((ret '()) (l list) (x extra))
    (if (or (null? l) (any null? x))
        (reverse ret)
        (lp (cons (apply proc (car l) (map car x)) ret)
            (cdr l) (map cdr x)))))

(define (values . x)
  x)
(define (call-with-values prod cons)
  (apply cons (prod)))

(define (call-with-current-continuation proc)
  ;; FIXME: support more than one return value
  (let ((stack (sys!copy-stack (self))))
    (if (bytevector? stack)
        (proc (lambda values
                (sys!restore-stack (self) stack values)))
        (car stack))))

(define call/cc call-with-current-continuation)
