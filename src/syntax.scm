(define-syntax forever
  (syntax-rules ()
    ((_ body ...)
     (let lp ()
       body ...
       (lp)))))

(define-syntax while
  (syntax-rules ()
    ((_ test body ...)
     (let lp ()
       (when test
         body ...
         (lp))))))

(define-syntax when
  (syntax-rules ()
    ((_ test body ...)
     (if test
         (begin body ...)))))

(define-syntax unless
  (syntax-rules ()
    ((_ test body ...)
     (if (not test)
         (begin body ...)))))

(define-syntax :optional
  (syntax-rules ()
    ((_ args default)
     (cond ((null? args)
            default)
           ((and (pair? args) (null? (cdr args)))
            (car args))
           (else
            (error ":optional received more than one argument" args))))))

(define-syntax cond-expand
  (syntax-rules (and or not else srfi-0 srfi-1 nygos)
    ((cond-expand) (syntax-error "Unfulfilled cond-expand"))
    ((cond-expand (else body ...))
     (begin body ...))
    ((cond-expand ((and) body ...) more-clauses ...)
     (begin body ...))
    ((cond-expand ((and req1 req2 ...) body ...) more-clauses ...)
     (cond-expand
       (req1
         (cond-expand
           ((and req2 ...) body ...)
           more-clauses ...))
       more-clauses ...))
    ((cond-expand ((or) body ...) more-clauses ...)
     (cond-expand more-clauses ...))
    ((cond-expand ((or req1 req2 ...) body ...) more-clauses ...)
     (cond-expand
       (req1
        (begin body ...))
       (else
        (cond-expand
           ((or req2 ...) body ...)
           more-clauses ...))))
    ((cond-expand ((not req) body ...) more-clauses ...)
     (cond-expand
       (req
         (cond-expand more-clauses ...))
       (else body ...)))
    ((cond-expand (srfi-0 body ...) more-clauses ...)
       (begin body ...))
    ((cond-expand (srfi-1 body ...) more-clauses ...)
       (begin body ...))
    ((cond-expand (nygos body ...) more-clauses ...)
       (begin body ...))
    ((cond-expand (feature-id body ...) more-clauses ...)
       (cond-expand more-clauses ...))))

;;; SRFI 8: receive: Binding to multiple values
(define-syntax receive
  (syntax-rules ()
    ((receive formals expression body ...)
     (call-with-values (lambda () expression)
                       (lambda formals body ...)))))

;;; SRFI 11: Syntax for receiving multiple values
;; This code is in the public domain.

(define-syntax let-values
  (syntax-rules ()
    ((let-values (?binding ...) ?body0 ?body1 ...)
     (let-values "bind" (?binding ...) () (begin ?body0 ?body1 ...)))

    ((let-values "bind" () ?tmps ?body)
     (let ?tmps ?body))

    ((let-values "bind" ((?b0 ?e0) ?binding ...) ?tmps ?body)
     (let-values "mktmp" ?b0 ?e0 () (?binding ...) ?tmps ?body))

    ((let-values "mktmp" () ?e0 ?args ?bindings ?tmps ?body)
     (call-with-values
       (lambda () ?e0)
       (lambda ?args
         (let-values "bind" ?bindings ?tmps ?body))))

    ((let-values "mktmp" (?a . ?b) ?e0 (?arg ...) ?bindings (?tmp ...) ?body)
     (let-values "mktmp" ?b ?e0 (?arg ... x) ?bindings (?tmp ... (?a x)) ?body))

    ((let-values "mktmp" ?a ?e0 (?arg ...) ?bindings (?tmp ...) ?body)
     (call-with-values
       (lambda () ?e0)
       (lambda (?arg ... . x)
         (let-values "bind" ?bindings (?tmp ... (?a x)) ?body))))))

(define-syntax let*-values
  (syntax-rules ()
    ((let*-values () ?body0 ?body1 ...)
     (begin ?body0 ?body1 ...))

    ((let*-values (?binding0 ?binding1 ...) ?body0 ?body1 ...)
     (let-values (?binding0)
       (let*-values (?binding1 ...) ?body0 ?body1 ...)))))

;;; SRFI-16: Syntax for procedures of variable arity.
;; This code is in the public domain.

(define-syntax case-lambda
  (syntax-rules ()
    ((case-lambda
       (?a1 ?e1 ...)
       ?clause1 ...)
     (lambda args
       (let ((l (length args)))
         (case-lambda "CLAUSE" args l
           (?a1 ?e1 ...)
           ?clause1 ...))))
    ((case-lambda "CLAUSE" ?args ?l
       ((?a1 ...) ?e1 ...)
       ?clause1 ...)
     (if (= ?l (length '(?a1 ...)))
         (apply (lambda (?a1 ...) ?e1 ...) ?args)
         (case-lambda "CLAUSE" ?args ?l
           ?clause1 ...)))
    ((case-lambda "CLAUSE" ?args ?l
       ((?a1 . ?ar) ?e1 ...)
       ?clause1 ...)
     (case-lambda "IMPROPER" ?args ?l 1 (?a1 . ?ar) (?ar ?e1 ...)
       ?clause1 ...))
    ((case-lambda "CLAUSE" ?args ?l
       (?a1 ?e1 ...)
       ?clause1 ...)
     (let ((?a1 ?args))
       ?e1 ...))
    ((case-lambda "CLAUSE" ?args ?l)
     (error "Wrong number of arguments to CASE-LAMBDA."))
    ((case-lambda "IMPROPER" ?args ?l ?k ?al ((?a1 . ?ar) ?e1 ...)
       ?clause1 ...)
     (case-lambda "IMPROPER" ?args ?l (+ ?k 1) ?al (?ar ?e1 ...)
       ?clause1 ...))
    ((case-lambda "IMPROPER" ?args ?l ?k ?al (?ar ?e1 ...)
       ?clause1 ...)
     (if (>= ?l ?k)
         (apply (lambda ?al ?e1 ...) ?args)
         (case-lambda "CLAUSE" ?args ?l
           ?clause1 ...)))))
