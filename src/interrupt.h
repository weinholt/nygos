/* -*- coding: utf-8 -*-
 * The Unbelievable Operating System from Hell
 * Copyright © 2005-2006 Göran Weinholt <goran@weinholt.se>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _INTERRUPT_H
#define _INTERRUPT_H

#include <malloc.h>

static inline void __attribute__((always_inline)) disable_interrupts(void)
{
	asm("cli"::);
}

static inline void __attribute__((always_inline)) enable_interrupts(void)
{
	asm("sti"::);
}

/* IRQ n is mapped to interrupt IRQ_START+n. The first 32 interrupt
 * numbers are reserved. */
#define IRQ_START	32
#define IRQ(n)		(IRQ_START+(n))

/* Interrupt Stack Table numbers. */
/* When in an ISR, interrupts are disabled. So an IRQ can never
 * interrupt an IRQ, thus all IRQs only need one stack. */
#define IST_IRQ        1
/* Page faults can happen in ISRs if the ISR is manipulating a
 * thread's stack. (They can't happen right now, but they will when
 * paging is implemented). Give the page fault handler its own
 * stack. */
#define IST_PAGE_FAULT 2
/* FIXME: sort out what other faults can happen in ISRs. */
#define IST_FAULT      3
/* Non-maskable interrupt. These happen when an NMI watchdog is used.
 * Certain computers are also wired up with a button that will trigger
 * an NMI. Use a special stack for these, as they can presumably
 * happen at any time.. */
#define IST_NMI        4

typedef void (*int_handler) (void);

void interrupt_init(void);
void set_intvec(int n, int_handler handler, int stack);

void irq_enable(int irq);
void irq_disable(int irq);

#endif
