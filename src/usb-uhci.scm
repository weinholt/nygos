;; -*- coding: utf-8 -*-
;; The Unbelievable Operating System from Hell
;; Copyright © 2006-2007 Göran Weinholt <goran@weinholt.se>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; Driver for the UHCI USB host controller

;; XXX: Ugly DMA allocator that allocates from <1M, just to make
;; prototyping the UHCI driver easier.

;; Something better would be DMA pools. The DMA pool is created for a
;; specific size of memory allocations and for a specific PCI device.
;; These pools wont get fragmented since all allocations are the same
;; size. When they run out of memory they request more locked pages.
;; What are we to do about allocations bigger than a page when we
;; don't have an IOMMU? We also need to provide a way to get the
;; address of the allocated bytes from the perspective of the PCI
;; device.

(define (align x to)
  (logand (+ x (- to 1))
          (logxor #xffffffff (- to 1))))

(define dma-alloc-process
  (spawn
   (lambda ()
     (define next-free 4096)
     (forever
       (let ((m (?)))
         (cond ((pair? m)
                (let ((pid (car m))
                      (size (cdr m)))
                  (if (> next-free (* 638 1024))
                      (error 'dma-malloc "DMA memory has been exhausted"))
                  (set! next-free (align next-free size))
                  (let ((ret (make-bytevector/direct size next-free)))
                    (set! next-free (+ next-free size))
                    (! pid ret))))))))))

(define (dma-alloc size device)
  "Allocates bytes that will not move around during garbage
collection. The allocated bytes are aligned by their size. The device
argument is needed to allow the device access to the allocated bytes."
  ;; IA-32 doesn't have an IOMMU though, so the device argument
  ;; currently does nothing.
  ;;   (if (not (memq size '(4 8 16 32 64 128 256 512 1024 2048 4096)))
  ;;       (contract-violation 'dma-alloc "unallowed size" size))
  (! dma-alloc-process (cons (self) size))
  (?? bytevector?))


(define (dma-free obj)
  #f)


;;; actual driver follows

(define-record-type uhci-device (make-uhci-device pci-device regs
                                                  ports qh-interrupt
                                                  qh-control qh-bulk)
  uhci-device?
  (pci-device uhci-device-pci-device)
  (regs uhci-device-regs)
  (ports uhci-device-ports)
  (qh-interrupt uhci-qh-interrupt)
  (qh-control uhci-qh-control)
  (qh-bulk uhci-qh-bulk)
  (next-addr uhci-next-addr uhci-next-addr-set!))

(define-bytevector-struct uhci-regs (endianness little)
  (u16 USBCMD-ref USBCMD-set!)
  (u16 USBSTS-ref USBSTS-set!)
  (u16 USBINTR-ref USBINTR-set!)
  (u16 FRNUM-ref FRNUM-set!)
  (u32 FRBASEADD-ref FRBASEADD-set!)
  (u8 SOFMOD-ref SOFMOD-set!)
  ;;(u16 PORTSC0-ref)
  ;;(u16 PORTSC1-ref)
  )


(define-bytevector-struct transfer-descriptor (endianness little)
  (u32 td-link-ref td-link-set!)
  (u32 td-ctlsts-ref td-ctlsts-set!)
  (u32 td-token-ref td-token-set!)
  (u32 td-buffer-ref td-buffer-set!))
(define (td-status-ref td)
  (bit-field (td-ctlsts-ref td) 16 24))
(define (td-actlen-ref td)
  (+ 1 (logand (td-ctlsts-ref td) #b1111111111)))
(define (td-active? td)
  (= 1 (bit-field (td-ctlsts-ref td) 23 24)))
(define PID-IN #x69)
(define PID-OUT #xE1)
(define PID-SETUP #x2D)

(define (print-TD td)
  (define (display-flag desc bit always)
    (let ((b (ash 1 bit)))
      (cond ((= (logand (td-ctlsts-ref td) b) b)
             (! vga-text-process '(attr . 12))
             (display desc) (display "+ ")
             (! vga-text-process '(attr . 7)))
            (always
             (display desc)
             (display "- ")))))
  (display-flag "SPD" 29 #t)
  (display "ErrC=")
  (display (bit-field (td-ctlsts-ref td) 27 29))
  (display " ")
  (display-flag "Low" 26 #t)
  (display-flag "IOS" 25 #t)
  (display-flag "IOC" 24 #t)
  (display-flag "Active" 23 #t)
  (display-flag "Stalled" 22 #f)
  (display-flag "DataBuffer" 21 #f)
  (display-flag "Babble" 20 #f)
  (display-flag "NAK" 19 #f)
  (display-flag "CRC/Timeout" 18 #f)
  (display-flag "Bitstuff" 17 #f)
  (print "ActLen=" (td-actlen-ref td)))

(define-bytevector-struct queue-header (endianness little)
  (u32 qh-head-ref qh-head-set!)
  (u32 qh-element-ref qh-element-set!))




(define (qh-alloc dev)
  ;; Actually just 8 bytes, but must be aligned on 16 bytes
  (dma-alloc 16 (uhci-device-pci-device dev)))

(define td-alloc-process
  ;; XXX: this will not be needed later, if DMA pools or something is
  ;; implemented. It's also seems rather dumb to do it like this...
  (spawn
   (lambda ()
     (let lp ((free-tds '()))
       (let ((m (?)))
         (cond ((thread? m)
                (cond ((null? free-tds)
                       ;; Actually 32 bytes, but the last 16 bytes are reserved for
                       ;; software use, and the structure is aligned on 16 bytes.
                       (! m (dma-alloc 16 #f))
                       (lp free-tds))
                      (else
                       (! m (car free-tds))
                       (lp (cdr free-tds)))))
               ((bytevector? m)
                (lp (cons m free-tds)))))))))

(define (td-alloc dev)
  (! td-alloc-process (self))
  (?? bytevector?))

(define (td-release! dev td)
  (! td-alloc-process td))

(define (queue-control-td uhci-device td)
  (if (not (td-active? td))
      (contract-violation 'queue-control-td
                          "tried to queue an inactive TD" td))
  (qh-element-set! (uhci-qh-control uhci-device)
                   (bytevector-dev-addr td)))

(define (dequeue-control-td uhci-device)
  (qh-element-set! (uhci-qh-control uhci-device)
                   1))

(define (td-wait! uhci-device td)
  (do ((try 0 (+ try 1)))
      ((or (= try 50) (not (td-active? td))))
    (thread-msleep! 1))
  (unless (zero? (td-status-ref td))
    (print-TD td)
    (error 'td-wait! "TD has failed to complete!"
           (td-ctlsts-ref td))))

(define (uhci-send-setup-request uhci-device usbdev endpoint req)
  (define (make-toggle init)
    (let ((toggle (if (zero? init) 1 0)))
      (lambda ()
        (set! toggle (logxor 1 toggle))
        toggle)))
  (let ((td (td-alloc uhci-device))
        (no-data (zero? (setup-length-ref req)))
        (reply (or (zero? (setup-length-ref req))
                   (dma-alloc (setup-length-ref req)
                              (uhci-device-pci-device
                               uhci-device))))
        (toggle (make-toggle 0)))

    ;; Setup stage
    (td-link-set! td #b1)
    (td-buffer-set! td (bytevector-dev-addr req))
    (td-token-set! td (logior (ash (- (bytevector-length req) 1) 21)
                              (ash (toggle) 19)
                              (ash endpoint 15)
                              (ash (usb-device-address usbdev) 8)
                              PID-SETUP))
    (td-ctlsts-set! td (logior (ash #b11 27) ; max three errors
                               (if (usb-device-low-speed? usbdev)
                                   (ash 1 26)
                                   0)
                               (ash 1 23))) ; set TD to active

    (queue-control-td uhci-device td)
    (td-wait! uhci-device td)
    (dequeue-control-td uhci-device)

    (let get-more ((offset 0)
                   (remaining (setup-length-ref req)))
      ;; Data state
      (unless (zero? remaining)
        (td-link-set! td #b1)
        (td-buffer-set! td (+ (bytevector-dev-addr reply) offset))
        (td-token-set! td (logior (ash (- (min (usb-device-max-packet0
                                                usbdev)
                                               remaining)
                                          1)
                                       21)
                                  (ash (toggle) 19)
                                  (ash endpoint 15)
                                  (ash (usb-device-address usbdev) 8)
                                  PID-IN))
        (td-ctlsts-set! td (logior (ash #b11 27) ; max three errors
                                   (if (usb-device-low-speed? usbdev)
                                       (ash 1 26)
                                       0)
                                   (ash 1 23))) ; set TD to active

        (queue-control-td uhci-device td)
        (td-wait! uhci-device td)
        (dequeue-control-td uhci-device))

      ;; (bytevector-u8-ref reply 0) ; bytes available
      ;; (+ offset (td-actlen-ref td)) ; bytes received
      ;; (setup-length-ref req) ; bytes requested
      (cond ((or no-data
                 (= (+ offset (td-actlen-ref td)) ; received
                    (setup-length-ref req))) ; requested
             (when (bytevector? reply)
               (display "<- ")
               (do ((i 0 (+ i 1)))
                   ((= i (setup-length-ref req)))
                 (display (bytevector-u8-ref reply i))
                 (display " "))
               (newline))

             ;; Status state
             (td-link-set! td #b1)
             (td-buffer-set! td 0)
             (td-token-set! td (logior (ash #x7ff 21) ; empty
                                       (ash 1 19)
                                       (ash endpoint 15)
                                       (ash (usb-device-address usbdev) 8)
                                       (if no-data PID-IN PID-OUT)))
             (td-ctlsts-set! td (logior (ash #b11 27) ; max three errors
                                        (if (usb-device-low-speed? usbdev)
                                            (ash 1 26)
                                            0)
                                        (ash 1 23))) ; set TD to active

             (queue-control-td uhci-device td)
             (td-wait! uhci-device td)
             (dequeue-control-td uhci-device)

             (td-release! uhci-device td)
             reply)
            ((< (+ offset (td-actlen-ref td)) ; received
                (setup-length-ref req))
             (get-more (+ offset (td-actlen-ref td))
                       (- remaining (td-actlen-ref td))))
            (else
             (error 'send-setup-request
                    "somehow we got more data than requested"
                    `(offset . ,offset)
                    `(actlen . ,(td-actlen-ref td))
                    `(setup-length . ,(setup-length-ref req))
                    `(reply0 . ,(bytevector-u8-ref reply 0))))))))

(define (uhci-do-setup-request uhci-device usbdev endpoint data)
  (let ((req (dma-alloc 8 (uhci-device-pci-device uhci-device))))
    (bytevector-copy! data 0 req 0 8)
    (uhci-send-setup-request uhci-device usbdev endpoint req)))


(define (uhci-hcd-init driver dev)
  (define (count-ports regs)
    ;; Check if a port is present by testing bit 7, which always is 1
;     (do ((maxports (/ (- (bytevector-length regs) #x10) 2))
;          (port 0 (+ port 1)))
;         ((or (= port maxports)
;              (zero? (logand (PORTSCn-ref regs port) #b10000000)))
;          port))
    2)

  (let* ((regs (pci-device-get-resource dev 0))
         (ports (count-ports regs))
         (sofmod (SOFMOD-ref regs)))

    (print "UHCI controller with " ports " USB ports detected.")

    (let ((framelist (dma-alloc 4096 dev))
          (qh-interrupt (dma-alloc 16 dev))
          (qh-control (dma-alloc 16 dev))
          (qh-bulk (dma-alloc 16 dev)))
      (pci-device-enable! dev)
      ;; Turn off legacy support (zero all R/W bits and clear all R/WC bits)
      (pci-device-conf-write! dev #xc0 'u16 #x8f00)
      ;; Do a host controller reset
      (USBCMD-set! regs 2)
      (thread-msleep! 100)
      ;; Disable interrupts and stop the controller
      (USBINTR-set! regs 0)
      (USBCMD-set! regs 0)
      ;; Set the SOF timing to the same value that the glorious BIOS
      ;; found in all of its internal wisdom.
      (SOFMOD-set! regs sofmod)
      ;; Reset the port status and control registers
      (do ((i 0 (+ i 1)))
          ((= i ports))
        (PORTSCn-set! regs i PORTSC-R/WC))

      ;; Set the base address for the frame table
      (FRBASEADD-set! regs (bytevector-dev-addr framelist))
      ;; Have the controller start at frame 0
      (FRNUM-set! regs 0)

      ;; Enable pirq (not used right now)
      (pci-device-conf-write! dev #xc0 'u16 #x2000)

      ;; Set up the queues to execute like interrupt -> control -> bulk.
      (qh-head-set! qh-interrupt (logior (bytevector-dev-addr qh-control) #b10))
      (qh-element-set! qh-interrupt #b1)
      (qh-head-set! qh-control (logior (bytevector-dev-addr qh-bulk) #b10))
      (qh-element-set! qh-control #b1)
      (qh-head-set! qh-bulk #b1)
      (qh-element-set! qh-bulk #b1)

      ;; Set all frame pointers to point at qh-interrupt
      (do ((i 0 (+ i 4)))
          ((= i 4096))
        (bytevector-u32-set! framelist i
                             (logior (bytevector-dev-addr qh-interrupt) #b10)
                             (endianness little)))

      ;; Start it!
      (USBCMD-set! regs #b11000001)
      (USBINTR-set! regs #b1111)

      ;; The UHCI controller is now up and running through the queues
      ;; every millisecond.

      (let* ((uhci-device (make-uhci-device dev regs ports qh-interrupt
                                            qh-control qh-bulk))
             (root-hub (make-usb-device (self) #f #f #f #f #f)))
        (usb-device-thread-set! root-hub (spawn (make-uhci-hcd-root-hub-driver uhci-device root-hub)))
        (usb-register-hcd (self) root-hub)
        (uhci-hcd-bus-driver uhci-device)))))

(define (uhci-hcd-bus-driver uhci-device)
  "USB bus driver for the UHCI host controller. All communication
is with the USBD."
  (forever
    (match (?)
      ((pid tag ('setup-request usbdev endpoint (? bytevector? req)))
       (! pid (list tag (uhci-do-setup-request uhci-device usbdev endpoint req))))
      
      (_ #f))))

(register-driver
 (make-pci-driver "USB UHCI driver"
                  '((#f #f #f #f #x0c #x03 #x00))
                  uhci-hcd-init))
