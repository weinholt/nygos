/* -*- coding: utf-8 -*-
 * The Unbelievable Operating System from Hell
 * Copyright © 2005-2007 Göran Weinholt <goran@weinholt.se>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <nygos.h>
#include <interrupt.h>
#include <malloc.h>
#include <runtime.h>
#include <number.h>
#include <multitasking.h>

struct idt_descriptor {
	uint16 offset_0_15;
	uint16 select;				/* %cs */
	uint16 type;
	uint16 offset_16_31;
	uint32 offset_32_63;
	uint32 reserved;
} __attribute__((packed, aligned(8)));

#define IDT_SIZE 0xff

static struct idt_descriptor idt[IDT_SIZE];
static uint8 pic1_mask, pic2_mask;
static SCM interrupt_handlers[IDT_SIZE];

extern void wrap_int_default_reserved(void);
extern void int_default_irq(void);
extern void idt_fault_DE(void);
extern void wrap_fault_DB(void);
extern void idt_int_NMI(void);
extern void wrap_trap_BP(void);
extern void wrap_fault_UD(void);
extern void wrap_fault_GP(void);
extern void wrap_fault_PF(void);

extern void wrap_irq_scheme_0(void);
extern void wrap_irq_scheme_1(void);
extern void wrap_irq_scheme_2(void);
extern void wrap_irq_scheme_3(void);
extern void wrap_irq_scheme_4(void);
extern void wrap_irq_scheme_5(void);
extern void wrap_irq_scheme_6(void);
extern void wrap_irq_scheme_7(void);
extern void wrap_irq_scheme_8(void);
extern void wrap_irq_scheme_9(void);
extern void wrap_irq_scheme_10(void);
extern void wrap_irq_scheme_11(void);
extern void wrap_irq_scheme_12(void);
extern void wrap_irq_scheme_13(void);
extern void wrap_irq_scheme_14(void);
extern void wrap_irq_scheme_15(void);

static int_handler scheme_irq_wrappers[] = {
	wrap_irq_scheme_0, wrap_irq_scheme_1, wrap_irq_scheme_2,
	wrap_irq_scheme_3, wrap_irq_scheme_4, wrap_irq_scheme_5,
	wrap_irq_scheme_6, wrap_irq_scheme_7, wrap_irq_scheme_8,
	wrap_irq_scheme_9, wrap_irq_scheme_10, wrap_irq_scheme_11,
	wrap_irq_scheme_12, wrap_irq_scheme_13, wrap_irq_scheme_14,
	wrap_irq_scheme_15
};

void interrupt_init(void)
{
	struct {
		uint16 limit;
		uint64 base;
	} __attribute__((packed)) idtr = {
		sizeof(idt) - 1,
		(uint64) idt
	};

	/* Program PIC1... */
	outb(0x11, 0x20);			/* Init, edge triggered, send ICW2 */
	outb(0x11, 0xa0);
	outb(IRQ_START, 0x21);		/* Offset for IRQ -> interrupt mapping */
	outb(IRQ_START + 8, 0xa1);
	outb(4, 0x21);				/* Slave PIC2 to PIC1 */
	outb(2, 0xa1);
	outb(0x01, 0x21);			/* 8086 mode, not SFNM, unbuffered, normal EIO */
	outb(0x01, 0xa1);

	pic1_mask = pic2_mask = 0xff; /* mask all interrupts */
	outb(pic1_mask, 0x21);
	outb(pic2_mask, 0xa1);

	asm volatile("lidt %0"::"m"(idtr));

	for (int i = 0; i < IDT_SIZE; i++) {
		set_intvec(i, i < 32 ? wrap_int_default_reserved : int_default_irq, IST_IRQ);
		interrupt_handlers[i] = EOL;
	}

	set_intvec(0, idt_fault_DE, IST_FAULT);
	set_intvec(1, wrap_fault_DB, IST_FAULT);
	set_intvec(2, idt_int_NMI, IST_NMI);
	set_intvec(3, wrap_trap_BP, IST_FAULT);
	set_intvec(6, wrap_fault_UD, IST_FAULT);
	set_intvec(13, wrap_fault_GP, IST_FAULT);
	set_intvec(14, wrap_fault_PF, IST_PAGE_FAULT);

	enable_interrupts();
	/* Make sure the code can handle spurious IRQs. */
	asm volatile("int %0" : : "N"(IRQ(7)));
}

void irq_enable(int irq)
{
	disable_interrupts();
	if (irq < 8) {
		pic1_mask &= ~(1 << irq);
		outb(pic1_mask, 0x21);
	} else {
		pic2_mask &= ~(1 << (irq - 8));
		outb(pic2_mask, 0xa1);
	}
	enable_interrupts();
}

void irq_disable(int irq)
{
	disable_interrupts();
	pic1_mask |= 1 << irq;
	outb(pic1_mask, 0x21);
	enable_interrupts();
}

void set_intvec(int n, int_handler handler, int stack)
{
	idt[n].offset_0_15 = (uint64) handler & 0x0000FFFF;
	idt[n].select = 8;
	idt[n].type = 0x8E00 | (stack & 7);
	idt[n].offset_16_31 = ((uint64) handler & 0xFFFF0000) >> 16;
	idt[n].offset_32_63 = ((uint64) handler & 0xFFFFFFFF00000000) >> 32;
}

#include <stdio.h>
void idt_int_default_reserved(void)
{
	panic("Unhandled (possibly reserved) CPU exception.");
}

void idt_fault_DB(void) {
	/* Can't really happen, because the debug registers aren't used. */
	panic("Debug Exception (#DB)");
}

void idt_trap_BP(void) {
	panic("Breakpoint Exception (#BP)");
}

void idt_trap_OF(void) {
	/* The INTO instruction is not available in 64-bit mode, so this
	 * can't happen. */
	panic("Overflow Exception (#OF)");
}

void idt_fault_BR(void) {
	/* The BOUND instruction is not available in 64-bit mode, so this
	 * can't happen. */
	panic("BOUND Range Exceeded Exception (#BR)");
}

void idt_fault_UD(void) {
	panic("Invalid Opcode Exception (#UD)");
}

void idt_fault_NM(void) {
	panic("Device Not Available Exception (#NM)");
}

void idt_abort_DF(void) {
	panic("Double Fault Exception (#DF)");
}

void idt_fault_TS(void) {
	panic("Invalid TSS Exception (#TS)");
}

void idt_fault_NP(void) {
	panic("Segment Not Present (#NP)");
}

void idt_fault_SS(void) {
	panic("Stack Fault Exception (#SS)");
}

void idt_fault_GP(void) {
	panic("General Protection Exception (#GP)");
}

void idt_fault_PF(int64 code)
{
	int64 address;

	asm volatile("mov %%cr2, %0":"=r"(address));

	puts("CR2=#x");
	puti(address, 16);
	puts(" code=#x");
	puti(code, 16);
	putchar('\n');
	panic("Page-Fault Exception (#PF)");
}

void idt_fault_MF(void) {
	panic("x87 FPU Floating-Point Error (#MF)");
}

void idt_fault_AC(void) {
	/* Alignment is only checked for CPL=3, so this can't happen. */
	panic("Alignment Check Exception (#AC)");
}

void idt_abort_MC(void) {
	/* Not yet initialized, so can't happen. */
	panic("Machine-Check Exception (#MC)");
}

void idt_fault_XF(void) {
	panic("SIMD Floating-Point Exception (#XF)");
}

void idt_irq_scheme(int irq)
{
	if (UNLIKELY(interrupt_handlers[IRQ(irq)] == EOL)) {
		puts("IRQ=");
		puti(irq, 10);
		putchar('\n');
		panic("got a disabled irq");
	}
	multitasking_send(interrupt_handlers[IRQ(irq)], number(irq));
	if (irq >= 8)
		outb(0x60 + 2, 0x20);	/* acknowledge cascade */
}

defun (set_irq_handler_ex, "set-irq-handler!", 2, 0, (SCM s_irq, SCM s_thread),
	   "Set the IRQ handler for `s_irq' to `s_thread'.", {
	TYPE_ASSERT(IS_INUM(s_irq), s_irq, 0);
	int irq = INUM_TO_INT(s_irq);
	TYPE_ASSERT(irq > 0 && irq < (int) buflen(scheme_irq_wrappers), s_irq, 0);
	TYPE_ASSERT(IS_THREAD(s_thread), s_thread, 1);

	interrupt_handlers[IRQ(irq)] = s_thread;
	disable_interrupts();
	set_intvec(IRQ(irq), scheme_irq_wrappers[irq], IST_IRQ);
	irq_enable(irq);
	if (irq >= 8)
		irq_enable(2);

	return UNSPEC;
})

defun (irq_eoi_ex, "irq-acknowledge!", 1, 0, (SCM s_irq),
	   "Declare that the specified interrupt has been serviced.", {
	TYPE_ASSERT(IS_INUM(s_irq), s_irq, 0);
	int irq = INUM_TO_INT(s_irq);
	TYPE_ASSERT(irq > 0 && irq < (int) buflen(scheme_irq_wrappers), s_irq, 0);

	if (irq < 8)
		outb(0x60+irq, 0x20); /* Signal End of Interrupt */
	else if (irq < 16) {
		outb(0x60+(irq-8), 0xa0);
	}
	return UNSPEC;
})
