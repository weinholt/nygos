/* -*- coding: utf-8 -*-
 * The Unbelievable Operating System from Hell
 * Copyright © 2005-2007 Göran Weinholt <goran@weinholt.se>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <nygos.h>
#include <multiboot.h>
#include <malloc.h>
#include <stdio.h>
#include <multitasking.h>
#include <interrupt.h>
#include <timer.h>
#include <string.h>
#include <number.h>
#include <runtime.h>

void panic(char *msg)
{
	register long *rsp asm("rsp");
	long *stack = rsp;
	disable_interrupts();
	puts("It has all gone wrong!\n");
	puts("Kernel panic: ");
	puts(msg);
	puts("\nStack trace:");
 	for (int i = 0; i < 32; i++) {
		puts(" #x");
		puti(stack[i], 16);
	}

	for (;;) asm("hlt"::);
}

extern multiboot_info_t *mbi;

defun (sys_multiboot_info, "multiboot-info-location", 0, 0, (),
	   "Provide the location of the multiboot information structure.", {
	return number((uint64) mbi);
})

extern void startup_thread(void);

void cmain(void)
{
	cls();

	/* Initialize some stuff... */
	interrupt_init();
	memory_init(mbi->mem_upper);
	timer_init();

	multitasking_new(startup_thread, 0, BOOL_F);
	multitasking_forever();
}
