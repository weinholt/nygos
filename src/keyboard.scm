;; -*- coding: utf-8 -*-
;; The Unbelievable Operating System from Hell
;; Copyright © 2005-2007 Göran Weinholt <goran@weinholt.se>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; Routines for i8042 compatible keyboard controllers

;; TODO: the second PS/2 port can have either a mouse or another keyboard.

;; FIXME: SRFI-60
(define (logtest n1 n2)
  (not (zero? (logand n1 n2))))

(define keyboard (make-bytevector/io 5 #x60))

(define (kbd-data-ref regs)
  "Reads one byte from the i8042's output buffer."
  (bytevector-u8-ref regs 0))
(define (kbd-data-set! regs v)
  "Sends one byte of data to the keyboard."
  (bytevector-u8-set! regs 0 v))
(define (kbd-status-ref regs)
  "Reads the i8042 status byte."
  (bytevector-u8-ref regs 4))
(define (kbd-cmd-set! regs v)
  "Sends a one-byte command to the i8042."
  (bytevector-u8-set! regs 4 v))

;; PS/2 compatible status bits
(define i8042-status-parity-error       #b10000000)
(define i8042-status-timeout            #b01000000)
(define i8042-status-mouse-buffer-full  #b00100000)
(define i8042-status-inhibit            #b00010000)
(define i8042-status-address-line-a2    #b00001000)
(define i8042-status-system-flag        #b00000100)
(define i8042-status-input-buffer-full  #b00000010)
(define i8042-status-output-buffer-full #b00000001)

;; Command register
(define i8042-cmd-translate-scancodes #b1000000)
(define i8042-cmd-disable-aux         #b0100000)
(define i8042-cmd-disable-keyboard    #b0010000)
(define i8042-cmd-sys                 #b0000100)
(define i8042-cmd-irq12               #b0000010)
(define i8042-cmd-irq1                #b0000001)

(define i8042-command-read-command-byte  #x20)
(define i8042-command-write-command-byte #x60)

(define (reboot)
  (print "rebooting...")
  (kbd-cmd-set! keyboard #xfe) ;pulse output bit 0
  (kbd-cmd-set! keyboard #xfe)
  ;; FIXME: panic or try something harder, like a triple-fault
  (print "reboot failed!")
  (reboot))

(define (keyboard-command command)
  (cond ((logtest i8042-status-input-buffer-full
                  (kbd-status-ref keyboard))
         (thread-msleep! 1)))
  (kbd-data-set! keyboard command)
  (let lp ()
    (cond ((not (logtest i8042-status-output-buffer-full
                         (kbd-status-ref keyboard)))
           (thread-msleep! 1)
           (lp))))
  (unless (= (kbd-data-ref keyboard) #xfa)
    (print "PS/2 keyboard slow to respond")))

(define (i8042-command command . parameters)
  (kbd-cmd-set! keyboard i8042-command-write-command-byte)
  (for-each (cut kbd-data-set! keyboard <>) parameters)
  (kbd-data-ref keyboard))

(define (keyboard-irq)
  ;; FIXME: There needs to be an object (of type interactive-driver)
  ;; that can receive these key presses, and this IRQ handler needs to
  ;; be a driver object that is bound to that object.
  (let ((consumer (make-scancode-consumer act-on-keypress))
        (led-bits 0))
    (define (set-leds)
      (i8042-command i8042-command-write-command-byte
                     (logior i8042-cmd-translate-scancodes
                             i8042-cmd-disable-aux
                             i8042-cmd-sys))
      (keyboard-command #xed)
      (keyboard-command led-bits)
      (i8042-command i8042-command-write-command-byte
                     (logior i8042-cmd-translate-scancodes
                             i8042-cmd-disable-aux
                             i8042-cmd-sys
                             i8042-cmd-irq1)))

    (i8042-command i8042-command-write-command-byte
                     (logior i8042-cmd-translate-scancodes
                             i8042-cmd-disable-aux
                             i8042-cmd-sys))
    
    (keyboard-command #xed)             ;clear leds
    (keyboard-command #x00)
    (keyboard-command #xf4)             ;enable
    (i8042-command i8042-command-write-command-byte
                     (logior i8042-cmd-translate-scancodes
                             i8042-cmd-disable-aux
                             i8042-cmd-sys
                             i8042-cmd-irq1))

    (set-irq-handler! 1 (self))
    (irq-acknowledge! 1)
    (forever
      (case (?)
        ((1)
         (when (logtest i8042-status-output-buffer-full
                        (kbd-status-ref keyboard))
           (let ((scancode (kbd-data-ref keyboard)))
             (consumer scancode)))
         (irq-acknowledge! 1))
        ((eq? x 'caps-lock)
         (set! led-bits (logxor led-bits #b100))
         (set-leds))
        ((eq? x 'num-lock)
         (set! led-bits (logxor led-bits #b010))
         (set-leds))
        ((eq? x 'scroll-lock)
         (set! led-bits (logxor led-bits #b001))
         (set-leds))))))


;; FIXME: Some new abstractions need to be created. A keyboard
;; abstraction for example, which keeps track of a keyboard's whole
;; nature of being or something. It needs to keep track of the LEDs
;; and the keymap, also it needs to have a scancode consumer. All
;; keyboards should have their own LED states, they should not be
;; shared.

(define (make-scancode-consumer keypress-consumer)
  "This function creates a closure of one argument that consumes
scancodes produced by a PC keyboard. The keypress-consumer argument is
a function that takes characters and symbols representing keys as its
only argument."
  ;; FIXME: this function is not completely correct, it is at least
  ;; missing support for the #xE1 scancode.
  (let ((shift-down #f)
        (ctrl-down #f)
        (meta-down #f)
        (extended 0))
    (lambda (scancode)
      (let ((current-map (if shift-down keymap-shift keymap)))
        (if (= scancode #xE0)
            (set! extended #b10000000)
            (let ((key (vector-ref current-map
                                   (logior extended
                                           (logand #b01111111 scancode))))
                  (pressed (zero? (logand #b10000000 scancode))))
              (case key
                ((shift) (set! shift-down pressed))
                ((ctrl) (set! ctrl-down pressed))
                ((meta) (set! meta-down pressed))
                ((num-lock)
                 (! keyboard-process 'num-lock))
                ((scroll-lock)
                 (! keyboard-process 'scroll-lock))
                ((#f)
                 (print "Unknown scancode: #x"
                        (number->string (if (zero? extended)
                                            scancode
                                            (logior #xE000 scancode))
                                        16)))
                (else
                 (when pressed
                   (if meta-down (keypress-consumer #\esc))
                   (if (and ctrl-down (char? key) (<= #x40 (char->integer key) #x7f))
                       (keypress-consumer
                        (integer->char (logand (char->integer key) 31)))
                       (keypress-consumer key)))))
              (set! extended 0)))))))

;; Generic interactive driver code:

(define global-map (make-hash-table))
(define ctl-x-map (make-hash-table))
(define help-map (make-hash-table))
(define esc-map (make-hash-table))
(define mode-specific-map (make-hash-table))
(define keymap? hash-table?)

(define (define-key keymap key binding)
  (unless (vector? key) (error 'define-key "key should be a vector" key))
  (unless (keymap? keymap) (error 'define-key "keymap should be a keymap" keymap))
  (let lp ((keys (vector->list key))
           (keymap keymap))
    (cond ((null? keys)
           binding)
          ((null? (cdr keys))
           (hash-table-set! keymap (car keys) binding))
          ((hash-table-ref/default keymap (car keys) #f) =>
           (lambda (oldbinding)
             (if (keymap? oldbinding)
                 (lp (cdr keys) oldbinding)
                 (error 'define-key "invalid prefix character" (car keys))))))))

(define (global-set-key key command)
  (define-key global-map key command))

(define current-keymap global-map)

(define kbd-buf (make-queue))           ; FIXME: see FIXME below
(define (kbd-enqueue c)
  (enqueue! kbd-buf c))

(define (act-on-keypress key)
  (cond ((hash-table-ref/default current-keymap key #f) =>
         (lambda (x)
           (cond ((hash-table? x)
                  (set! current-keymap x))
                 ((symbol? x)
                  (set! current-keymap global-map)
                  (cond ((and (char? key) (eq? x 'self-insert-command))
                         (display key)
                         (kbd-enqueue key))
                        (else
                         (eval `(,x))))))))
        (else
         (set! current-keymap global-map)
         (beep))))

(define (delete-backward-char)
  (cond ((queue-empty? kbd-buf)
         (beep))
        (else
         ;; FIXME: interact with a real editor buffer
         (dequeue! kbd-buf)
         (display "\b \b"))))

(global-set-key '#(#\x18) ctl-x-map)    ; C-x
(global-set-key '#(#\x08) help-map)     ; C-h
(global-set-key '#(#\x1B) esc-map)      ; ESC
(global-set-key '#(#\x03) mode-specific-map) ; C-c

(do ((i 32 (+ i 1)))
    ((= i 127))
  (global-set-key (vector (integer->char i)) 'self-insert-command))
(do ((i 160 (+ i 1)))
    ((= i 255))
  (global-set-key (vector (integer->char i)) 'self-insert-command))
(global-set-key '#(#\x7f) 'delete-backward-char)

(global-set-key '#(#\x08 #\x08) 'help-for-help)
(global-set-key '#(#\x18 #\x03) 'reboot)

(global-set-key '#(#\esc #\x) 'execute-extended-command)

;;;

(define keyboard-process (spawn keyboard-irq))
