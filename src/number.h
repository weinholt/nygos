/* -*- coding: utf-8 -*-
 * The Unbelievable Operating System from Hell
 * Copyright © 2005-2007 Göran Weinholt <goran@weinholt.se>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _NUMBER_H
#define _NUMBER_H

SCM number(int64 i);

static inline SCM fixnum(int64 i)
{			  /* Use this if `i' can never be larger than a fixnum. */
	return (SCM) (i << 2);
}

static inline int64 INUM_TO_INT(SCM n)
{
	return ((int64) n) >> 2;
}

struct biggernum {
	int64 value;
};

#include <runtime.h>

/* 62 bits for the whole int, and one bit is used for sign. */
#define INT_MAX ((1 << 61) - 1)
#define INT_MIN (-INT_MAX - 1)

static inline int64 _to_int64(SCM num, const char *func)
{
	if (LIKELY(IS_INUM(num)))
		return INUM_TO_INT(num);
	else if (IS_NUMBER(num)) {
		struct biggernum *n = get_addrt(num, NUMBER);
		return n->value;
	}
	return signal_error(func, "Expected a 64-bit number: ", cons(num, EOL));
}
#define to_int64(n)  _to_int64((n), __FUNCTION__)

static inline int64 get_fixnum(SCM num)
{
	if (LIKELY(IS_INUM(num)))
		return INUM_TO_INT(num);
	else
		return assertion_violation("Expected a fixnum");
}

#endif
