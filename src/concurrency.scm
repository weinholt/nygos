;; -*- coding: utf-8 -*-
;; The Unbelievable Operating System from Hell
;; Copyright © 2007 Göran Weinholt <goran@weinholt.se>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define (spawn func . args)
  (sys!spawn (lambda ()
               (apply func args)
               (halt!))
             'unnamed))

(define (halt!)
  (thread-terminate! (self)))

(define (thread-sleep! x)
  (thread-msleep! (* x 1000)))

;; @inproceedings{ valois94implementing,
;;     author = "J. D. Valois",
;;     title = "Implementing Lock-Free Queues",
;;     booktitle = "Proceedings of the Seventh International Conference on Parallel and Distributed Computing Systems",
;;     address = "Las Vegas, NV",
;;     pages = "64--69",
;;     year = "1994",
;;     url = "citeseer.ist.psu.edu/valois94implementing.html" }

(define (make-atomic-queue)
  (let ((empty (list #f)))
    (cons empty empty)))

(define (atomic-enqueue! queue x)
  (let ((q (cons x '())))
    (let lp ()
      (let ((p (cdr queue)))
        (cond ((cas-cdr! p '() q)
               (cas-cdr! queue p q)
               (unspecified))
              (else
               (cas-cdr! queue p (cdr p))
               (lp)))))))

(define (atomic-dequeue! queue)
  (let lp ()
    (let ((p (car queue)))
      (if (null? (cdr p))
          (error 'atomic-dequeue! "queue empty"))
      (cond ((cas-car! queue p (cdr p))
             (let ((ret (cadr p)))
               ;; This is not in the cited paper: there is a space
               ;; leak in the given algorithm. The code below would
               ;; fix this problem, but instead introduces a bug when
               ;; the same message is sent twice.
               ;; (if (null? (cdar queue))
               ;;     (cas-car! (car queue) ret #f))
               ret))
            (else
             (lp))))))

(define (atomic-queue-empty? queue)
  (null? (cdar queue)))

(define atomic-queue-head car)
(define atomic-queue-tail cdr)

(define (! who message)
  ;; XXX: this should notify the scheduler somehow so it can make the
  ;; thread runnable if it's waiting
  (atomic-enqueue! (sys!thread-mailbox who) message))

(define (?-retry queue timeout default)
  (cond ((not (atomic-queue-empty? queue))
         (atomic-dequeue! queue))
        ((sys!thread-irqslot) =>
         (lambda (irq)
           (sys!thread-irqslot-clear!)
           irq))
        (else
         (sys!thread-cursor-set! (atomic-queue-head queue))
         (sys!thread-wait!)
         (if (and timeout (atomic-queue-empty? queue)
                  (not (sys!thread-irqslot)))
             (default)
             (?-retry queue timeout default)))))

(define (? . rest)
  ;; More efficient version of (?? (lambda (x) #t) timeout default)
  ;; Lifted the loop out of there because loops are not yet optimized
  ;; by the compiler.
  (let-optionals* rest ((timeout #f) (default #f))
    (sys!thread-timeout-set! timeout)
    (?-retry (sys!thread-mailbox (self))
             timeout (if (= (length rest) 1)
                         (lambda () (error '? "timeout!"))
                         (lambda () default)))))

(define (?? match? . rest)
  ;; This code is tied to the implementation of the atomic queue code
  ;; and, of course, the scheduler.
  (let-optionals* rest ((timeout #f) (default #f))
    (sys!thread-timeout-set! timeout)
    (let ((queue (sys!thread-mailbox (self))))
      (sys!thread-cursor-set! (atomic-queue-head queue))
      (let retry ()
        (cond ((and (sys!thread-irqslot)
                    (match? (sys!thread-irqslot)))
               (let ((irq (sys!thread-irqslot)))
                 (sys!thread-irqslot-clear!)
                 irq))
              ((eq? (sys!thread-cursor)
                    (atomic-queue-tail queue))
               (sys!thread-wait!)
               (if (and timeout (eq? (sys!thread-cursor)
                                     (atomic-queue-tail queue))
                        (not (sys!thread-irqslot)))
                   (if (= (length rest) 1)
                       (error '? "timeout!")
                       default)
                   (retry)))
              ((match? (cadr (sys!thread-cursor)))
               (let* ((cursor (sys!thread-cursor))
                      (ret (cadr cursor)))
                 (cond ((eq? cursor (atomic-queue-head queue))
                        ;; We're at the head of the queue
                        (atomic-dequeue! queue))
                       ((eq? (cdr cursor) (atomic-queue-tail queue))
                        ;; We're at the tail, tricky! I'm not
                        ;; even sure this is going to work on a
                        ;; multiprocessor system.
                        (let ((old-tail (atomic-queue-tail queue)))
                          (set-cdr! cursor (cddr cursor))
                          (cas-cdr! queue old-tail cursor)))
                       (else
                        ;; We're in the middle, ok to remove it
                        ;; the old-fashioned way.
                        (set-cdr! cursor (cddr cursor))))
                 ret))
              (else
               (sys!thread-cursor-set! (cdr (sys!thread-cursor)))
               (retry)))))))

(define-record-type tag (make-tag) tag?)

(define (!? who message)
  "Send `message' to `who' with a tag appended, and wait for a reply."
  (let ((tag (make-tag)))
    (! who (list (self) tag message))
    (cadr (?? (lambda (m)
                (and (pair? m)
                     (eq? (car m) tag)
                     (pair? (cdr m))
                     (null? (cddr m))))))))

(define (spawn-supervised thunk)
  ;; This is most likely exactly the wrong way to do supervision.
  ;; TODO: check how erlang does it.
  (spawn (lambda ()
           (let ((thread (spawn thunk))
                 (tag (make-tag)))
             (forever
               (let ((msg (? 10000 tag)))
                 (cond
                  ((eq? msg tag)
                   (let* ((tag (make-tag))
                          (reply (list tag 'pong)))
                     (! thread (list (self) tag 'ping))
                     (let ((m (?? (lambda (m) (equal? m reply)) 5000 tag)))
                       (when (eq? m tag)
                         (print "Restarting supervised thread")
                         (thread-terminate! thread)
                         (set! thread (spawn thunk))))))
                  (else
                   ;; act as a proxy
                   (! thread msg)))))))))
