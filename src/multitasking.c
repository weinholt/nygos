/* -*- coding: utf-8 -*-
 * The Unbelievable Operating System from Hell
 * Copyright © 2005-2007 Göran Weinholt <goran@weinholt.se>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <nygos.h>
#include <malloc.h>
#include <multitasking.h>
#include <string.h>
#include <runtime.h>
#include <interrupt.h>
#include <number.h>
#include <timer.h>

static const int DEBUG = 0;
static const uint32 canary = 0xDEFACED;

volatile static SCM threads = EOL;		/* all threads */
volatile static int runnable = 0;		/* runnable threads */
volatile uint64 saved_rsp = 0;			/* %rsp of main() thread */
void multitasking_switch(void);

enum thread_state {
	THREAD_RUNNABLE, THREAD_BLOCKED, THREAD_TERMINATED
};

#define STACK_SIZE (1024*16)

struct thread {
	uint64 *sp;					/* saved stack pointer */
	char *stack_top;			/* points to the end of stack_bottom
								 * (for runtime_s.S) */
	uint64 wakeup;				/* no. of jiffies when to wake up */
	SCM mailbox;				/* atomic queue (see concurrency.scm) */
	SCM cursor;					/* mailbox cursor */
	SCM messagewait;			/* waiting for the tail of mailbox to
								 * change from this value. */
	SCM irqslot;				/* a single message from the IRQ stuff */
	
	SCM name;
	enum thread_state state;
	uint64 canary;
	char stack_bottom[STACK_SIZE];
	uint64 canary_underflow;
};

extern void multitasking_run(struct thread *task);

/* This one is called from interrupt handlers */
void multitasking_send(SCM s_thread, SCM message)
{
	struct thread *thread = get_addrt(s_thread, THREAD);
	thread->irqslot = message;
}

SCM multitasking_new(void *rip, uint64 rbx, SCM name)
{
	SCM s_thread = new(sizeof(struct thread), THREAD);
	struct thread *thread = get_addrt(s_thread, THREAD);
	SCM tmp = cons(BOOL_F, EOL);

	thread->name = name;
	thread->wakeup = 0;
	thread->mailbox = cons(tmp, tmp);
	thread->cursor = tmp;
	thread->messagewait = tmp;
	thread->irqslot = BOOL_F;
	thread->state = THREAD_RUNNABLE;
	thread->canary = canary;
	thread->canary_underflow = 0x0a141e28;
	/* Stack grows downwards */
	thread->stack_top = &thread->stack_bottom[STACK_SIZE - sizeof(SCM)];
	thread->sp = (uint64 *) thread->stack_top;

	*thread->sp-- = (uint64) rip;	/* return address from a call */
	/* See multitask.S */
	thread->sp--;				/* rdi */
	*thread->sp-- = rbx;		/* rbx */
	thread->sp--;				/* rbp */
	thread->sp--;				/* r12 */
	thread->sp--;				/* r13 */
	thread->sp--;				/* r14 */
	*thread->sp = s_thread;		/* r15 */

	disable_interrupts();
 	threads = cons(s_thread, threads);
	runnable++;
	enable_interrupts();

	return s_thread;
}

void multitasking_forever(void)
{
	SCM next = EOL;
	int empty = 1;
	int empty_passes = 0;

	while (1) {
		if (next == EOL) {
			if (threads == EOL)
				panic("Last thread exited");
			if (!runnable) {
				if (empty == 1)
					empty_passes++;
				empty = 1;
				if (empty_passes == 1) {
					empty_passes = 0;
					asm("hlt");
				}
			}
			next = threads;
		}
		current_thread = CAR(next);
		struct thread *thread = get_addrt(current_thread, THREAD);
		if (thread->canary != canary) {
			/* XXX: use the MMU instead */
			panic("stack corrupt");
		}
		switch (thread->state) {
		case THREAD_BLOCKED:
			if (thread->messagewait == BOOL_T &&
				(thread->irqslot != BOOL_F ||
				 thread->cursor != CDR(thread->mailbox))) {
				runnable++;
				thread->state = THREAD_RUNNABLE;
				thread->messagewait = BOOL_F;
			} else if (thread->wakeup && jiffies >= thread->wakeup) {
				runnable++;
				thread->state = THREAD_RUNNABLE;
				thread->wakeup = 0;
				thread->messagewait = BOOL_F;
				/* pass through */
			} else
				break;
		case THREAD_RUNNABLE:
			empty = 0;
			multitasking_run(thread);
			break;
		default:;
			/* Remove s_thread from `threads'. */
			SCM *prev = (SCM *) &threads, iter;

			for (iter = threads; iter != EOL; iter = CDR(iter))
				if (CAR(iter) == current_thread)
					*prev = CDR(iter);
				else
					prev = &CDR(iter);
		}
		current_thread = BOOL_F;
		next = CDR(next);
	}
}

/**************************************************************************/

SCM thread_name(SCM s_thread)
{
	TYPE_ASSERT(IS_THREAD(s_thread), s_thread, 0);
	struct thread *thread = get_addrt(s_thread, THREAD);
	return thread->name;
}

SCM thread_terminate_ex(SCM s_thread)
{
	TYPE_ASSERT(IS_THREAD(s_thread), s_thread, 0);
	struct thread *thread = get_addrt(s_thread, THREAD);

	if (thread->state == THREAD_TERMINATED) {
		return BOOL_F;
	}
	disable_interrupts();
	if (thread->state == THREAD_RUNNABLE)
		runnable--;
	thread->state = THREAD_TERMINATED;
	enable_interrupts();

	if (s_thread == current_thread) {
		multitasking_switch();
		panic("A terminated thread got rescheduled");
	}
	
	return s_thread;
}

defun (_self, "self", 0, 0, (),
	   "Return the current thread.", {
	return current_thread;
})

defun (sys__spawn, "sys!spawn", 2, 0, (SCM s_thunk, SCM name),
	   "System procedure used to implement spawn.", {
	TYPE_ASSERT(IS_PROC(s_thunk), s_thunk, 0);
	struct closure *thunk = get_addrt(s_thunk, PROC);

	return multitasking_new(thunk->code, s_thunk, name);
})

defun (_thread_name, "thread-name", 1, 0, (SCM thread),
	   "Returns the `name' field of a thread.", {
	return thread_name(thread);
})

defun (thread_yield_ex, "thread-yield!", 0, 0, (),
	   "Yields the current time slice.", {
	multitasking_switch();
	return UNSPEC;
})

defun (thread_msleep_ex, "thread-msleep!", 1, 0, (SCM s_timeout),
	   "Blocks the current thread for s_timeout milliseconds.", {
	TYPE_ASSERT(IS_INUM(s_timeout), s_timeout, 0);

	struct thread *thread = get_addrt(current_thread, THREAD);
	thread->state = THREAD_BLOCKED;
	thread->wakeup = jiffies + (INUM_TO_INT(s_timeout) * HZ) / 1000;
	runnable--;

	multitasking_switch();
	return UNSPEC;
})

defun (thread_usleep_ex, "thread-usleep!", 1, 0, (SCM s_timeout),
	   "Blocks the current thread for at least s_timeout microseconds.", {
	TYPE_ASSERT(IS_INUM(s_timeout), s_timeout, 0);

	struct thread *thread = get_addrt(current_thread, THREAD);
	thread->state = THREAD_BLOCKED;
	thread->wakeup = jiffies + (INUM_TO_INT(s_timeout) * HZ) / 1000000;
	runnable--;

	multitasking_switch();
	return UNSPEC;
})

defun (_thread_terminate_ex, "thread-terminate!", 1, 0, (SCM thread),
	   "Terminate the given thread.", {
	return thread_terminate_ex(thread);
})

/* Message passing */

defun (sys_thread_mailbox, "sys!thread-mailbox", 1, 0, (SCM s_thread),
	   "Returns the mailbox of the given thread.", {
	TYPE_ASSERT(IS_THREAD(s_thread), s_thread, 0);
	struct thread *thread = get_addrt(s_thread, THREAD);
	return thread->mailbox;
})

defun (sys_thread_cursor, "sys!thread-cursor", 0, 0, (),
	   "Returns the mailbox cursor of the current thread.", {
	struct thread *thread = get_addrt(current_thread, THREAD);
	return thread->cursor;
})

defun (sys_thread_cursor_set, "sys!thread-cursor-set!", 1, 0, (SCM new_cursor),
	   "Sets the mailbox cursor of the current thread.", {
	struct thread *thread = get_addrt(current_thread, THREAD);
	thread->cursor = new_cursor;
	return UNSPEC;
})

defun (sys_thread_irqslot, "sys!thread-irqslot", 0, 0, (),
	   "Returns the IRQ slot of the current thread.", {
	struct thread *thread = get_addrt(current_thread, THREAD);
	return thread->irqslot;
})

defun (sys_thread_irqslot_clear, "sys!thread-irqslot-clear!", 0, 0, (),
	   "Returns the IRQ slot of the current thread.", {
	struct thread *thread = get_addrt(current_thread, THREAD);
	thread->irqslot = BOOL_F;
	return UNSPEC;
})

defun (sys_thread_timeout_set, "sys!thread-timeout-set!", 1, 0, (SCM timeout),
	   "Set the process wakeup time to `timeout' ms in the future.", {
	struct thread *thread = get_addrt(current_thread, THREAD);
	if (IS_INUM(timeout)) {
		thread->wakeup = jiffies + (INUM_TO_INT(timeout) * HZ) / 1000;
	} else {
		thread->wakeup = 0;
	}
	return UNSPEC;
})

defun (sys_thread_wait_ex, "sys!thread-wait!", 0, 0, (),
	   "Waits for either a new message or timeout.", {
	struct thread *thread = get_addrt(current_thread, THREAD);

	thread->messagewait = BOOL_T;
	thread->state = THREAD_BLOCKED;
	runnable--;

	multitasking_switch();
	return UNSPEC;
})
