/* -*- coding: utf-8 -*-
 * The Unbelievable Operating System from Hell
 * Copyright © 2005-2007 Göran Weinholt <goran@weinholt.se>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _MALLOC_H
#define _MALLOC_H

static inline void outb(uint8 value, uint16 port)
{
	/* dN = either a byte constant or the %dx register. */
	asm volatile ("outb %0, %1" : : "a"(value), "dN"(port));
}

static inline void outw(uint16 value, uint16 port)
{
	asm volatile ("outw %0, %1" : : "a"(value), "dN"(port));
}

static inline void outl(uint32 value, uint16 port)
{
	asm volatile ("outl %0, %1" : : "a"(value), "dN"(port));
}

static inline uint8 inb(uint16 port)
{
	uint8 value;
	asm volatile ("inb %1, %0" : "=a"(value) : "dN"(port));
	return value;
}

static inline uint16 inw(uint16 port)
{
	uint16 value;
	asm volatile ("inw %1, %0" : "=a"(value) : "dN"(port));
	return value;
}

static inline uint32 inl(uint16 port)
{
	uint32 value;
	asm volatile ("inl %1, %0" : "=a"(value) : "dN"(port));
	return value;
}

void memory_init(unsigned long mem_upper);
void memory_init2(void);

typedef uint64 SCM;

/* The master copy of these constants is in compiler.scm. */
#define FIXNUM_MASK 0x3
#define FIXNUM_TAG 0x0
#define IS_FIXNUM(n) (((n) & FIXNUM_MASK) == FIXNUM_TAG)

#define CHAR_MASK 0xff
#define CHAR_TAG 0x0f
#define IS_CHAR(n) (((n) & CHAR_MASK) == CHAR_TAG)

#define ICHAR_TO_WCHAR(n) (((uint32) (n)) >> 8)
#define ucs4_to_character(c) (((c) << 8) | CHAR_TAG)

#define BOOL_MASK 0xff
#define BOOL_TAG 0x1f
#define BOOL_F ((0 << 8) | BOOL_TAG)
#define BOOL_T ((1 << 8) | BOOL_TAG)
#define TO_BOOL(n) (((!!(n)) << 8) | BOOL_TAG)
#define IS_BOOL(n) (((n) & BOOL_MASK) == BOOL_TAG)

#define EOL 0x2f
#define UNSPEC 0x4f
#define EOF 0x6f

#define HEAP_MASK 0xf
#define PAIR_MASK 0x7
#define BYTEVECTOR_MASK 0x7
#define PAIR_TAG 0x1
#define VECTOR_TAG 0x2
#define BYTEVECTOR_TAG 0x3
#define SYMBOL_TAG 0x5
#define PROC_TAG 0x6
#define NUMBER_TAG 0x7
/* #define IMPAIR_TAG 0x9 */
#define RECORD_TAG 0xa
/* #define IMBYTEVECTOR_TAG 0xb */
#define THREAD_TAG 0xd
#define STRING_TAG 0xe

#define IS_INUM(n) IS_FIXNUM(n)
#define IS_ICHAR(n) IS_CHAR(n)

#define IS_PAIR(v)       (((v) & PAIR_MASK) == PAIR_TAG)
#define IS_SYMBOL(v)     (((v) & HEAP_MASK) == SYMBOL_TAG)
#define IS_STRING(v)     (((v) & HEAP_MASK) == STRING_TAG)
#define IS_NUMBER(v)     (((v) & HEAP_MASK) == NUMBER_TAG)
#define IS_VECTOR(v)     (((v) & HEAP_MASK) == VECTOR_TAG)
#define IS_PROC(v)       (((v) & HEAP_MASK) == PROC_TAG)
#define IS_RECORD(v)     (((v) & HEAP_MASK) == RECORD_TAG)
#define IS_BYTEVECTOR(v) (((v) & HEAP_MASK) == BYTEVECTOR_TAG)
#define IS_THREAD(v)     (((v) & HEAP_MASK) == THREAD_TAG)
#define IS_FALSE(v)      ((v) == BOOL_F)
#define IS_TRUE(v)       ((v) == BOOL_T)

enum type { STRING = STRING_TAG, NUMBER = NUMBER_TAG, SYMBOL = SYMBOL_TAG,
			PAIR = PAIR_TAG, PROC = PROC_TAG, VECTOR = VECTOR_TAG,
			RECORD = RECORD_TAG, THREAD = THREAD_TAG, BYTEVECTOR = BYTEVECTOR_TAG
};

SCM _new(uint64 size, enum type type);
#define ALIGNMENT 16
#define new(size,type) _new(ALIGN((size),ALIGNMENT),(type))
void immortalize(SCM index, int mortal);

static inline void __attribute__((always_inline)) *get_addr(SCM ptr)
{
	return (void *) (ptr & ~HEAP_MASK);
}

static inline void __attribute__((always_inline)) *get_addrt(SCM ptr, enum type type) 
{
	return (void *) (ptr - type);
}

static inline SCM __attribute__((always_inline)) scmref(SCM ptr, enum type t, uint32 i)
{
	SCM *p = get_addrt(ptr, t) + i * sizeof(SCM);
	return *p;
}

static inline void __attribute__((always_inline)) scmset(SCM ptr, enum type t, uint32 i, SCM v)
{
	SCM *p = get_addrt(ptr, t) + i * sizeof(SCM);
	*p = v;
}

#include <pair.h>

#endif
