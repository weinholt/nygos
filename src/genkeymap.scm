;; -*- coding: utf-8 -*-
;; The Unbelievable Operating System from Hell
;; Copyright © 2007 Göran Weinholt <goran@weinholt.se>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; Script that generates keymaps for keyboard.scm

(use-modules (ice-9 syncase))
(load "syntax.scm")

;; The character syntax of guile differers from the R6RS syntax, so
;; unicode code points are given as numbers in some places here.
(define swedish-keymap
  '((1 #\esc)
    (41 #xa7 #xbd)                      ; § ½
    (2 #\1 #\!) (3 #\2 #\") (4 #\3 #\#) (5 #\4) (6 #\5 #\%)
    (7 #\6 #\&) (8 #\7 #\/) (9 #\8 #\() (10 #\9 #\)) (11 #\0 #\=)
    (12 #\+ #\?) (14 #x7f)
    (15 #x09)
    (16 #\q) (17 #\w) (18 #\e) (19 #\r) (20 #\t) (21 #\y)
    (22 #\u) (23 #\i) (24 #\o) (25 #\p)

    (28 #x0d)                           ; #\return

    (30 #\a) (31 #\s) (32 #\d) (33 #\f) (34 #\g) (35 #\h)
    (36 #\j) (37 #\k) (38 #\l) (39 #\p) (40 #\p)

    (43 #\' #\*)

    (44 #\z) (45 #\x) (46 #\c) (47 #\v) (48 #\b) (49 #\n)
    (50 #\m) (51 #\, #\;) (52 #\. #\:) (53 #\- #\_)

    (57 #\space)

    (86 #\< #\>)

    (29 ctrl)                           ; left ctrl
    (42 shift)
    (54 shift)
    (56 meta)

    (#xE01D ctrl)                       ; right ctrl
    (#xE038 altgr)

    (58 ctrl)                           ; (58 caps-lock)
    (69 num-lock)
    (70 scroll-lock)
    (59 <f1>) (60 <f2>) (61 <f3>) (62 <f4>)

    (#xE052 <insert>)
    (#xE047 <home>)
    (#xE049 <prior>)
    (#xE053 <delete>)
    (#xE04F <end>)
    (#xE051 <next>)

    (#xE04B <left>)
    (#xE04D <right>)
    (#xE048 <up>)
    (#xE050 <down>)
    
    ;; Swedish characters
    (26 #xe5 #xc5)
    (40 #xe4 #xc4)
    (39 #xf6 #xd6)))

(define keymap (make-vector 255 #f))
(define keymap-shift (make-vector 255 #f))

;; Scancodes beginning with #xE0 are remapped to the otherwise unused
;; half of the keymap vector.
(define (remap x)
  (if (= #xE0 (ash x -8))
      (logior #b10000000 (logand x #xff))
      x))

(for-each (lambda (mapping)
            (cond ((and (= (length mapping) 2)
                        (char? (cadr mapping))
                        (char-lower-case? (cadr mapping)))
                   (vector-set! keymap-shift (remap (car mapping))
                                (char-upcase (cadr mapping))))
                  ((= (length mapping) 2)
                   (vector-set! keymap-shift (remap (car mapping))
                                (cadr mapping)))
                  ((> (length mapping) 2)
                   (vector-set! keymap-shift (remap (car mapping))
                                (caddr mapping))))
            (vector-set! keymap (remap (car mapping)) (cadr mapping)))
          swedish-keymap)

(define (print-char c)
  (display "#\\x")
  (if (< c #xf)
      (display 0))
  (display (number->string c 16)))

(define (print-vector v)
  (display "#(")
  (let lp ((i 0) (len (vector-length v)))
    (unless (>= i len)
      (let ((item (vector-ref v i)))
        (if (number? item)
            (print-char item)
            (write item)))
      (if (> (- len 1) i)
          (write-char #\space))
      (lp (+ i 1) len)))
  (display ")"))

(display "(define keymap '")
(print-vector keymap)
(display ")")
(newline)
(display "(define keymap-shift '")
(print-vector keymap-shift)
(display ")")
(newline)
