;; -*- coding: utf-8 -*-
;; The Unbelievable Operating System from Hell
;; Copyright © 2007 Göran Weinholt <goran@weinholt.se>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; (i/o ports) Synchronous I/O ports from R^{5.92}SL

;;; 7.2.4. Transcoders
(define (latin-1-codec)
  (error 'latin-1-codec 'not-implemented))

(define (utf-8-codec)
  (error 'utf-8-codec 'not-implemented))

(define (utf-16-codec)
  (error 'utf-16-codec 'not-implemented))

(define (utf-32-codec)
  (error 'utf-32-codec 'not-implemented))

(define-syntax eol-style
  (syntax-rules (lf cr crlf ls)
    ((_ lf) 'lf)
    ((_ cr) 'cr)
    ((_ crlf) 'crlf)
    ((_ ls) 'ls)))

(define (native-eol-style)
  (eol-style lf))

;; (define-condition-type &i/o-decoding &i/o-port
;;   i/o-decoding-error?
;;   (transcoder i/o-decoding-error-transcoder))

;; (define-condition-type &i/o-encoding &i/o-port
;;   i/o-encoding-error?
;;   (char i/o-encoding-error-char)
;;   (transcoder i/o-encoding-error-transcoder))

(define-syntax error-handling-mode
  (syntax-rules (ignore raise replace)
    ((_ ignore) 'ignore)
    ((_ raise) 'raise)
    ((_ replace) 'replace)))

(define-syntax buffer-mode
  (syntax-rules (none line block)
    ((_ none) 'none)
    ((_ line) 'line)
    ((_ block) 'block)))

(define (buffer-mode? mode)
  (if (memq mode '(none line block)) #t #f))

(define (make-transcoder codec . rest)
  (let-optionals* rest ((eol-style (native-eol-style))
                        (handling-mode 'raise))
    (really-make-transcoder codec eol-style handling-mode)))

(define-record-type transcoder (really-make-transcoder codec eol-style error-handling-mode)
  transcoder?
  (codec transcoder-codec)
  (eol-style transcoder-eol-style)
  (error-handling-mode transcoder-error-handling-mode))

(define binary-transcoder
  (let ((transcoder (really-make-transcoder #f #f #f)))
    (lambda ()
      transcoder)))

(define (bytevector->string bytevector transcoder)
  (error 'bytevector->string 'not-implemented))

(define (string->bytevector string transcoder)
  (error 'string->bytevector 'not-implemented))

;; (eof-object) is primitive

(define (eof-object? obj)
  (eq? obj (eof-object)))

;;; 7.2.6. Input and output ports
(define-record-type port (make-port thread type transcoder buffer-mode
                                    input? output?
                                    has-port-position?
                                    has-set-port-position!?)
  port?                                 ;export
  (thread port-thread)                  ;private to the OS
  (type port-type)                      ;private
  (transcoder port-transcoder)          ;export
  (buffer-mode port-buffer-mode)        ;private
  (input? input-port*?)                 ;private
  (output? output-port*?)               ;private
  (has-port-position? port-has-port-position?) ;private
  (has-set-port-position!? port-has-set-port-position!?)) ;private

(define (binary-port? port)
  (eqv? (port-transcoder port) (binary-transcoder)))

(define (transcoded-port binary-port transcoder)
  (unless (binary-port? binary-port)
    (error 'transcoded-port "Expected a binary port" binary-port))
  (error 'transcoded-port 'not-implemented))

(define (port-position port)
  (unless (port-has-port-position? port)
    (assertion-violation 'port-position
                         "This port has no position" port))
  (let ((reply (!? (port-thread port) 'get-position)))
    ;; FIXME: let port-thread send exceptions
    reply))

(define (set-port-position! port pos)
  (unless (and (integer? pos)
               (>= pos 0))
    (assertion-violation 'set-port-position!
                         "The position must be a non-negative integer" pos))
  (unless (port-has-set-port-position!? port)
    (assertion-violation 'set-port-position!
                         "This port can not set its position" port))
  (let ((reply (!? (port-thread port) (list 'set-position! pos))))
    ;; FIXME: exceptions
    reply))

(define (close-port port)
  ;; FIXME: exceptions
  (!? (port-thread port) 'close))

(define (call-with-port port proc)
  (call-with-values (lambda ()
                      (proc port))
    (lambda x
      (close-port port)
      (apply values x))))

;;; 7.2.7. Input ports

(define (input-port? obj)
  (and (port? obj) (input-port*? obj)))

(define (port-eof? input-port)
  (unless (input-port? input-port)
    (assertion-violation 'get-u8 "Expected an input port" input-port))
  (eof-object? ((if (binary-port? input-port)
                    lookahead-u8
                    lookahead-char)
                input-port)))

;; (open-file-input-port filename :opt fileoptions transcoder)
;; (open-bytevector-input-port bytevector :opt transcoder)
;; (open-string-input-port string)
;; (standard-input-port)

(define (make-custom-binary-input-port
         id read! get-position set-position! close)
  (let ((id (string-copy id)))
    (define (binary-input-port-thread)
      (forever
        ;; FIXME: catch exceptions from the given functions
        (match (?)
          ((from tag 'get-u8)
           #f)
          ((from tag 'lookahead-u8)
           #f)
          ((from tag ('get-bytevector-n k))
           #f)
          ((from tag ('get-bytevector-n! bytevector start count))
           (! from (list tag (read! bytevector start count))))
          ((from tag 'get-bytevector-some)
           #f)
          ((from tag 'get-position)
           (! from (list tag (if get-position (get-position)))))
          ((from tag ('set-position! pos))
           (if set-position! (set-position! pos))
           (! from (list tag (unspecified))))
          ((from tag 'close)
           (if close (close))
           (! from (list tag (unspecified)))))))
    (make-port (spawn binary-input-port-thread) 'custom (binary-transcoder)
               (buffer-mode none) #t #f
               (not get-position) (not set-position!))))


;;; 7.2.8. Binary input

(define (get-u8 binary-input-port)
  (unless (and (input-port? binary-input-port) (binary-port? binary-input-port))
    (assertion-violation 'get-u8 "Expected a binary input port" binary-input-port))
  ;; FIXME: exceptions
  (!? (port-thread binary-input-port) 'get-u8))

(define (lookahead-u8 binary-input-port)
  (unless (and (input-port? binary-input-port) (binary-port? binary-input-port))
    (assertion-violation 'lookahead-u8 "Expected a binary input port" binary-input-port))
  ;; FIXME: exceptions
  (!? (port-thread binary-input-port) 'lookahead-u8))

(define (get-bytevector-n binary-input-port k)
  (unless (and (input-port? binary-input-port) (binary-port? binary-input-port))
    (assertion-violation 'get-bytevector-n "Expected a binary input port" binary-input-port))
  (!? (port-thread binary-input-port) (list 'get-bytevector-n k)))

(define (get-bytevector-n! binary-input-port bytevector start count)
  (unless (and (input-port? binary-input-port) (binary-port? binary-input-port))
    (assertion-violation 'get-bytevector-n! "Expected a binary input port" binary-input-port))
  (unless (and (bytevector? bytevector)
               (>= (bytevector-length bytevector) (+ start count)))
    (assertion-violation 'get-bytevector-n! "Expected a bytevector of at least start+count bytes" bytevector))
  (!? (port-thread binary-input-port) (list 'get-bytevector-n! bytevector start count)))

(define (get-bytevector-some binary-input-port)
  (unless (and (input-port? binary-input-port) (binary-port? binary-input-port))
    (assertion-violation 'get-bytevector-some "Expected a binary input port" binary-input-port))
  (!? (port-thread binary-input-port) 'get-bytevector-some))

;;; 7.2.10. Output ports

(define (output-port? obj)
  (and (port? obj) (output-port*? obj)))

(define (flush-output-port output-port)
  (unless (output-port? binary-input-port)
    (assertion-violation 'get-bytevector-some "Expected a binary input port" binary-input-port))
  (!? (port-thread output-port) 'flush))

(define (output-port-buffer-mode output-port)
  (unless (output-port? binary-input-port)
    (assertion-violation 'get-bytevector-some "Expected a binary input port" binary-input-port))
  (port-buffer-mode output-port))

(define (make-custom-binary-output-port
         id write! get-position set-position! close)
  (let ((id (string-copy id)))
    (define (binary-output-port-thread)
      (forever
        ;; FIXME: catch exceptions from the given functions
        (match (?)
          ((from tag ('put-bytevector bv))
           (! from (list tag (write! bv 0 (bytevector-length bv)))))
          ((from tag 'get-position)
           (! from (list tag (if get-position (get-position)))))
          ((from tag ('set-position! pos))
           (if set-position! (set-position! pos))
           (! from (list tag (unspecified))))
          ((from tag 'close)
           (if close (close))
           (! from (list tag (unspecified)))))))
    (make-port (spawn binary-output-port-thread) 'custom (binary-transcoder)
               (buffer-mode none) #f #t
               (not get-position) (not set-position!))))

(define (put-bytevector binary-output-port bytevector . rest)
  (unless (output-port? binary-input-port)
    (assertion-violation 'put-bytevector "Expected a binary input port" binary-input-port))
  (unless (bytevector? binary-input-port)
    (assertion-violation 'put-bytevector "Expected a bytevector" bytevector))
  (cond ((null? rest)
         (!? (port-thread binary-output-port) (list 'put-bytevector bytevector)))
        (else
         (let-optionals* rest ((start 0)
                               (count (- (bytevector-length bytevector) start)))
           (!? (port-thread binary-output-port)
               (list 'put-bytevector (bytevector-section/shared bytevector start (+ start count))))))))

(define (make-custom-binary-input/output-port
         id read! write! get-position set-position! close)
  (let ((id (string-copy id)))
    (define (binary-output-port-thread)
      (forever
        ;; FIXME: catch exceptions from the given functions
        (match (?)
          ((from tag ('put-bytevector bv))
           (! from (list tag (write! bv 0 (bytevector-length bv)))))
          ((from tag 'get-position)
           (! from (list tag (if get-position (get-position)))))
          ((from tag ('set-position! pos))
           (if set-position! (set-position! pos))
           (! from (list tag (unspecified))))
          ((from tag 'close)
           (if close (close))
           (! from (list tag (unspecified)))))))
    (make-port (spawn binary-output-port-thread) 'custom (binary-transcoder)
               (buffer-mode none) #f #t
               (not get-position) (not set-position!))))
