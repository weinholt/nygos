;; -*- coding: utf-8 -*-
;; The Unbelievable Operating System from Hell
;; Copyright © 2006-2007 Göran Weinholt <goran@weinholt.se>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; Convert a .bdf font file into a two-level table

(cond-expand
 (guile
  (use-modules (srfi srfi-1) (srfi srfi-9) (srfi srfi-13)
               (ice-9 syncase) (ice-9 rdelim))
  (load "srfi-69.scm")
  (define (->string x)
    (with-output-to-string
      (lambda ()
        (display x))))))

(define (make-bdf-reader file)
  (define (parse-bdf-line line)
    (with-input-from-string line
      (lambda ()
        (let lp ((val (read)))
          (if (eof-object? val)
              '()
              (cons val (lp (read))))))))
  (let ((port (open-input-file file)))
    (lambda ()
      (let ((line (read-line port)))
        (if (eof-object? line)
            line
            (parse-bdf-line line))))))

(define (make-bdf-parser read-line)
  (define (read-character)
    (let ((line (read-line)))
      (if (not (eq? 'ENCODING (car line)))
          (read-character)
          (let ((encoding (cadr line)))
            (let skip ((line (read-line)))
              (if (not (eq? 'BITMAP (car line)))
                  (skip (read-line))
                  (let lp ((line (read-line))
                           (ret '()))
                    (if (eq? 'ENDCHAR (car line))
                        (cons encoding (list->vector (reverse ret)))
                        (lp (read-line)
                            (cons (string->number (->string (car line)) 16)
                                  ret))))))))))
  (lambda ()
    (let lp ((line (read-line)))
      (case (car line)
        ((STARTCHAR)
         (read-character))
        ((ENDFONT)
         #f)
        (else
         (lp (read-line)))))))

(define (bdf->two-stage-table filename)
  (let ((font (make-hash-table))
        (parser (make-bdf-parser (make-bdf-reader filename))))
    (do ((character (parser) (parser)))
        ((or (not character)
             (> (car character) #x1ff)))
      (let ((table# (quotient (car character) #x100)))
        ;; Create the table if it doesn't exist
        (if (not (hash-table-ref/default font table# #f))
            (hash-table-set! font table# (make-vector 256 #f)))
        (let ((table (hash-table-ref/default font table# #f)))
          (vector-set! table
                       (modulo (car character) #x100)
                       (cdr character)))))
    (let ((ret (make-vector (+ 1 (apply max (hash-table-keys font))) #f)))
      (hash-table-walk font
                       (lambda (key value)
                         (vector-set! ret key value)))
      ret)))

(define (read-xbm file)
  ;; Take an XBM file that fits in 2x2 character cells in a 8x16 font.
  (define (read-lines file)
    (with-input-from-file file
      (lambda ()
        (do ((line (read-line) (read-line))
             (lines '() (cons line lines)))
            ((eof-object? line)
             (reverse! lines))))))
  (define (every-nth list n)
    (do ((i 0 (+ i 1))
         (l list (cdr l))
         (ret '() (if (zero? (modulo i n))
                      (cons (car l) ret)
                      ret)))
        ((null? l)
         (reverse! ret))))
  (define (reverse-byte x)
    (define nibbles '#(#b0000 #b1000 #b0100 #b1100 #b0010 #b1010 #b0110 #b1110
                       #b0001 #b1001 #b0101 #b1101 #b0011 #b1011 #b0111 #b1111))
    (logior (ash (vector-ref nibbles (logand x #x0f)) 4)
            (vector-ref nibbles (ash (logand x #xf0) -4))))
  (let* ((all (apply string-append (read-lines file)))
         (bytes (map (lambda (n)
                       (string->number (substring/shared n 2) 16))
                     (string-tokenize
                      (string-delete
                       (substring/shared
                        all
                        (+ 1 (string-index all #\{))
                        (string-index all #\})) #\,)))))
    (let ((bitmap (map (lambda (i)
                         (reverse-byte (logxor i #xff)))
                       bytes)))
      (let ((even (every-nth bitmap 2))
            (odd (every-nth (cdr bitmap) 2)))
        (vector (list->vector (take even 16))
                (list->vector (take odd 16))
                (list->vector (drop even 16))
                (list->vector (drop odd 16)))))))

(define (print . x) (for-each display x) (newline))

(define (write-font v)
  ;; This is just because guile doesn't have bytevectors
  (cond
   ((pair? v)
    (display "(")
    (write-font (car v))
    (display " . ")
    (write-font (cdr v))
    (display ")"))
   ((vector? v)
    (if (and (positive? (vector-length v))
             (number? (vector-ref v 0)))
        (display "#vu8(")               ;cheatery and trickery
        (display "#("))
    (do ((len (vector-length v))
         (i 0 (+ i 1)))
        ((= i len))
      (write-font (vector-ref v i))
      (if (> (- len 1) i)
          (display " ")))
    (display ")")
    (newline))
   (else
    (display v))))

(write-font `(define font-8x13 ',(bdf->two-stage-table "8x13.bdf")))
(newline)
;; Hack in a logo
(write-font `(define logo-font ',(read-xbm "../knight.xbm")))
(newline)
