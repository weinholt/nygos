;; -*- coding: utf-8 -*-
;; The Unbelievable Operating System from Hell
;; Copyright © 2007 Göran Weinholt <goran@weinholt.se>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define (symbol->string x)
  (if (not (symbol? x))
      (contract-violation 'symbol->string
                          "not a symbol argument" x))
  (string-copy (value-ref x 0)))

(define symbol-process
  (spawn
   (lambda ()
     (define (string->symbol x)
       (let* ((copy (string-copy x))
              (hash (string-hash copy))
              (table (sys!symbol-table))
              (index (modulo hash (vector-length table)))
              (bucket (vector-ref table index)))
         (cond ((find (lambda (e) (string=? (value-ref (car e) 0) copy)) bucket) =>
                (lambda (match)
                  (car match)))
               (else
                (let ((sym (unsafe-make-value 2 symbol)))
                  (value-set! sym 0 copy)
                  (value-set! sym 1 hash)
                  (vector-set! table index (cons (cons sym '())
                                                 bucket))
                  sym)))))
     (forever
       (match (?)
         ((from tag (? string? x))
          (! from (list tag (string->symbol x))))
         (_ #f))))))

(define (string->symbol x)
  (if (not (string? x))
      (contract-violation 'string->symbol
                          "not a string argument" x))
  (!? symbol-process x))

(define (sys!symbol-hash x)
  (if (not (symbol? x))
      (contract-violation 'sys!symbol-hash
                          "not a symbol argument" x))
  (value-ref x 1))
