;; -*- coding: utf-8 -*-
;; The Unbelievable Operating System from Hell
;; Copyright © 2005-2007 Göran Weinholt <goran@weinholt.se>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; Subroutines that access the VGA hardware.

;; Current text mode
(define cols 80)
(define rows 25)
(define font-height 16)

(define %vga-memory (make-bytevector/direct #x4000 #xa0000))
;; Get one extra row, which is always blank, so that scrolling the
;; screen up one line is easy.
(define %vga-text-memory (make-bytevector/direct (* cols (+ rows 1) 2) #xb8000))
(define %vga-regs (make-bytevector/io 32 #x3c0))

;; Attribute address, Sequencer, Graphics Address, CRT Controller

(define attribute-addr 0)
(define attribute-data-w 0)
(define attribute-data-r 1)
(define seq-addr 4)
(define seq-data 5)
(define gfx-addr 14)
(define gfx-data 15)
(define crtc-addr 20)
(define crtc-data 21)

;; Registers in the CRT Controller:
(define cursor-start #x0a)
(define cursor-end #x0b)

(define cursor-location-high #x0e)
(define cursor-location-low #x0f)

;; Sequencer registers
(define seq-map-addr 2)
(define seq-mode-addr 4)
(define seq-mode-clock-mode 1)
(define seq-mode-ext 2)
(define seq-mode-sequential 4)

;; Graphics address registers
(define gfx-map-reg 4)
(define gfx-mode-reg 5)
(define gfx-misc-reg 6)

;; DAC registers
(define dac-addr-r 7)
(define dac-addr-w 8)
(define dac-data 9)

;; Current state
(define vga-text-current-x 0)
(define vga-text-current-y 0)
(define vga-text-current-attr 7)

(define (with-vga-plane plane thunk)
  ;; I'm not sure if this works for anything other than plane 2
  (let* ((seq-map (seq-write/save seq-map-addr (expt 2 plane)))
         (seq-mode (seq-write/save seq-mode-addr
                                   (logior seq-mode-ext seq-mode-sequential)))
         (gfx-map (gfx-write/save gfx-map-reg plane))
         (gfx-mode (gfx-write/save gfx-mode-reg 0))
         (gfx-misc (gfx-write/save gfx-misc-reg 0)))
    (thunk)
    (seq-write seq-map-addr seq-map)
    (seq-write seq-mode-addr seq-mode)
    (gfx-write gfx-map-reg gfx-map)
    (gfx-write gfx-mode-reg gfx-mode)
    (gfx-write gfx-misc-reg gfx-misc)))

(define (crtc-read addr)
  (bytevector-u8-set! %vga-regs crtc-addr addr)
  (bytevector-u8-ref %vga-regs crtc-data))

(define (crtc-write addr byte)
  (bytevector-u8-set! %vga-regs crtc-addr addr)
  (bytevector-u8-set! %vga-regs crtc-data byte))

(define (attribute-read addr)
  (bytevector-u8-set! %vga-regs attribute-addr addr)
  (bytevector-u8-ref %vga-regs attribute-data-r))

(define (attribute-write addr byte)
  (bytevector-u8-set! %vga-regs attribute-addr addr)
  (bytevector-u8-set! %vga-regs attribute-data-w byte))

(define (seq-read offset)
  (bytevector-u8-set! %vga-regs seq-addr offset)
  (bytevector-u8-ref %vga-regs seq-data))

(define (seq-write offset data)
  (bytevector-u8-set! %vga-regs seq-addr offset)
  (bytevector-u8-set! %vga-regs seq-data data))

(define (seq-write/save offset data)
  (bytevector-u8-set! %vga-regs seq-addr offset)
  (let ((save (bytevector-u8-ref %vga-regs seq-data)))
    (bytevector-u8-set! %vga-regs seq-data data)
    save))

(define (gfx-read offset)
  (bytevector-u8-set! %vga-regs gfx-addr offset)
  (bytevector-u8-ref %vga-regs gfx-data))

(define (gfx-write offset data)
  (bytevector-u8-set! %vga-regs gfx-addr offset)
  (bytevector-u8-set! %vga-regs gfx-data data))

(define (gfx-write/save offset data)
  (bytevector-u8-set! %vga-regs gfx-addr offset)
  (let ((save (bytevector-u8-ref %vga-regs gfx-data)))
    (bytevector-u8-set! %vga-regs gfx-data data)
    save))


(define (coord x y)
  (* 2 (+ x (* y cols))))

(define (vga-cursor-get)
  (let ((pos (logior (ash (crtc-read cursor-location-high) 8)
                     (crtc-read cursor-location-low))))
    (values (modulo pos cols)
            (quotient pos cols))))

(define (vga-cursor-move x y)
  ;; TODO: implement a non-blinking block cursor by inverting the attribute
  (let ((offset (+ x (* y cols))))
    (crtc-write cursor-location-high (bit-field offset 8 16))
    (crtc-write cursor-location-low (bit-field offset 0 8))))

(define (vga-border-attr-set! attr)
  (attribute-write #x31 attr))

(define (vga-palette-ref i)
  (bytevector-u8-set! %vga-regs dac-addr-r i)
  (let ((r (bytevector-u8-ref %vga-regs dac-data))
        (g (bytevector-u8-ref %vga-regs dac-data))
        (b (bytevector-u8-ref %vga-regs dac-data)))
    (values r g b)))

(define (vga-palette-set! i r g b)
  (bytevector-u8-set! %vga-regs dac-addr-w i)
  (bytevector-u8-set! %vga-regs dac-data r)
  (bytevector-u8-set! %vga-regs dac-data g)
  (bytevector-u8-set! %vga-regs dac-data b))

(define (beep)
  (let-values (((r g b) (vga-palette-ref 0)))
    (vga-palette-set! 0 63 63 63)
    (thread-msleep! 50)
    (vga-palette-set! 0 r g b)))

(define (vga-text-set-cell x y c attr)
  (bytevector-u8-set! %vga-text-memory (coord x y) c)
  (bytevector-u8-set! %vga-text-memory (+ 1 (coord x y)) attr))

(define (vga-text-newline)
  (cond ((>= vga-text-current-y (- rows 1))
         (bytevector-copy! %vga-text-memory (* 2 cols) ; source
                           %vga-text-memory 0          ; destination
                           (* cols 2 rows)))
        (else
         (set! vga-text-current-y (+ vga-text-current-y 1))))
  (set! vga-text-current-x 0))

(define (vga-text-putchar c)
  (cond ((eq? c #\newline)
         (vga-text-newline))
        ((eq? c #\backspace)
         (set! vga-text-current-x (- vga-text-current-x 1))
         (if (negative? vga-text-current-x)
             (set! vga-text-current-x 0)))
        (else
         (vga-text-set-cell vga-text-current-x vga-text-current-y
                            (logand (char->integer c) #xff)
                            vga-text-current-attr)
         (set! vga-text-current-x (+ vga-text-current-x 1))
         (if (>= vga-text-current-x cols)
             (vga-text-newline)))))

(define (vga-text-putstring s)
  (let lp ((n 0) (max (string-length s)))
    (when (< n max)
      (vga-text-putchar (string-ref s n))
      (lp (+ n 1) max))))

(define (vga-text-clear-line y)
  (let lp ((x 0))
    (when (< x cols)
      (vga-text-set-cell x y 32 vga-text-current-attr)
      (lp (+ x 1)))))

(define (vga-text-clear)
  (vga-text-clear-line 0)
  (let lp ((y 1))
    (when (< y rows)
      (bytevector-copy! %vga-text-memory (* 80 2 (- y 1))
                        %vga-text-memory (* 80 2 y) (* 80 2))
      (lp (+ y 1))))
  (set! vga-text-current-x 0)
  (set! vga-text-current-y 0)
  (vga-cursor-move vga-text-current-x vga-text-current-y))

(define (vga-text-font-width-set! width)
  (case width
    ((8) (seq-write seq-mode-clock-mode
                    (logior (seq-read seq-mode-clock-mode) 1)))
    ((9) (seq-write seq-mode-clock-mode
                    (logand (seq-read seq-mode-clock-mode) #xfe)))))

(define (vga-set-cursor! start end)
  (crtc-write cursor-start (logior (logand (logxor #xff 31)
                                           (crtc-read cursor-start))
                                   (logand start 31)))
  (crtc-write cursor-end (logior (logand (logxor #xff 31)
                                         (crtc-read cursor-end))
                                 (logand end 31))))

;;; Init VGA

(receive (x y) (vga-cursor-get)
  (set! vga-text-current-x x)
  (set! vga-text-current-y y))

(vga-text-clear-line rows)

(define vga-text-process
  (spawn (lambda ()
           (let ((update-cursor #f)
                 (update-now (make-tag)))
             (forever
               (let ((x (? update-cursor update-now)))
                 (cond ((string? x)
                        ;; Wait for 10 ms, should probably be the same
                        ;; time as the keyboard repeat rate.
                        (set! update-cursor 10)
                        (vga-text-putstring x))
                       ((char? x)
                        (set! update-cursor 10)
                        (vga-text-putchar x))
                       ((eq? x update-now)
                        (set! update-cursor #f)
                        (vga-cursor-move vga-text-current-x vga-text-current-y))
                       ((and (pair? x) (eq? (car x) 'attr))
                        (set! vga-text-current-attr (cdr x))))))))))


;;; Experimenting with fonts. Something like Hurd's dynafont will probably
;;; be implemented.


(define (set-glyph index glyph)
  (with-vga-plane 2
    (lambda ()
      (do ((size (bytevector-length glyph))
           (i 0 (+ i 1)))
          ((= i font-height))
        (let ((byte (if (>= i size)
                        0
                        (bytevector-u8-ref glyph i))))
          (bytevector-u8-set! %vga-memory (+ i (* 32 index))
                              byte))))))


(define (font-ref font codepoint)
  (let* ((table (vector-ref font (quotient codepoint #x100)))
         (glyph (vector-ref table (modulo codepoint #x100))))
    glyph))

(vga-text-font-width-set! 8)
(do ((i 0 (+ i 1)))
    ((= i #xff))
  (let ((glyph (font-ref font-8x13 i)))
    (if glyph
        (set-glyph i glyph))))

;; Logo hack
(set-glyph 180 (vector-ref logo-font 0))
(set-glyph 181 (vector-ref logo-font 1))
(set-glyph 182 (vector-ref logo-font 2))
(set-glyph 183 (vector-ref logo-font 3))

(define (font-allocate codepoint)
  "Look in the multistage table to see if there already is a number
allocated for the given codepoint "
  #f)

(define (font-release codepoint)
  #f)

;; (spawn
;;  (lambda ()
;;    (let lp ((x 1) (order 1))
;;      (vga-palette-set! 0 0 0 x)
;;      (thread-msleep! 10)
;;      (let ((neworder (if (or (zero? x) (= x 62))
;;                          (- order)
;;                          order)))
;;        (lp (+ x neworder) neworder)))))

;; (define some-status 26)

;; (define (hrstart)
;;   (if (not (zero? (logand #x1 (bytevector-u8-ref %vga-regs some-status))))
;;       (hrstart)))
;; (define (hrend)
;;   (if (zero? (logand #x1 (bytevector-u8-ref %vga-regs some-status)))
;;       (hrend)))

;; (define (vrstart)
;;   (if (not (zero? (logand #x8 (bytevector-u8-ref %vga-regs some-status))))
;;       (vrstart)))
;; (define (vrend)
;;   (if (zero? (logand #x8 (bytevector-u8-ref %vga-regs some-status)))
;;       (vrend)))

;; (spawn
;;  (lambda ()
;;    (forever
;;      (thread-yield!)
;;      (vrstart) (vrend)
;;      (do ((x 0 (+ x 1)))
;;          ((= x 63))
;;        (hrstart)
;;        (vga-palette-set! 0 0 0 x)
;;        (hrend))
;;      (do ((x 63 (- x 1)))
;;          ((zero? x))
;;        (hrstart)
;;        (vga-palette-set! 0 0 0 x)
;;        (hrend)))))
