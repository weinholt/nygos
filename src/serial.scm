;; -*- coding: utf-8 -*-
;; The Unbelievable Operating System from Hell
;; Copyright © 2005-2007 Göran Weinholt <goran@weinholt.se>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; Routines for the 8250 UART and successors

(define-bytevector-struct serial-regs (endianness little)
  (u8 serial-RBR-ref! serial-THR-set!)  ; Receiver Buffer &
                                        ; Transmitter Holder Buffer
  (u8 serial-IER-ref serial-IER-set!)   ; Interrupt Enable Register
  (u8 serial-IIR-ref! serial-FCR-set!)  ; Interrupt Identification &
                                        ; FIFO Control Register
  (u8 serial-LCR-ref serial-LCR-set!)   ; Line Control Register
  (u8 serial-MCR-ref serial-MCR-set!)   ; Modem Control Register
  (u8 serial-LSR-ref!)                  ; Line Status Register
  (u8 serial-MSR-ref)                   ; Modem Status Register
  (u8 serial-SCR-ref serial-SCR-set!))  ; Scratch register

;; Accessible if bit 8 (DLAB) in LCR is 1
(define-bytevector-struct serial-regs-latch (endianness little)
  (u8 serial-DLL-ref serial-DLL-set!)   ; Divisor Latch Low Byte
  (u8 serial-DLM-ref serial-DLM-set!))  ; Divisor Latch High Byte

(define (serial-DL-set! i/o latch)
  (serial-DLL-set! i/o (bit-field latch 0 8))
  (serial-DLM-set! i/o (bit-field latch 8 16)))

(define IER-READ #b01)
(define IER-R/W #b11)

(define (uart-set-baudrate comport rate)
  (let ((latch (/ 115200 rate))
        (i/o (serial-port-i/o comport)))
    (if (zero? latch)
        (error 'uart-set-baudrate "tried to set baud rate to 0"))
    ;; Set the Divisor Latch Access Bit
    (serial-LCR-set! i/o (logior (serial-LCR-ref i/o) #x80))
    ;; Latch the new baudrate
    (serial-DL-set! i/o latch)
    ;; Clear the DLAB
    (serial-LCR-set! i/o (logand (serial-LCR-ref i/o) #x7f))))

(define (uart-set-protocol comport bits parity stop)
  (let ((par (cdr (assq parity '((no . #b000)
                                 (odd . #b001)
                                 (even . #b011)
                                 (mark . #b101)
                                 (space . #b111)))))
        (bits (cdr (assq bits '((5 . #b00)
                                (6 . #b01)
                                (7 . #b10)
                                (8 . #b11)))))
        (stop (if (= stop 1) 0 1)))
    (serial-LCR-set! (serial-port-i/o comport)
                     (logior (ash par 3)
                             (ash stop 2)
                             bits))
    (serial-LCR-set! (serial-port-i/o comport)
                     3)))

(define (make-tty-key-decoder keypress-consumer)
  "Creates a closure that takes bytes from a terminal and converts
them into keypresses which it sends to the `keypress-consumer'
closure."
  (lambda (byte)
    (keypress-consumer (integer->char byte))))


;; FIXME: we should in reality check all comports that share this IRQ
(define (make-serial-irq-handler port comport irq)
  (let ((i/o (serial-port-i/o comport))
        (key-decoder (make-tty-key-decoder act-on-keypress)))
    (define (serial-send comport byte)
      (enqueue! (serial-port-outq comport) byte))
    (define (handle-irq-once)
      (when (= (logand (serial-IIR-ref! i/o) 1) 0)
        (let ((lsr (serial-LSR-ref! i/o)))
          (when (= (logand lsr 1) 1)
            ;; There is data to read
            (while (= (logand (serial-LSR-ref! i/o) 1) 1)
              ;; TODO: buffering
              (key-decoder (serial-RBR-ref! i/o))))
          (when (= (logand lsr #b100000) #b100000)
            ;; It's ok to write now
            (while (and (= (logand (serial-LSR-ref! i/o) #b100000) #b100000)
                        (not (queue-empty? (serial-port-outq comport))))
              (serial-THR-set! i/o (dequeue!
                                    (serial-port-outq comport))))))
        (serial-IER-set! i/o 0)
        (if (queue-empty? (serial-port-outq comport))
            (serial-IER-set! i/o IER-READ)
            (serial-IER-set! i/o IER-R/W))))
    (lambda ()
      (set-irq-handler! irq (self))
      (handle-irq-once)
      (irq-acknowledge! irq)
      (forever
        (let ((x (?)))
          (cond ((eq? x irq)
                 (handle-irq-once)
                 (irq-acknowledge! irq))
                ((string? x)
                 (serial-IER-set! (serial-port-i/o comport) IER-R/W)
                 (do ((i 0 (+ i 1)))
                     ((= i (string-length x)))
                   (char->utf8-bytes (string-ref x i)
                                     (cut serial-send com1 <>))))
                ((and (list? x) (= (length x) 3)
                      (thread? (first x))
                      (tag? (second x))
                      (eq? (third x) 'ping))
                 (! (first x) (list (second x) 'pong)))
                ((char? x)
                 (serial-IER-set! (serial-port-i/o comport) IER-R/W)
                 (char->utf8-bytes x (cut serial-send com1 <>)))))))))


;;; FIXME: put all this inside the driver process above

(define-record-type serial-port (make-serial-port i/o inq outq) serial-port?
  (i/o serial-port-i/o)
  (inq serial-port-inq)
  (outq serial-port-outq))

(define-record-printer (serial-port x port)
  (print "#<serial-port "
         (serial-port-i/o x) " "
         (serial-port-inq x) " "
         (serial-port-outq x) ">"))

(define com1
  (make-serial-port (make-bytevector/io 8 #x3f8)
                    (make-queue)
                    (make-queue)))

(define com1-type
  (let ((iir (serial-IIR-ref! (serial-port-i/o com1))))
    (cond ((logtest iir #x40)
           (if (logtest iir #x80)
               (if (logtest iir #x20)
                   "16750"
                   "16550A")
               "16550"))
          (else
           (serial-SCR-set! (serial-port-i/o com1) #x2A)
           (if (= (serial-SCR-ref (serial-port-i/o com1)) #x2A)
               "16450"
               "8250")))))

(serial-FCR-set! (serial-port-i/o com1) #b11001111)

(uart-set-protocol com1 8 'no 1)
(uart-set-baudrate com1 9600)
(serial-IER-set! (serial-port-i/o com1) 0)
(serial-IER-set! (serial-port-i/o com1) IER-READ)

;; Ready To Send & Data Terminal Ready & OUT2 (enable IRQ)
(serial-MCR-set! (serial-port-i/o com1) #b1011)


(define com1-process (spawn-supervised (make-serial-irq-handler #f com1 4)))

(print "com1 activated (" com1-type ")")

