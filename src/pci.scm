;; -*- coding: utf-8 -*-
;; The Unbelievable Operating System from Hell
;; Copyright © 2006-2007 Göran Weinholt <goran@weinholt.se>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; Subroutines that access the PCI bus.

;;; Abstractions for PCI buses

(define-record-type pci-host (_make-pci-host id regs bus-type reader writer)
  pci-host?
  (id pci-host-id)
  (regs pci-host-regs)
  (bus-type pci-host-type)
  (reader pci-host-reader)
  (writer pci-host-writer))

(define (probe-pci-type regs)
  (bytevector-u8-set! regs 0 0)
  (bytevector-u8-set! regs 2 0)
  (if (and (zero? (bytevector-u8-ref regs 0)) (zero? (bytevector-u8-ref regs 2)))
      2
      (let ((tmp (bytevector-u32-ref regs 0 (endianness little))))
        (bytevector-u32-set! regs 0 #x80000000 (endianness little))
        (let ((tmp2 (bytevector-u32-ref regs 0 (endianness little))))
          (bytevector-u32-set! regs 0 tmp (endianness little))
          (if (= tmp2 #x80000000)
              1
              #f)))))

(define (pci-conf1-select! host bus slot function register)
  (bytevector-u32-set! (pci-host-regs host) 0
                       (logior #x80000000
                               (ash bus 16)
                               (ash slot 11)
                               (ash function 8)
                               (logand register #xfc))
                       (endianness little)))

(define (pci-conf1-read host bus slot function register size)
  (pci-conf1-select! host bus slot function register)
  (let ((offset (+ 4 (logand register #x3))))
    (case size
      ((u8) (bytevector-u8-ref (pci-host-regs host) offset))
      ((u16) (bytevector-u16-ref (pci-host-regs host) offset (endianness little)))
      ((u32) (bytevector-u32-ref (pci-host-regs host) offset (endianness little)))
      (else (error "bad size" size)))))

(define (pci-conf1-write! host bus slot function register size value)
  (pci-conf1-select! host bus slot function register)
  (let ((offset (+ 4 (logand register #x3))))
    (case size
      ((u8) (bytevector-u8-set! (pci-host-regs host)
                                offset value))
      ((u16) (bytevector-u16-set! (pci-host-regs host)
                                  offset value (endianness little)))
      ((u32) (bytevector-u32-set! (pci-host-regs host)
                                  offset value (endianness little)))
      (else (error "bad size" size)))))

(define (make-pci-host id base-address)
  (let* ((regs (make-bytevector/io 8 base-address))
         (type (probe-pci-type regs)))
    (case type
      ((1)  (_make-pci-host id regs type pci-conf1-read pci-conf1-write!))
      ((#f) #f)
      (else
       (print "PCI configuration mechanism #" type " is not supported.")
       #f))))

(define (pci-conf-read host bus slot function register size)
  ((pci-host-reader host) host bus slot function register size))

(define (pci-conf-write! host bus slot function register size value)
  ((pci-host-writer host) host bus slot function register size value))

;;; Abstractions for PCI devices

(define-record-type pci-device (_make-pci-device host bus slot function
                                                 command
                                                 vendor device revision
                                                 subvendor subdevice
                                                 base-class sub-class interface
                                                 base-addresses)
  pci-device?
  (host pci-device-host)
  (bus pci-device-bus)
  (slot pci-device-slot)
  (function pci-device-function)
  (command pci-device-command)
  (vendor pci-device-vendor)
  (device pci-device-device)
  (revision pci-device-revision)
  (subvendor pci-device-subvendor)
  (subdevice pci-device-subdevice)
  (base-class pci-device-base-class)
  (sub-class pci-device-sub-class)
  (interface pci-device-interface)
  (base-addresses pci-device-base-addresses))

(define-record-printer (pci-device dev port)
  (define (show . x) (for-each display x))
  (display "#<pci-device ")
  (show (pci-device-bus dev) ":" (pci-device-slot dev)
        "." (pci-device-function dev)
        " "
        (translate-class-code (pci-device-base-class dev)
                              (pci-device-sub-class dev)
                              (pci-device-interface dev))
        ": " (number->string (pci-device-vendor dev) 16)
        ":" (number->string (pci-device-device dev) 16))

  (unless (zero? (pci-device-revision dev))
    (show " (rev " (pci-device-revision dev) ")"))
  (display ">"))

(define (make-pci-device host bus slot function)
  (define (get register size)
    (pci-conf-read host bus slot function register size))
  (define (put register size value)
    (pci-conf-write! host bus slot function register size value))
  (define (get-bases)
    (let lp ((address #x10)
             (bases '()))
      (let ((data (get address 'u32)))
        (define (test-size mask max)
          ;; Write all ones to this base address register.
          ;; When reading it back, there will be zeroes in the
          ;; don't-care bits.
          (put address 'u32 #xffffffff)
          (let ((size (+ 1 (logxor max (logand mask (get address 'u32))))))
            (put address 'u32 data)
            size))
        (cond ((>= address #x28)
               (reverse! bases))
              ((zero? data)
               (lp (+ address 4) bases))
              ((= 1 (logand 1 data))
               ;; I/O space
               (let* ((mask #xfffe)
                      (base (logand mask data)))
                 (lp (+ address 4)
                     (cons (list 'I/O address base (test-size mask #xffff))
                           bases))))
              (else
               ;; Memory space
               (let* ((mask #xFFFFfff0)
                      (base (logand mask data))
                      (type (vector-ref '#(32-bit 1-meg 64-bit reserved)
                                        (bit-field data 1 3))))
                 (lp (+ address (if (eq? type '64-bit) 8 4))
                     (cons (list 'memory address base (test-size mask #xffffffff)
                                 type)
                           bases))))))))

  (let* ((vendor (get #x00 'u16))
         (device (get #x02 'u16))
         (command (get #x04 'u16))
         (revision (get #x08 'u8))
         (interface (get #x09 'u8))
         (sub-class (get #x0a 'u8))
         (base-class (get #x0b 'u8))
         (header-type (get #x0e 'u8))
         (layout (bit-field header-type 0 7)))

    (_make-pci-device host bus slot function command
                      vendor device revision
                      ;; If the layout is #x00, then the subsystem vendor id
                      ;; and subsystem id are provided in the registers below.
                      (and (= layout 0) (get #x2c 'u16))
                      (and (= layout 0) (get #x2e 'u16))
                      base-class sub-class interface
                      (and (= layout 0) (get-bases)))))

;; Match a device against the provided numbers
(define (pci-device-id-fits? dev vendor device subvendor subdevice
                             base-class sub-class interface)
  (and (if vendor (eq? vendor (pci-device-vendor dev)) #t)
       (if device (eq? device (pci-device-device dev)) #t)
       (if subvendor (eq? subvendor (pci-device-subvendor dev)) #t)
       (if base-class (eq? base-class (pci-device-base-class dev)) #t)
       (if sub-class (eq? sub-class (pci-device-sub-class dev)) #t)
       (if interface (eq? interface (pci-device-interface dev)) #t)))

;; The first return value is true if there is a device present, the
;; second value is true if the device has multiple functions.
(define (pci-device-present? host bus slot function)
  (define (get register size)
    (pci-conf-read host bus slot function register size))
  (let ((vendor (get #x00 'u16))
        (header-type (get #x0e 'u8)))
    (if (= vendor #xffff)
        (values #f #f)
        (values #t (= 1 (bit-field header-type 7 8))))))

(define (pci-device-conf-read dev register size)
  (pci-conf-read (pci-device-host dev)
                 (pci-device-bus dev)
                 (pci-device-slot dev)
                 (pci-device-function dev)
                 register
                 size))

(define (pci-device-conf-write! dev register size value)
  (pci-conf-write! (pci-device-host dev)
                   (pci-device-bus dev)
                   (pci-device-slot dev)
                   (pci-device-function dev)
                   register
                   size
                   value))

(define (pci-device-get-resource dev index)
  (let ((base (list-ref (pci-device-base-addresses dev) index)))
    (cond ((eq? (car base) 'I/O)
           (make-bytevector/io (fourth base) (third base)))
          ((eq? (car base) 'memory)
           (make-bytevector/direct (fourth base) (third base)))
          (else (error "bad data in base address list" dev)))))

(define (pci-device-irq dev)
  (pci-device-conf-read dev #x3c 'u8))

(define (pci-device-is-bridge? dev)
  (= #x01 (bit-field (pci-device-conf-read dev #x0e 'u8) 0 7)))

(define (pci-device-bridge-max dev)
  ;; #x18 = primary bus number, #x19 = secondary bus number,
  ;; #x1a = subordinate bus number
  (pci-device-conf-read dev #x1a 'u8))

(define (pci-device-disable! dev)
  (pci-device-conf-write! dev #x04 'u16 0))

(define (pci-device-enable! dev)
  (pci-device-conf-write! dev #x04 'u16 (pci-device-command dev)))

(define (translate-class-code base-class sub-class interface)
  (case base-class
    ;; TODO: use pci.ids instead (http://pciids.sf.net/)
    ((#x00) "built before class codes")
    ((#x01)
     (case sub-class
       ((#x00) "SCSI bus controller")
       ((#x01) "IDE controller")
       ((#x02) "Floppy disk controller")
       ((#x03) "IPI bus controller")
       ((#x04) "RAID controller")
       ((#x80) "Other mass storage controller")
       (else "Mass storage")))
    ((#x02) "Network controller")
    ((#x03)
     (case sub-class
       ((#x00)
        (case interface
          ((#x00) "VGA-compatible controller")
          ((#x01) "8514-compatible controller")))
       (else "Display controller")))
    ((#x04) "Multimedia device")
    ((#x05) "Memory controller")
    ((#x06)
     (case sub-class
       ((#x00) "Host bridge")
       ((#x01) "ISA bridge")
       ((#x02) "EISA bridge")
       ((#x04) "PCI bridge")
       ((#x09) "Semi-transparent PCI-to-PCI bridge")
       (else   "Bridge device")))
    ((#x07) "Simple communication controller")
    ((#x08) "Base system peripherals")
    ((#x09) "Input devices")
    ((#x0a) "Docking stations")
    ((#x0b) "Processors")
    ((#x0c)
     (case sub-class
       ((#x00) "FireWire (IEEE 1394)")
       ((#x01) "ACCESS.bus.")
       ((#x02) "SSA")
       ((#x03) "USB Controller")
       ((#x04) "Fibre Channel")
       (else "Serial bus controllers")))
    ((#xff) "does not fit anywhere")
    (else "reserved")))


(define *pci-devices* '())

(let ((host0 (make-pci-host 0 #xCF8)))
  (cond ((and host0 (= (pci-host-type host0) 1))
         (print "Scanning the PCI bus...")
         (do ((number-of-buses 1)
              (bus 0 (+ bus 1)))
             ((= bus number-of-buses))
           (do ((functions 1 1)
                (slot 0 (+ slot 1)))
               ((= slot 32))
             (do ((func 0 (+ func 1)))
                 ((= func functions))
               (receive (present multifunc)
                   (pci-device-present? host0 bus slot func)
                 (if (and (zero? func) multifunc)
                     (set! functions 8))
                 (when present
                   (let ((dev (make-pci-device host0 bus slot func)))
                     (set! *pci-devices* (cons dev *pci-devices*))
                     ;; If this is a PCI-to-PCI bridge it probably has more buses
                     (when (pci-device-is-bridge? dev)
                       (set! number-of-buses
                             (max number-of-buses
                                  (+ 1 (pci-device-bridge-max dev)))))))))))
         (set! *pci-devices* (reverse! *pci-devices*))
         (for-each print *pci-devices*))
        (else
         (print "No PCI bus detected."))))

;;; Try making a driver...

(define-record-type pci-driver (make-pci-driver name ids init)
  pci-driver?
  (name pci-driver-name)
  (ids pci-driver-ids)
  (init pci-driver-init))

(define (register-driver driver)
  (cond ((pci-driver? driver)
         (for-each (lambda (dev)
                     (if (any (lambda (id)
                                (apply pci-device-id-fits? dev id))
                              (pci-driver-ids driver))
                         (spawn
                          (lambda ()
                            ((pci-driver-init driver) driver dev)))))
                   *pci-devices*))
        (else
         (error "register-driver takes a driver as its argument" driver))))

;; FireWire is an expansion bus that looks like a peripheral bus, it
;; can do bus mastering and DMA. It is unclear if it can do this
;; without our approval, but lets not take any chances.
(register-driver
 (make-pci-driver
  "Anti FireWire driver" '((#f #f #f #f #x0C 0 #f))
  (lambda (driver dev)
    (print "FireWire device found and disconnected from the bus.")
    (pci-device-disable! dev))))
