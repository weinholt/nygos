;; -*- coding: utf-8 -*-
;; The Unbelievable Operating System from Hell
;; Copyright © 2005-2007 Göran Weinholt <goran@weinholt.se>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; IPv6, used by network.scm

;;; RFC 2460: Internet Protocol, Version 6 (IPv6) Specification

(define-bytevector-struct ip6-header (endianness big)
  ;; FIXME: improve define-bytevector-struct with syntax-case
;;   (u4 ip6-version-ref ip6-version-set!)
;;   (u8 ip6-traffic-class-ref ip6-traffic-class-set!)
;;   (u20 ip6-flow-label-ref ip6-flow-label-set!)
  (u32 ip6-v/tc/fl-ref ip6-v/tc/fl-set!)
  (u16 ip6-payload-length-ref ip6-payload-length-set!)
  (u8 ip6-next-header-ref ip6-next-header-set!)
  (u8 ip6-hop-limit-ref ip6-hop-limit-set!)
;;   (u128 ip6-source-address-ref ip6-source-address-set!)
;;   (u128 ip6-destination-address-ref ip6-destination-address-set!)
  )

(define (ip6-version-ref pkt)
  (ash (ip6-v/tc/fl-ref pkt) -28))
(define (ip6-traffic-class-ref pkt)
  (logand #xff (ash (ip6-v/tc/fl-ref pkt) -20)))
(define (ip6-flow-label-ref pkt)
  (logand #xfffff (ip6-v/tc/fl-ref pkt)))
(define (ip6-source-address-ref pkt)
  (bytevector-section pkt 8 24))
(define (ip6-destination-address-ref pkt)
  (bytevector-section pkt 24 40))

(define (process-ip6-packet interface pkt)
  "Process one IPv6 packet that came in on the given interface."
  ;; FIXME: check routes and interface addresses
  ;; FIXME: check length
  ;; FIXME: options
  (case (ip6-next-header-ref pkt)
    ((58)
     (process-icmp6-packet interface    ;interface
                           (bytevector-section/shared pkt 0 40) ;header
                           (bytevector-section/shared pkt 40 (+ 40 (ip6-payload-length-ref pkt))))) ;pkt
    ((17)
     (process-udp6-packet interface
                          (bytevector-section/shared pkt 0 40)
                          (bytevector-section/shared pkt 40 (+ 40 (ip6-payload-length-ref pkt)))))
    ((6)
     ;; TCP
     (process-tcp-packet (bytevector-section/shared pkt 40 (+ 40 (ip6-payload-length-ref pkt)))
                         (ip6-bytevector->inetaddr (ip6-source-address-ref pkt)
                                                   interface)
                         (ip6-bytevector->inetaddr (ip6-destination-address-ref pkt)
                                                   interface)))
    (else
     (print "ip6 header " (ip6-next-header-ref pkt) " not supported"))))

(define (sum-bytevector-length bvs)
  "Return the total number of bytes in a list of bytevectors."
  (do ((l bvs (cdr l))
       (sum 0 (+ sum (bytevector-length (car l)))))
      ((null? l) sum)))

(define (ip6-send payload source target next-header)
  (cond ((route-lookup-by-target target) =>
         (lambda (route)
           (cond ((interface-lookup-neighbor (route-interface route)
                                             (or (route-router route) target))
                  =>
                  (lambda (ncache-entry)
                    (let ((hdr (make-bytevector 40)))
                      (ip6-v/tc/fl-set! hdr (ash 6 28))
                      (ip6-payload-length-set! hdr (sum-bytevector-length payload))
                      (ip6-next-header-set! hdr next-header)
                      (ip6-hop-limit-set! hdr 255)
                      (bytevector-copy! (inetaddr-bytevector (route-source route)) 0 hdr 8 16)
                      (bytevector-copy! (inetaddr-bytevector target) 0 hdr 24 16)
                      (ethernet-send (route-interface route)
                                     (ncache-entry-lladdr ncache-entry)
                                     #x86dd (cons hdr payload)))))
                 (else
                  (print "not in neighbor table: " target)))))
        (else
         (print "no route to host: " target))))

(define (ip6-send* interface pkt source target next-header)
  (cond ((interface-lookup-neighbor interface (ip6-bytevector->inetaddr target interface)) =>
         (lambda (ncache-entry)
           (let ((lladdr (ncache-entry-lladdr ncache-entry))
                 (hdr (make-bytevector 40)))
             (ip6-v/tc/fl-set! hdr (ash 6 28))
             (ip6-payload-length-set! hdr (sum-bytevector-length pkt))
             (ip6-next-header-set! hdr next-header)
             (ip6-hop-limit-set! hdr 255)
             (bytevector-copy! source 0 hdr 8 16)
             (bytevector-copy! target 0 hdr 24 16)
             (ethernet-send interface lladdr #x86dd (cons hdr pkt)))))))

(define (process-udp6-packet interface ip6-header pkt)
  (process-udp-packet pkt (ip6-bytevector->inetaddr
                           (ip6-source-address-ref ip6-header)
                           interface)
                      (ip6-bytevector->inetaddr
                       (ip6-destination-address-ref ip6-header)
                       interface)
                      0 '()))

(define (process-tcp6-packet interface ip6-header pkt)
  (print "tcp6 " pkt))

;;; RFC 1885: Internet Control Message Protocol (ICMPv6), for the
;;; Internet Protocol Version 6 (IPv6), Specification

(define-bytevector-struct icmp6-header (endianness big)
  (u8 icmp6-type-ref icmp6-type-set!)
  (u8 icmp6-code-ref icmp6-code-set!)
  (u16 icmp6-checksum-ref icmp6-checksum-set!))

(define (process-icmp6-packet interface ip6-header pkt)
  ;; (print "icmp6 ip6-header = " ip6-header " pkt = " pkt)
  (case (icmp6-type-ref pkt)
    ((135)
     (process-icmp6-ndp interface ip6-header pkt))
    ((128)
     (process-icmp6-echo-request interface ip6-header pkt))
    ((133)                              ;router solicitation
     #f)
    (else
     (print "icmp6 type=" (icmp6-type-ref pkt)
         " code=" (icmp6-code-ref pkt)
         " data=" pkt))))

(define (icmp6-send interface pkt source target type code)
  (let ((hdr (make-bytevector 4))
        (pseudo (make-bytevector 8)))
    (icmp6-type-set! hdr type)
    (icmp6-code-set! hdr code)
    (bytevector-u32-set! pseudo 0 (+ 4 (bytevector-length pkt)) (endianness big))
    (bytevector-u8-set! pseudo 7 58)
    (bytevector-u16-native-set! hdr 2
                                (ip-native-checksum-finish
                                 (+ (ip-native-checksum-part source)
                                    (ip-native-checksum-part target)
                                    (ip-native-checksum-part pseudo)
                                    (ip-native-checksum-part hdr)
                                    (ip-native-checksum-part pkt))))
    (ip6-send* interface (list hdr pkt) source target 58)))

;; ping!
(define-bytevector-struct icmp6-echo (endianness big)
  (u16 icmp6-echo-identifier-ref icmp6-echo-identifier-set!)
  (u16 icmp6-sequence-number-ref icmp6-sequence-number-set!))

(define (process-icmp6-echo-request interface ip6-header pkt)
  ;; FIXME: provide the neighbor cache with reachability info
  (icmp6-send interface (bytevector-section/shared pkt 4 (bytevector-length pkt))
              (ip6-destination-address-ref ip6-header)
              (ip6-source-address-ref ip6-header)
              129 0))

;;; Neighbor Discovery for IP Version 6 (IPv6)

(define (icmp6-ndp-target-address-ref pkt)
  (bytevector-section/shared pkt 8 24))

(define ICMP6-NDP-OPTION-SOURCE-LLADDR 1)

(define-bytevector-struct macaddr (endianness big)
  (u48 bytevector->lladdr))

(define (process-icmp6-ndp interface ip6-header pkt)
  (define (unspecified-address? address)
    #f)
  (define (solicited-node-mcast-addr? address)
    ;; Match against FF02::1:FF00:0000/104
    (and (= #xFF02 (bytevector-u16-ref address 0 (endianness big)))
         (= #x0000 (bytevector-u16-ref address 2 (native-endianness)))
         (= #x00000000 (bytevector-u32-ref address 4 (native-endianness)))
         (= #x00000001 (bytevector-u32-ref address 8 (endianness big)))
         (= #xFF (bytevector-u8-ref address 12))))
  ;; See RFC 2461, 7.1.1. Validation of Neighbor Solicitations
  (when (and (= (ip6-hop-limit-ref ip6-header) 255) ;not routed
             ;; FIXME: check the checksum
             (>= (bytevector-length pkt) 24)
             (zero? (icmp6-code-ref pkt))
             ;; FIXME: check that it's not a multicast target
             (or (not (unspecified-address? (ip6-source-address-ref ip6-header)))
                 (solicited-node-mcast-addr? (ip6-destination-address-ref ip6-header))))
    (let* ((options (bytevector-section/shared pkt 24 (bytevector-length pkt)))
           (source-lladdr (icmp6-ndp-get-option options ICMP6-NDP-OPTION-SOURCE-LLADDR))
           (source (ip6-bytevector->inetaddr (ip6-source-address-ref ip6-header) interface)))
      (when (and (icmp6-ndp-sanity-check? options)
                 (or (unspecified-address? (ip6-source-address-ref ip6-header))
                     source-lladdr))
        (when (interface-has-ip6? interface (icmp6-ndp-target-address-ref pkt))
          (interface-add-neighbor interface
                                  source
                                  (bytevector->lladdr source-lladdr) 'stale)
          (let ((adv (make-bytevector 28))
                (mac (interface-lladdr interface)))
            (bytevector-u32-set! adv 0 (logior (ash 1 30) ;solicited, override
                                               (ash 1 29))
                                 (endianness big))
            (bytevector-copy! (icmp6-ndp-target-address-ref pkt) 0 adv 4 16)
            (bytevector-u8-set! adv 20 2) ;option, destination lladdr
            (bytevector-u8-set! adv 21 1) ;option, 8 bytes (really 6)
            (bytevector-u32-set! adv 22 (ash mac -16) (endianness big))
            (bytevector-u16-set! adv 26 (logand mac #xffff) (endianness big))
            (icmp6-send interface adv (icmp6-ndp-target-address-ref pkt)
                        (ip6-source-address-ref ip6-header) 136 0)))))))

(define (icmp6-ndp-sanity-check? options)
  ;; From RFC 2461: Nodes MUST silently discard an ND packet that
  ;; contains an option with length zero.
  (let lp ((offset 0))
    (cond ((> (+ offset 2) (bytevector-length options))
           #t)
          ((zero? (bytevector-u8-ref options (+ offset 1)))
           #f)
          (else
           (lp (+ offset (* 8 (bytevector-u8-ref options (+ offset 1)))))))))

(define (icmp6-ndp-get-option options option)
  (let lp ((offset 0))
    (cond ((> (+ offset 2) (bytevector-length options))
           #f)
          ((= (bytevector-u8-ref options offset) option)
           ;; Found the option, section it out. The size is in the
           ;; second byte, but the packet might be shorter than that.
           (bytevector-section/shared
            options (+ offset 2)
            (min (* 8 (bytevector-u8-ref options (+ offset 1)))
                 (- (bytevector-length options) offset))))
          (else
           (lp (+ offset (* 8 (bytevector-u8-ref options (+ offset 1)))))))))
