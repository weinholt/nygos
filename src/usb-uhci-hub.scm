;; -*- coding: utf-8 -*-
;; The Unbelievable Operating System from Hell
;; Copyright © 2006-2007 Göran Weinholt <goran@weinholt.se>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; Driver for the UHCI root hub

(define (PORTSCn-ref regs n)
  (bytevector-u16-ref regs (+ #x10 (* n 2)) (endianness little)))
(define (PORTSCn-set! regs n v)
  (bytevector-u16-set! regs (+ #x10 (* n 2)) v (endianness little)))

(define PORTSC-suspend      #b1000000000000) ;R/W
(define PORTSC-port-reset      #b1000000000) ;R/W
(define PORTSC-low-speed-device #b100000000) ;RO
(define PORTSC-resume-detect      #b1000000) ;R/W
(define PORTSC-line-status         #b110000) ;RO
(define PORTSC-enable/disable-change #b1000) ;R/WC
(define PORTSC-enabled/disabled       #b100) ;R/W
(define PORTSC-connect-status-change   #b10) ;R/WC
(define PORTSC-connect-status           #b1) ;RO
(define PORTSC-R/W          #b1001001000100)
(define PORTSC-R/WC                  #b1010)

(define (display-PORTSC s)
  (define (display-flag desc bit)
    (cond ((= (logand s bit) bit)
           (! vga-text-process '(attr . 12))
           (display desc) (display #\+)
           (! vga-text-process '(attr . 7)))
          (else
           (display desc) (display #\-)))
    (display " "))
  (display-flag "Connect" PORTSC-connect-status)
  (display-flag "ConnectC" PORTSC-connect-status-change)
  (display-flag "Enabled" PORTSC-enabled/disabled)
  (display-flag "EnabledC" PORTSC-enable/disable-change)
  (display-flag "ResumeD" PORTSC-resume-detect)
  (display "Line=")
  (display (ash (logand s PORTSC-line-status) -4))
  (display " ")
  (display-flag "LowS" PORTSC-low-speed-device)
  (display-flag "Reset" PORTSC-port-reset)
  (display-flag "Suspend" PORTSC-suspend))


(define (PORTSCn-connect-changed-clear! regs n)
  (PORTSCn-set! regs n (logior PORTSC-connect-status-change
                               (logand PORTSC-R/W
                                       (PORTSCn-ref regs n)))))
(define (PORTSCn-changes-clear! regs n)
  (PORTSCn-set! regs n (logior PORTSC-connect-status-change
                               PORTSC-enable/disable-change
                               (logand PORTSC-R/W
                                       (PORTSCn-ref regs n)))))

(define (PORTSCn-assert-reset! regs n)
  (PORTSCn-set! regs n (logior PORTSC-port-reset
                               (logand PORTSC-R/W
                                       (PORTSCn-ref regs n)))))
(define (PORTSCn-unassert-reset! regs n)
  (PORTSCn-set! regs n (logand (logxor PORTSC-R/W PORTSC-port-reset)
                               (PORTSCn-ref regs n))))

(define (PORTSCn-enable! regs n)
  (PORTSCn-set! regs n (logior PORTSC-enabled/disabled
                               (logand PORTSC-R/W
                                       (PORTSCn-ref regs n)))))
(define (PORTSCn-disable! regs n)
  (PORTSCn-set! regs n (logand (logxor PORTSC-R/W PORTSC-enabled/disabled)
                               (PORTSCn-ref regs n))))


(define (make-uhci-hcd-root-hub-driver uhci-device hub)
  "USB hub driver for the UHCI host controller root hub. All
communication is with the USBD."
  (lambda ()
    (define (valid-port? x)
      (or (= x 1) (= x 2)))
    (let* ((ports (uhci-device-ports uhci-device))
           (regs (uhci-device-regs uhci-device))
           (timeout (make-tag)))
      (forever
        (match (? 1000 timeout)
          ((pid tag ('clear-port-feature (? valid-port? port) feature))
           (let ((port (- port 1)))
             (case feature
               ((port-enable)
                (PORTSCn-disable! regs port)
                (! pid (list tag #t))))))

          ((pid tag ('set-port-feature (? valid-port? port) feature))
           (let ((port (- port 1)))
             (case feature
               ((port-reset)
                ;; Reset and enable the port
                (PORTSCn-assert-reset! regs port)   (thread-msleep! 250)
                (PORTSCn-unassert-reset! regs port) (thread-msleep! 100)
                (PORTSCn-enable! regs port)         (thread-msleep! 50)
                (! pid (list tag #t))))))

          ((pid tag ('get-port-status (? valid-port? port)))
           (print "will get port status ")
           (let ((response (make-bytevector 4))
                 (sc (PORTSCn-ref regs (- port 1))))
             ;; FIXME: these bitfields are not completely filled out
             (bytevector-u16-set!       ; Port status bits
              response 0
              (logior (if (logtest sc PORTSC-connect-status)    #b1 0)
                      (if (logtest sc PORTSC-enabled/disabled)  #b10 0)
                      (if (logtest sc PORTSC-port-reset)        #b10000 0)
                      (if (logtest sc PORTSC-low-speed-device)  #b1000000000 0))
              (endianness little))
             (bytevector-u16-set!       ; Port status change bits
              response 2
              (logior (if (logtest sc PORTSC-connect-status-change) #b1 0)
                      (if (logtest sc PORTSC-enable/disable-change) #b10 0))
              (endianness little))
             (! pid (list tag response))))

          ((? (lambda (x) (eq? timeout x)) x)
           (when (logtest (PORTSCn-ref regs 0) PORTSC-connect-status-change)
             ;; FIXME: this is probably wrong: the connect status
             ;; should probably be cleared by the USBD.
             (PORTSCn-changes-clear! regs 0)
             (usb-device-connect-notify hub 1))
           (when (logtest (PORTSCn-ref regs 1) PORTSC-connect-status-change)
             (PORTSCn-changes-clear! regs 1)
             (usb-device-connect-notify hub 2)))

          (_ #f))))))
