/* -*- coding: utf-8 -*-
 * The Unbelievable Operating System from Hell
 * Copyright (C) 1999  Free Software Foundation, Inc.
 * Copyright © 2005-2007 Göran Weinholt <goran@weinholt.se>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */
#include <nygos.h>
#include <stdio.h>
#include <string.h>
#include <multitasking.h>

/* Some screen stuff.  */
/* The number of columns.  */
#define COLUMNS			80
/* The number of lines.  */
#define LINES			24
/* The attribute of a character.  */
#define ATTRIBUTE		2

/* Variables.  */
/* Save the X position.  */
static int xpos;

/* Save the Y position.  */
static int ypos;

/* Point to the video memory.  */
static volatile uint8 *video = (uint8 *) 0xB8000;

#include <number.h>

static void real_display(SCM v, int write)
{
	switch (v) {
	case EOF:
		puts("#<eof>");
		return;
	case UNSPEC:
		puts("#<unspecified>");
		return;
	case EOL:
		puts("()");
		return;
	case BOOL_F:
		puts("#f");
		return;
	case BOOL_T:
		puts("#t");
		return;
	default:
		break;
	}

	if (IS_INUM(v)) {
		puti(INUM_TO_INT(v), 10);
	} else if (IS_ICHAR(v)) {
		wchar_t c = ICHAR_TO_WCHAR(v);

		if (c == L' ')
			puts("#\\space");
		else if (c == L'\n')
			puts("#\\newline");
		else if (c > L' ' && c < 128) {
			puts("#\\");
			putchar(c);
		} else {
			puts("#\\u");
			puti(c, 16);
		}
	} else if (IS_STRING(v)) {
		puts("#<string>");
	} else if (IS_SYMBOL(v)) {
		puts("#<symbol>");
	} else if (IS_THREAD(v)) {
		puts("#<thread>");
	} else if (IS_PROC(v)) {
		puts("#<procedure>");
	} else if (IS_RECORD(v)) {
		puts("#<record>");
	} else if (IS_PAIR(v)) {
		putchar('(');
		for (; v != EOL; v = CDR(v)) {
			real_display(CAR(v), write);
			if (CDR(v) == EOL)
				continue;
			if (IS_PAIR(CDR(v)))
				puts(" ");
			else {
				puts(" . ");
				real_display(CDR(v), write);
				break;
			}
		}
		putchar(')');
	} else {
		puts("#<unknown #x");
		puti(v, 16);
		putchar('>');
	}
}

void write(SCM v)
{
	real_display(v, 1);
}

/* Clear the screen and initialize XPOS and YPOS.  */
void cls(void)
{
	short cursor_pos;
	outb(0xe, 0x3d4);
	cursor_pos = inb(0x3d5) << 8;
	outb(0xf, 0x3d4);
	cursor_pos |= inb(0x3d5);
	xpos = cursor_pos % COLUMNS;
	ypos = cursor_pos / COLUMNS;
}

void getxy(int *x, int *y)
{
	*x = xpos;
	*y = ypos;
}
void setxy(int x, int y)
{
	xpos = x;
	ypos = y;
}

/* Convert the integer D to a string and save the string in BUF. */
static void itoa(char *buf, int base, int64 d)
{
	char *p = buf;
	char *p1, *p2;
	unsigned long ud = d;
	int divisor = 10;

	/* If %d is specified and D is minus, put `-' in the head.  */
	if (base == 10 && d < 0) {
		*p++ = '-';
		buf++;
		ud = -d;
	} else if (base == 16)
		divisor = 16;

	/* Divide UD by DIVISOR until UD == 0.  */
	do {
		int remainder = ud % divisor;

		*p++ = (remainder < 10) ? remainder + '0' : remainder + 'a' - 10;
	}
	while (ud /= divisor);

	/* Terminate BUF.  */
	*p = 0;

	/* Reverse BUF.  */
	p1 = buf;
	p2 = p - 1;
	while (p1 < p2) {
		char tmp = *p1;

		*p1 = *p2;
		*p2 = tmp;
		p1++;
		p2--;
	}
}

/* Put the character C on the screen.  */
void putchar(unsigned char c)
{
	if (c == '\n' || c == '\r') {
	  newline:
		xpos = 0;
		ypos++;
		if (ypos >= LINES) {
			ypos--;
			memmove((void *) video, (void *) video + (COLUMNS * 2), COLUMNS * (LINES - 1) * 2);
			memset((void *) video + (LINES - 1) * COLUMNS * 2, 0, COLUMNS * 2);
		}

		short cursor_pos = (ypos + 1) * COLUMNS;
		outb(0xe, 0x3d4);
		outb(cursor_pos >> 8, 0x3d5);
		outb(0xf, 0x3d4);
		outb(cursor_pos & 0xff, 0x3d5);
		return;
	}

	*(video + (xpos + ypos * COLUMNS) * 2) = c & 0xFF;
	*(video + (xpos + ypos * COLUMNS) * 2 + 1) = ATTRIBUTE;

	xpos++;
	if (xpos >= COLUMNS)
		goto newline;
}

void puts(const char *s)
{
	while (*s)
		putchar(*s++);
}

void puti(int64 i, int base)
{
	char buf[30];

	itoa(buf, base, i);
	puts(buf);
}
