/* -*- coding: utf-8 -*-
 * The Unbelievable Operating System from Hell
 * Copyright © 2005-2007 Göran Weinholt <goran@weinholt.se>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <nygos.h>
#include <malloc.h>
#include <multitasking.h>
#include <stdio.h>

#include <interrupt.h>

#include <string.h>

/* XXX: remember to unintern symbols */

/* A page-based memory manager should be implemented. There are a
 * number of types of pages that should be provided:
 *  . Read-only executable pages for code. These pages will at
 *    first need to be read/write non-executable.
 *  . Read/write non-executable pages for normal data.
 *  . Read/write non-executable pages neighbouring not-present
 *    pages. These will be used to implement automatically
 *    growing stacks. There will have to be a limit to the size
 *    of stacks. Since the address space is rather big (on 64-bit
 *    machines anyway) there should not be a problem with spacing
 *    the stacks far apart. There is a chance that call/cc can be
 *    made more efficient with copy-on-write stacks.
 *  . Pages for DMA. These will have a PCI device associated with
 *    them so that an IOMMU can be used to set access rights.
 *    These will be handled as 'bytes' objects and it would be
 *    nice if they could be passed between driver and user code
 *    without being copied.
 *    
 * One might also want to set different cache modes on a page per page
 * basis. UHCI transfer descriptors should probably be allocated from
 * pages that are "strong uncachable", but data transfers should be
 * made to cachable pages.
 */

static const int DEBUG = 0;

static void run_gc(void);

extern void *end, *start;
static void *heap = NULL, *heap_end;
static void *to_start, *to_end;
static void *from_start, *from_end;
static void *next_free;

void memory_init(unsigned long mem_upper)
{
	heap = (void *) ALIGN((unsigned) &end + 1, 4096);
	heap_end = (void *) ((unsigned) &start + (mem_upper * 1024));

	to_start = next_free = heap;
	to_end = (void *) ((unsigned) &start + (mem_upper / 2) * 1024);
	from_start = to_end;
	from_end = heap_end;
}

/* size is aligned to 16 bytes */
SCM _new(uint64 size, enum type type)
{
	SCM *ptr;

retry:
	ptr = (SCM *) size;
	asm volatile ("lock; xadd %1, %2" :
				  "=r" (ptr) :
				  "0" (ptr), "m" (next_free) :
				  "%1", "memory");
	if (UNLIKELY(ptr > (SCM *) to_end)) {
		run_gc();
		goto retry;
	}

	if (DEBUG) {
		int x, y;
		getxy(&x, &y);
		setxy(70, 0);
		puts("         ");
		setxy(70, 0);
		puti((uint64) to_end - (uint64) ptr, 10);
		setxy(x, y);
	}

	memset(ptr, 0, size);

	return (SCM) ptr | (unsigned int) type;
}

void immortalize(SCM index, int mortal)
{
	/* Put SCM variables in a special section or add their locations here. */
}

static void run_gc(void)
{
	panic("implement GC");
}
