;; -*- coding: utf-8 -*-
;; The Unbelievable Operating System from Hell
;; Copyright © 2005-2006 Göran Weinholt <goran@weinholt.se>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;;
;; Utility functions

;; A UTF8->string would be better, maybe.
;; FIXME: move away from here.... :)
(define (asciiz->string memory _offset)
  (let lp ((l '()) (offset _offset))
    (let ((byte (bytevector-u8-ref memory offset)))
      (if (zero? byte)
          (list->string (reverse (map integer->char l)))
          (lp (cons byte l) (+ offset 1))))))

;;;
;; Multiboot information structure for i386
(define-bytevector-struct mbi (endianness little)
  (u32 mbi-flags)
  (u32 mbi-mem-lower)
  (u32 mbi-mem-upper)
  (u32 mbi-boot-device)
  (u32 mbi-cmdline)
  (u32 mbi-mods-count)
  (u32 mbi-mods-addr)
  ;; ELF section header table
  (u32 mbi-elf-sec-num)
  (u32 mbi-elf-sec-size)
  (u32 mbi-elf-sec-addr)
  (u32 mbi-elf-sec-shndx)
  (u32 mbi-mmap-length)
  (u32 mbi-mmap-addr)
  (u32 mbi-drives-length)
  (u32 mbi-drives-addr)
  (u32 mbi-config-table)
  (u32 mbi-boot-loader-name)
  (u32 mbi-apm-table))

(define multiboot-flags-all
  '#(mem boot_device cmdline mods syms-a.out syms-elf mmap
     drives config_table boot_loader_name apm_table vbe
     unknown-12 unknown-13 unknown-14 unknown-15 unknown-16
     unknown-17 unknown-18 unknown-19 unknown-20 unknown-21
     unknown-22 unknown-23 unknown-24 unknown-25 unknown-26
     unknown-27 unknown-28 unknown-29 unknown-30 unknown-31))

(define mbi
  (make-bytevector/direct (* 4 (+ 1 2 1 1 2 4 2 2 1 1 1 6))
                          (multiboot-info-location)))

;; Go through every bit and add to multiboot-flags the
;; symbolic name for the flags that are set.
(define multiboot-flags
  (do ((flag-bits (mbi-flags mbi))
       (bit 31 (- bit 1))
       (flags '() (if (logtest (ash 1 bit) flag-bits)
                      (cons (vector-ref multiboot-flags-all bit) flags)
                      flags)))
      ((negative? bit) flags)))

(print "Multiboot information structure flags:")
(print multiboot-flags)

(when (memq 'mem multiboot-flags)
  (print "mem-lower = " (mbi-mem-lower mbi)
         " KiB, mem-upper = " (mbi-mem-upper mbi) " KiB"))

(when (memq 'boot_loader_name multiboot-flags)
  (let ((loader@ (mbi-boot-loader-name mbi)))
    (print "boot_loader_name = "
           (asciiz->string (make-bytevector/direct 64 loader@) 0))))

(define cmdline
  (if (memq 'cmdline multiboot-flags)
      (let ((cmdline@ (mbi-cmdline mbi)))
        (asciiz->string (make-bytevector/direct 1024 cmdline@) 0))
      ""))

(define-bytevector-struct multiboot-mmap (endianness little)
  (u32 mmap-size)
  (u64 mmap-base-address)
  (u64 mmap-length)
  (u32 mmap-type))

;;; Fetch and print the memory map
(define mmap
  (cond ((memq 'mmap multiboot-flags)
         (print "Memory map:")
         (let lp ((offset 0)
                  (size (+ 4 (mmap-size
                              (make-bytevector/direct 4 (mbi-mmap-addr mbi)))))
                   (map '()))
            (if (>= offset (mbi-mmap-length mbi))
                (reverse map)
                (let ((part (make-bytevector/direct size
                                                    (+ (mbi-mmap-addr mbi) offset))))
                  (lp (+ offset size)
                      (+ (mmap-size part) 4)
                      (cons (list (mmap-base-address part)
                                  (mmap-length part)
                                  (mmap-type part))
                            map))))))
        (else '())))

(let ()
  (define (type->name x)
    (cond ((assq x '((1 . "Usable")
                     (2 . "Reserved")
                     (3 . "ACPI data")
                     (4 . "ACPI NVS"))) => cdr)
          (else
           (string-append "Reserved (" (number->string x) ")"))))
  (define (bytes->binary-IEC-amount bytes)
    (cond ((< bytes 1024)
           (string-append (number->string bytes) " B"))
          ((< bytes (* 1024 1024))
           (string-append (number->string (ash bytes -10)) " KiB"))
          ((< bytes (* 1024 1024 1024))
           (string-append (number->string (ash bytes -20)) " MiB"))
          (else
           (string-append (number->string (ash bytes -30)) " GiB"))))
  (for-each (lambda (region)
              (print (number->string (car region) 16)
                     "-" (number->string (+ (car region)
                                            (cadr region)
                                            -1)
                                         16)
                     " " (bytes->binary-IEC-amount (cadr region))
                     " (" (type->name (caddr region)) ")"))
            mmap))

(print "cmdline = " cmdline)

(newline)
