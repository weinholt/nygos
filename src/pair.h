/* -*- coding: utf-8 -*-
 * The Unbelievable Operating System from Hell
 * Copyright © 2005-2006 Göran Weinholt <goran@weinholt.se>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _PAIR_H
#define _PAIR_H

void pair_import(void);

SCM cons(SCM car, SCM cdr);
SCM set_cdr_ex(SCM pair, SCM cdr);

struct pair {
	SCM car, cdr;
};

#define CAR(n)  (((struct pair *) get_addr(n))->car)
#define CDR(n)  (((struct pair *) get_addr(n))->cdr)

#endif
